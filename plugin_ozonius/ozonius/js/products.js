

$(document).ready(function(){

    const prepBtn = $('.prepare-btn'),
          sendBtn = $('.send-btn');

    prepBtn.on('click',function(ev){


        const catId = $(ev.target).data('id');
        const marNum = $(ev.target).data('markup');
        const ozonCatId = $(ev.target).data('ozon_cat');
        var data = {};
        if($(this).hasClass('pr-category-js')){
             data = {
                 setId : catId,
                 marNum : marNum,
                 ozonCatId : ozonCatId
             }
        }
        else {
            data = {
                categoryId : catId,
                marNum : marNum,
                ozonCatId : ozonCatId
            }
        }
        App.loader('show');


        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=prepareCategory',
            data: JSON.stringify(data),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){

                if (data['validCount'] && data['validCount'] > 0) {
                    $('.pr-ready[data-id='+catId+']').html(data['validCount']);
                    $('.send-btn[data-id='+catId+']').prop('disabled', false);
                    $('.pp[data-id='+catId+']').addClass('hidden');
                    $('.pr-errors-container[data-id='+catId+']').html('');
                } else {
                    $('.send-btn[data-id='+catId+']').prop('disabled', true);
                }
                //console.log(data);
                if (data['errors'] && data['errors'].length > 0) {
                    $('.pp[data-id='+catId+']').removeClass('hidden');
                    $('.pr-errors-container[data-id='+catId+']').html('');
                    data['errors'].forEach(function (error) {
                       $html = '<div class="row pr-error-row">\n' +
                           '                                    <div class="container-fluid">\n' +
                           '                                        <div class="row">\n' +
                           '                                            <div class="pr-error-product col-6 text-center"><a href="'+error['url']+'" target="_blank">'+error['name']+'</a></div>\n' +
                           '                                            <div class="pr-error-message col-6 text-center">'+error['message']+'</div>\n' +
                           '                                        </div>\n' +
                           '                                    </div>\n' +
                           '                                </div>';
                        $('.pr-errors-container[data-id='+catId+']').append($html);
                    });
                }
                else {
                    $('.pp[data-id='+catId+']').addClass('hidden');
                }
                App.loader('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
                App.loader('hide');
            }
        });


    });

    sendBtn.on('click',function(ev){
        const catId = $(ev.target).data('id');
        App.loader('show');
        var data = {};
        if($(this).hasClass('send-category-js')){
            data = { setId : catId }
        }
        else {
            data = { categoryId : catId }
        }
        $.ajax({
            type: "POST",
            url:'?plugin=ozonius&module=api&action=sendCategory',
            data: JSON.stringify(data),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                if (data['messages']) {
                    if (Array.isArray(data['messages'])) {
                        var message = '';
                        data['messages'].forEach(function(message,i){
                            if (message['count']) {
                                message += i + ': ' + message['message'] +' ('+ message['count'] +') '+ '<br>';
                            } else {
                                message += i + ': ' + message['message'] + '<br>';
                            }
                        });
                        alert(message);
                    } else {
                        alert(data['messages']);
                    }
                }
                if (data['count']) {
                    let count = $('.pr-count-sended[data-id='+catId+']');
                    let current = new Number(count.text());
                    let newCount = current + data['count'] * 1;
                    count.text(newCount);
                }
                $('.send-btn[data-id='+catId+']').prop('disabled', true);
                App.loader('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
                App.loader('hide');
            }
        });
    });
});
var Action = {
    obj: '',
    productIds: [],
    productSku: [],
    productCatOzon: [],
    productMarkup: [],
    currentCat: null,
    currentData: null,
    startPage : 1,
    init: function (action) {
        this.collectIds();

        if (this.productIds.length > 0) {
            Action[action]();
        } else {
            alert("Не выбран ни один товар!");
        }
    },
    loader: function(state){
        var layout = $('.layout-loader'),
            loader = $('.lds-roller');
        if (typeof state === 'string' && state === 'hide') {
            layout.hide();
            loader.hide();
        } else if (typeof state === 'string' && state === 'show') {
            layout.show();
            loader.show();
        }
    },
    collectIds: function () {
        var cheks = $('.pl-product-row input[type=checkbox]');
        this.productSku = [];
        $.each(cheks, function (index, el) {
            if ($(el).prop('checked')) {
                Action.productSku.push($(el).data('sku'));
            }
        });
        this.productIds = [];
        $.each(cheks, function (index, el) {
            if ($(el).prop('checked')) {
                Action.productIds.push($(el).data('product_id'));
            }
        });
        this.productMarkup = [];
        $.each(cheks, function (index, el) {
            if ($(el).prop('checked')) {
                Action.productMarkup.push($(el).data('markup'));
            }
        });
        this.productCatOzon = [];
        $.each(cheks, function (index, el) {
            if ($(el).prop('checked')) {
                Action.productCatOzon.push($(el).data('ozon_cat'));
            }
        });
    },
    activate: function () {
        let ids = this.productIds.filter(function (el) {
            return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
        });
        this.ajaxSend('?plugin=ozonius&module=api&action=productActivate', this.productIds);
    },
    deactivate: function () {
        let ids = this.productIds.filter(function (el) {
            return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
        });
        this.ajaxSend('?plugin=ozonius&module=api&action=productDeactivate', this.productIds);
    },
    delete: function () {
        this.ajaxSend('?plugin=ozonius&module=api&action=productDelete', this.productSku);
    },
    updatePrice: function () {
        if (this.productIds.length > 1000) {
            alert("За один запрос можно изменить цены для 1000 товаров!");
        } else {
            this.ajaxSend('?plugin=ozonius&module=api&action=productUpdatePrice', this.productIds);
        }
    },
    updateStock: function () {
        if (this.productIds.length > 100) {
            alert("За один запрос можно изменить наличие для 100 товаров!");
        } else {
            let ids = this.productIds.filter(function (el) {
                return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
            });

            this.ajaxSend('?plugin=ozonius&module=api&action=productUpdateStock', this.productIds);
        }
    },
    updateStockWarehouse: function () {
        if (this.productIds.length > 100) {
            alert("За один запрос можно изменить наличие для 100 товаров!");
        } else {

            let ids = this.productIds.filter(function (el) {
                return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
            });
            this.productIds.push(this.obj);
            this.ajaxSend('?plugin=ozonius&module=api&action=productUpdateStockWarehouse', this.productIds);
        }
    },
    updateDescription: function () {
        let ids = this.productIds.filter(function (el) {
            return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
        });
        this.obj = {
            markup: this.productMarkup,
            ozon_cat: this.productCatOzon
        };
        this.productIds.push(this.obj);
        this.ajaxSend('?plugin=ozonius&module=api&action=productUpdateDescription', this.productIds);
    },
    updateDescriptionManually: function () {
        if (this.productIds.length > 1) {
            alert("За один запрос можно изменить карточку только для одного товара!");
        } else {


            let ids = this.productIds.filter(function (el) {
                return $('input[data-product_id=' + el + ']').parent().siblings('.pl-state').text() == 'Обработан';
            });

            this.productIds.push(this.productSku);
            this.ajaxSend('?plugin=ozonius&module=api&action=productUpdateDescriptionManually', this.productIds);
        }
    },
    ajaxSend: function (url, data) {
        Action.loader('show');
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify({data: data}),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                alert('Запрос отправлен на Ozon! Обновленные данные отобразятся на странице через некоторое время. Не забывайте периодически обновлять страницу.');
                Action.loader('hide');
                Action.refreshInfo();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');
                $('#result').html('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
                Action.loader('hide');
            }
        });
    },


    refreshInfo: function () {
        var rowsContainer = $('.pl-product-rows');
        var container = $('#pagination');
        Action.loader('show');
        $.ajax({
            type: "GET",
            url: '?plugin=ozonius&module=api&action=getProductsCountGet&categoryId=' + Action.currentCat + '&select=' + Action.currentData,
            dataType: "json",
            success: function (res) {
                let notesOnPage = 1;
                //let limit = 1;
                // let prev_text = 'назад';
                // let next_text = 'вперед';
                // let separator = '...';

                let countOfItems = Math.ceil(res.count / res.limit) ;

                if(parseInt(res.count) <= parseInt(res.limit)){
                    $('#pagination').hide();
                }
                else {
                    $('#pagination').show();
                }
                let pagination = document.querySelector('#pagination');
                pagination.innerHTML = '';
                let it = [];
                // let li_next = document.createElement('li');
                // li_next.innerHTML = next_text;

                for (let i = 1; i <= countOfItems; i++) {
                    let li = document.createElement('li');
                    li.innerHTML = i;

                    pagination.appendChild(li);
                    it.push(li);
                }

                var pagination_li = document.querySelectorAll('#pagination li');

                for (let item of pagination_li) {
                    var active;
                    var page = 1;
                    item.addEventListener('click', function (e) {

                            if(this.classList.contains('disabled')){
                                return false;
                            }
                        if (active) {
                            active.classList.remove('active');
                            //active.classList.remove('disabled');
                        }
                        active = this;

                        // if(active.innerText == 'вперед'){
                        //     page ++;
                        //     console.log(pagination_li.length -1);
                        //
                        //     if(page == pagination_li.length -1){
                        //         this.classList.add('disabled');
                        //         // this.onclick = function() {
                        //         //
                        //         // };
                        //
                        //         //return false;
                        //     }
                        //     //
                        //      p(page);
                        // }

                        this.classList.add('active');
                        p(this.innerText);
                        //page = (this);

                    });
                }


                function p(that) {
                    $.ajax({
                        type: "GET",
                        url: '?plugin=ozonius&module=api&action=getProductsInfoGet&categoryId=' + Action.currentCat + '&page=' + that + '&select=' + Action.currentData,
                        dataType: "json",
                        success: function (res) {


                            //if (res.products  &&  Object.keys(res.products).length !== 0) {

                            rowsContainer.html('');
                            if (res.products) {

                            $.each(res.products, function (index, data) {

                                //var data = data[1];
                                let stocks = '--',
                                    visibleDetails = '',
                                    validErrors = '--',
                                    price = data.price || '--',
                                    visible = data.visible.state || '--',
                                    shopPrice = data.shopPrice || '--',
                                    shopCount = data.shopCount || '--';
                                if (data.stocks && Object.keys(data.stocks).length !== 0) {
                                    stocks = data.stocks.present;
                                }
                                if (data.markup !== 0) {
                                    shopPrice = Number((shopPrice / 100 * data.markup)) + Number(shopPrice) + Number(data.fix);

                                }
                                if (data.validationErrors && Object.keys(data.validationErrors).length !== 0) {
                                    $.each(data.validationErrors, function (index, elements) {
                                        validErrors = '<div class="error-wrap text-left">Поле: ' + elements.field + '<br> Код ошибки: ' + elements.code + '<br> Описание ошибки: ' + elements.error + '</div>';
                                    });
                                }
                                if (data.visible.details && Object.keys(data.visible.details).length !== 0) {
                                    let hasPrice = data.visible.details['has_price'] ? 'да' : 'нет',
                                        hasStock = data.visible.details['has_stock'] ? 'да' : 'нет',
                                        activeProduct = data.visible.details['active_product'] ? 'да' : 'нет';
                                    visibleDetails = '<div class="visible-wrap text-left">Доступен на складе: ' + hasStock + '</div>';
                                }

                                let row = '<div class="row p-1 pl-product-row d-flex flex-row align-items-center">\n' +
                                    '                        <div class="col-1"><input type="checkbox" data-sku = "' + data.shopSku + '" data-product_id="' + data.ozonId + '" data-markup="' + data.markup + '" data-ozon_cat="' + data.ozonCat + '"></div>\n' +
                                    '                        <div class="col-2"><a href="' + data.url + '" target="_blank">' + data.name + '</a></div>\n' +
                                    '                        <div class="col-1">' + data.shopSku + '</div>\n' +
                                    '                        <div class="col-1">' + shopPrice + ' /<br>' + price + '</div>\n' +
                                    '                        <div class="col-1">' + data.markup + '%' + '</div>\n' +
                                    '                        <div class="col-1">' + shopCount + '/' + stocks + '</div>\n' +
                                    '                        <div class="col-2 pl-state">' + data.state + '</div>\n' +
                                    '                        <div class="col-1">' + visible + visibleDetails + '</div>\n' +
                                    '                        <div class="col-2">' + validErrors + '</div>\n' +
                                    '                    </div>';
                                rowsContainer.append(row);
                            });
                        }
                            //}
                            Action.loader('hide');
                        }
                    });
                }
                p(notesOnPage);
            }
        });



    },
    addProduct: function () {
        Action.loader('show');
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=setProductsAdd',
            //data: JSON.stringify({ categoryId : Action.currentCat }),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                Action.loader('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                $('#result').html('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
                Action.loader('hide');
            }
        });

    },
    search: function (data) {
        Action.loader('show');
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=searchProducts',
            data: JSON.stringify(data),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {

                var rowsContainer = $('.pl-product-rows');
                rowsContainer.html('');

                $.each(res, function (index, data) {
                    let stocks = '--',
                        visibleDetails = '',
                        validErrors = '--',
                        price = data.price || '--',
                        visible = data.visible.state || '--',
                        shopPrice = data.shopPrice || '--',
                        shopCount = data.shopCount || '--';
                    if (data.stocks && Object.keys(data.stocks).length !== 0) {
                        stocks = data.stocks.present;
                    }
                    if (data.markup !== 0) {
                        shopPrice = Number((shopPrice / 100 * data.markup)) + Number(shopPrice) + Number(data.fix);

                    }
                    if (data.validationErrors && Object.keys(data.validationErrors).length !== 0) {
                        $.each(data.validationErrors, function (index, elements) {
                            validErrors = '<div class="error-wrap text-left">Поле: ' + elements.field + '<br> Код ошибки: ' + elements.code + '<br> Описание ошибки: ' + elements.error + '</div>';
                        });
                    }
                    if (data.visible.details && Object.keys(data.visible.details).length !== 0) {
                        let hasPrice = data.visible.details['has_price'] ? 'да' : 'нет',
                            hasStock = data.visible.details['has_stock'] ? 'да' : 'нет',
                            activeProduct = data.visible.details['active_product'] ? 'да' : 'нет';
                        visibleDetails = '<div class="visible-wrap text-left">Доступен на складе: ' + hasStock + '</div>';
                    }

                    let row = '<div class="row p-1 pl-product-row d-flex flex-row align-items-center">\n' +
                        '                        <div class="col-1"><input type="checkbox" data-sku = "' + data.shopSku + '" data-product_id="' + data.ozonId + '" data-markup="' + data.markup + '" data-ozon_cat="' + data.ozonCat + '"></div>\n' +
                        '                        <div class="col-2"><a href="' + data.url + '" target="_blank">' + data.name + '</a></div>\n' +
                        '                        <div class="col-1">' + data.shopSku + '</div>\n' +
                        '                        <div class="col-1">' + shopPrice + ' /<br>' + price + '</div>\n' +
                        '                        <div class="col-1">' + data.markup + '%' + '</div>\n' +
                        '                        <div class="col-1">' + shopCount + '/' + stocks + '</div>\n' +
                        '                        <div class="col-2 pl-state">' + data.state + '</div>\n' +
                        '                        <div class="col-1">' + visible + visibleDetails + '</div>\n' +
                        '                        <div class="col-2">' + validErrors + '</div>\n' +
                        '                    </div>';
                    rowsContainer.append(row);
                });
                Action.loader('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                $('#result').html('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
                Action.loader('hide');
            }
        });

    }
};

$(document).ready(function () {

    var select = $('#pl-category-select');
    var search = $('.search-ozon-wr input');
    search.on('input', function (ev) {
        $('#pagination').hide();
        if(search.val().length >=3){
            let obj = {
                value: search.val(),
                select_val: select.val(),
                sel: $('#pl-category-select option:selected').data('sel')
            };
            Action.search(obj);
        }
        if(search.val() == 0){
            Action.refreshInfo();
            $('#pagination').show();
        }

    });

    select.on('change', function (ev) {
        if ($('#pl-category-select option:selected').data('manually') == 'manually') {
            //$("pl-action-select").val()
            $('#pl-action-select option:nth-child(4)').val('updateDescriptionManually');
        } else {
            $('#pl-action-select option:nth-child(4)').val('updateDescription');
        }

        Action.currentCat = select.val();
        Action.currentData = $('#pl-category-select option:selected').data('sel');

        //Action.currentData = $('#pl-category-select option:selected').data('manually');

        if (!Action.currentCat) {
            $('#btn-refresh').prop('disabled', true);
        } else {
            $('#btn-refresh').prop('disabled', false);
            $('.search-ozon-wr').show();
        }
        Action.refreshInfo();
    });

    $('.select-all').on('click', function () {
        var cheks = $('.pl-product-row input[type=checkbox]');
        if ($(this).prop('checked')) {
            $.each(cheks, function (index, el) {
                $(el).prop('checked', true);
            });
        } else {
            $.each(cheks, function (index, el) {
                $(el).prop('checked', false);
            });
        }
    });

    $('#btn-refresh').on('click', function () {
        Action.refreshInfo();
    });
    $('#btn-add-product').on('click', function () {
        Action.addProduct();
    });
    /* ----- product actions -----*/

    var btnActivate = $('#btn-activate'),
        btnDeactivate = $('#btn-deactivate'),
        btnDelete = $('#btn-delete'),
        actionSelect = $('#pl-action-select'),
        actionSelectWarehouse = $('#pl-action-select-warehouse'),
        btnOk = $('#btn-ok');

    actionSelect.on('change', function () {
        if (actionSelect.val() && actionSelect.val()) {

            if (actionSelect.val() == 'updateStockWarehouse') {

                $('.warehouse').css("display", "flex");
                actionSelectWarehouse.on('change', function () {
                    if (actionSelectWarehouse.val()) {
                        btnOk.attr('disabled', false);
                    } else {
                        btnOk.attr('disabled', true);
                    }
                });

                btnOk.attr('disabled', true);
            } else {

                $('.warehouse').hide();
                btnOk.attr('disabled', false);
            }


        } else {
            btnOk.attr('disabled', true);
        }
    });

    $('#pl-action-select-warehouse').on('change', function () {
        Action.obj = $(this).val();
    });

    btnOk.on('click', function () {
        let action = actionSelect.val();
        Action.init(action);
    });

    btnActivate.on('click', function () {
        Action.init('activate');
    });

    btnDeactivate.on('click', function () {
        Action.init('deactivate');
    });

    btnDelete.on('click', function () {
        Action.init('delete');
    });

});
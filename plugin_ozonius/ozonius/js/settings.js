jQuery(function ($) {

    var buttonWarehouses = $('button.сompare-warehouses'),
        selectShop = $('#shop_ozonius_stocks_plugin'),
        selectOzon = $('#shop_ozonius_warehouse_plugin'),
        categoryList = $('ul.stocks-list');

    $('#shop_ozonius_stocks_plugin, #shop_ozonius_warehouse_plugin').on('change', function () {
        if (selectShop.val() && selectOzon.val()) {
            buttonWarehouses.attr('disabled', false);
        } else {
            buttonWarehouses.attr('disabled', true);
        }
    });

    $(document).on('click', 'button.сompare-warehouses', function () {
        if (selectShop.val() && selectOzon.val()) {
            const shopStocksId = selectShop.val(),
                shopStocksName = selectShop.find('option:selected').html().replace(/&nbsp;/gi, ''),
                ozonStocksId = selectOzon.val(),
                ozonStocksName = selectOzon.find('option:selected').html().replace(/&nbsp;/gi, '');


            const newRow = '<li class="category-row">' +
                '                        <span class="remove">X</span>\n' +
                '                        <ul class="item">\n' +
                '                            <li class="shop" data-id="' + shopStocksId + '" data-name="' + shopStocksName.trim() + '">' + shopStocksName + '</li>\n' +
                '                            <li class="ozon" data-id="' + ozonStocksId + '" data-name="' + ozonStocksName + '">' + ozonStocksName + '</li>\n' +
                '                        </ul>\n' +
                '                    </li>';

            categoryList.append(newRow);
            selectShop.children('option:selected').prop('selected', false);
            selectOzon.children('option:selected').prop('selected', false);
            $("#selectShop option[value='" + shopStocksId + "']").remove();

            buttonWarehouses.attr('disabled', true);

        }
    });
    $(document).on('click', '.remove', function (event) {
        $(this).parent().fadeOut(500, function () {
            let shop = $(this).find('li.shop');
            let option = "<option value='" + shop.data('id') + "'>" + shop.data('name') + "</option>";
            $('#selectShop').append(option);
            $(this).remove();
        });
        event.stopPropagation();
    });

    var fbs = document.querySelector('[name="shop_ozonius[get_fbs]"]');

    var divFbs = $('#shop_ozonius_stocks');
    var divWarehouse = $('.shop_ozonius_warehouse');
    function toggle() {
        if (this.checked) {
            divFbs.hide();
            divWarehouse.show();
        } else {
            divFbs.show();
            divWarehouse.hide();
        }
    }
    fbs.onchange = toggle;

    function settingToJSON(f) {
        var fd = $(f).serializeArray();
        var d = {};
        $(fd).each(function () {
            if (this.name.indexOf('shop_ozonius') === 0) {
                var matchName = this.name.matchAll(/\[(.*?)\]/g),
                    matchName = Array.from(matchName),
                    name = matchName[0][1];
                if (name != '') d[name] = this.value;
            }
        });
        return d;
    }

    $('#ozonius-settings-form').on('submit', function (e) {
        e.preventDefault();

        var $form = $(this),
            settings = settingToJSON($form),
            stocks = $form.find('#shop_ozonius_stocks input:checked'),
            warehouse = $form.find('.shop_ozonius_warehouse input:checked'),
            dataStocks = [],
            dataWarehouse = [];
        var ch = "";
        if (fbs.checked) {
            ch = "On";
        } else {
            ch = "Off";
        }

        let items = categoryList.find('ul.item');
        var data = {};

        if (items.length > 0) {
            items.each(function (i, e) {
                let shop = $(e).find('.shop'),
                    ozon = $(e).find('.ozon');

                data[i] = {};

                data[i].ozon = ozon.data('name') + ';' + ozon.data('id');
                data[i].shop = shop.data('name') + ';' + shop.data('id');

            });
        } else {
            data = "";
        }

        $.each(stocks, function (i, v) {
            dataStocks[i] = [];
            dataStocks[i] = $(this).val();
        });
        if (warehouse.length > 0) {
            $.each(warehouse, function (i, v) {
                dataWarehouse[i] = [];
                dataWarehouse[i] = $(this).val();
            });
        } else {
            dataWarehouse = "";
        }

        let rulCheckContact = document.querySelector(".check-rules-contact:checked");
        let rulContact = 'disabled';
        if(rulCheckContact !== null){
            rulContact = rulCheckContact.value;
        }

        settings.rulContact = rulContact;
        settings.stocks = dataStocks;
        settings.warehouseData = dataWarehouse;
        settings.get_fbs = ch;
        settings.dataRes = data;
        $.ajax({
            url: '?plugin=ozonius&module=settings&action=save',
            method: 'post',
            data: {
                settings: settings,
                stocks: dataStocks,
                d: data
            },
            dataType: 'json',
            success: function (response) {

                if (response.status == 'ok') {
                    $('#plugins-settings-form-status > i').removeClass('no').addClass('yes');
                    $('#plugins-settings-form-status > span').text(response.data.message);
                } else {
                    $('#plugins-settings-form-status > i').removeClass('yes').addClass('no');
                    $('#plugins-settings-form-status > span').text(response.errors.message);
                }
                $('#plugins-settings-form-status').fadeIn(300).delay(3000).fadeOut(300);
            }
        });
    });

});
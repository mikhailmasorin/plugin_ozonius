
var App = {
  parents: [],
  parentsShop: [],
  origin: window.location.origin,
  init: function() {
      this.loader('hide');
      if($('.check-set-js button').hasClass("check-set")){
          $("#selectShop").select2({
              placeholder: "Выберите списки магазина",
              allowClear: true
          });
      }
      else {
          $("#selectShop").select2({
              placeholder: "Выберите категорию магазина",
              allowClear: true
          });
      }

      $("#selectOzon").select2({
          placeholder: "Выберите категорию на Ozon",
          allowClear: true
      });
      if ($('ul.category-list').children().length > 0) {
          $('button.remove-all').removeClass('hidden');
      }
      var parentsString = $('div.parents').data('parents');
      if (parentsString != "" && typeof parentsString != "undefined") {
        this.parents = parentsString.split(':');
      }
      var parentsShopString = $('div.parentsShop').data('parents');
      if (typeof parentsShopString != "undefined" && parentsShopString != "") {
        this.parentsShop = parentsShopString.split(':');
      }

  },
  commit: function(data){
      App.loader('show');
      let url = '?plugin=ozonius&module=api&action=matchedCategories';
      $.ajax({
          type: "POST",
         // url: '/ozonius/api/categoriesCommit/',
          url: '?plugin=ozonius&module=api&action=commit',
          data: JSON.stringify({
              matches: data,
          }),
          //contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(data){
              var message = '';
              if (typeof data['messages'] !== 'undefined') {
                  if (typeof data['messages']['main']  !== 'undefined') {
                      message += data['messages']['main'] + '\n';
                  }
                  if (typeof data['messages']['count']  !== 'undefined') {
                      message += data['messages']['count'] + '\n';
                  }
                  if (typeof data['messages']['deleted']  !== 'undefined') {
                      message += data['messages']['deleted'] + '\n';
                  }
                  if (typeof data['messages']['delete_errors'] !== 'undefined') {
                      data['messages']['delete_errors'].forEach(function(el){
                          message += el + '\n';
                      });
                  }
                  if (typeof data['messages']['update_errors']  !== 'undefined') {
                      data['messages']['update_errors'].forEach(function(el){
                          message += el + '\n';
                      });
                  }
              }
              if (typeof data['categories']  !== 'undefined') {
                  data['categories'].forEach(function (el) {
                      $("#selectShop option[value='" + el + "']").remove();
                  });
              }
              //$('button.apply').addClass('hidden');
              App.init();
              alert(message);
              App.loader('hide');
              App.refresh(url);
          },
          failure: function(data) {
              var message = '';
              if (typeof data['messages'] !== 'undefined') {
                  if (data['messages']['main']) {
                      message += data['messages']['main'] + '\n';
                  }
                  if (typeof data['messages']['deleted']  !== 'undefined') {
                      message += data['messages']['deleted'] + '\n';
                  }
                  if (typeof data['messages']['delete_errors'] !== 'undefined') {
                      data['messages']['delete_errors'].forEach(function(el){
                          message += el + '\n';
                      });
                  }
              }
              alert(message);
          }
      });
  },
    commit_set: function(data){
        App.loader('show');
        let url = '?plugin=ozonius&module=api&action=matchedSets';
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=commitSets',
            data: JSON.stringify({
                matches: data,
            }),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data){
                var message = '';
                if (typeof data['messages'] !== 'undefined') {
                    if (typeof data['messages']['main']  !== 'undefined') {
                        message += data['messages']['main'] + '\n';
                    }
                    if (typeof data['messages']['count']  !== 'undefined') {
                        message += data['messages']['count'] + '\n';
                    }
                    if (typeof data['messages']['deleted']  !== 'undefined') {
                        message += data['messages']['deleted'] + '\n';
                    }
                    if (typeof data['messages']['delete_errors'] !== 'undefined') {
                        data['messages']['delete_errors'].forEach(function(el){
                            message += el + '\n';
                        });
                    }
                    if (typeof data['messages']['update_errors']  !== 'undefined') {
                        data['messages']['update_errors'].forEach(function(el){
                            message += el + '\n';
                        });
                    }
                }
                if (typeof data['categories']  !== 'undefined') {
                    data['categories'].forEach(function (el) {
                        $("#selectShop option[value='" + el + "']").remove();
                    });
                }
                //$('button.apply').addClass('hidden');
                App.init();
                alert(message);
                App.loader('hide');
                App.refresh(url);
            },
            failure: function(data) {
                var message = '';
                if (typeof data['messages'] !== 'undefined') {
                    if (data['messages']['main']) {
                        message += data['messages']['main'] + '\n';
                    }
                    if (typeof data['messages']['deleted']  !== 'undefined') {
                        message += data['messages']['deleted'] + '\n';
                    }
                    if (typeof data['messages']['delete_errors'] !== 'undefined') {
                        data['messages']['delete_errors'].forEach(function(el){
                            message += el + '\n';
                        });
                    }
                }
                alert(message);
            }
        });
    },
  remove: function(url){
      App.loader('show');
      $.ajax({
          type: "POST",
          url: url,
          //contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(data){
              var message = '';
              if (typeof data['messages'] !== 'undefined') {
                  if (data['messages']['main']) {
                      message += data['messages']['main'] + '\n';
                  }
                  if (typeof data['messages']['deleted']  !== 'undefined') {
                      message += data['messages']['deleted'] + '\n';
                  }
                  if (typeof data['messages']['delete_errors'] !== 'undefined') {

                      data['messages']['delete_errors'].forEach(function(el){
                          message += el + '\n';
                      });
                  }
              }
              //$('button.apply').addClass('hidden');
              $('button.remove-all').addClass('hidden');
              App.clearTable();
              App.init();
              alert(message);
              App.loader('hide');
              App.refresh();
          },
          failure: function(data) {
              var message = '';
              if (data['messages']['main']) {
                  message += data['messages']['main'] + '\n';
              }
              alert(message);
          }
      });
  },
  clearTable: function(){
      const categoryList = $('ul.category-list');

      categoryList.children().each(function(i,child){
          let shop = $(child).find('li.shop');
          let option = "<option value='"+shop.data('id')+"'>"+shop.data('name')+"</option>";
          $('#selectShop').append(option);
      });

      categoryList.empty();
  },
  loader: function(state){
      var layout = $('.layout-loader'),
          loader = $('.lds-roller');
      if (typeof state === 'string' && state === 'hide') {
          layout.hide();
          loader.hide();
      } else if (typeof state === 'string' && state === 'show') {
          layout.show();
          loader.show();
      }
  },
  refresh: function(url) {
      App.loader('show');
      $.ajax({
          type: "POST",
          url: url,
          data: {},
          //contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(data){
              if (typeof data.categories !== 'undefined') {
                  var categoryList = $('.category-list');
                  categoryList.html('');
                  $.each(data.categories,function(index,data){
                      let item = '<li class="category-row">\n' +
                          '                <span class="remove">X</span>\n' +
                          '                <ul class="item">\n' +
                          '                    <li class="shop" data-id="'+index+'" data-name="'+data.name+'">'+data.name+'</li>\n' +
                          '                    <li class="ozon" data-id="'+data.ozonId+'" data-name="'+data['ozonName']+'">'+data['ozonName']+'</li>\n' +
                          '                    <li class="markup"><label>Наценка, %: <input class="markup_price" value="'+data['markupPrice']+'" type="number" name="markup_price"></label></li>\n' +
                          '                    <li class="fix"><label>Фиксированная цена, руб, %: <input class="fix_price" value="'+data['fixPrice']+'" type="number" name="fix_price"></label></li>\n' +

                          '                </ul>\n' +
                          '            </li>';

                      categoryList.append(item);
                  });
              }
              App.loader('hide');
          },
          error: function(jqXHR, textStatus, errorThrown) {
              alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');
              $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
              console.log('jqXHR:');
              console.log(jqXHR);
              console.log('textStatus:');
              console.log(textStatus);
              console.log('errorThrown:');
              console.log(errorThrown);
              App.loader('hide');
          }
      });
  }
};

$(document).ready(function(){

    const selectShop = $('#selectShop'),
    selectOzon = $('#selectOzon'),
    buttonAdd = $('button.add'),
    buttonApply = $('button.apply'),
    buttonRemove = $('button.remove-all'),
    categoryList = $('ul.category-list');

    $('select').on('change',function(){
        if (selectShop.val() && selectOzon.val()) {
            buttonAdd.attr('disabled',false);
        } else {
            buttonAdd.attr('disabled',true);
        }
    });

    App.init();

    $(document).on('click','button.add',function(e){
        e.preventDefault();
        if (selectShop.val() && selectOzon.val()) {
            if (App.parents.indexOf(selectOzon.val()) !== -1 || App.parentsShop.indexOf(selectShop.val()) !== -1) {
                alert("Вы пытаетесь осуществить привязку к родительской категории,\n привязка доступна только для категорий последнего уровня, т.е. не имеющих вложенных категорий!");
                selectShop.children('option:selected').prop('selected', false);
                selectOzon.children('option:selected').prop('selected', false);
                if($('.check-set-js button').hasClass("check-set")){
                    $("#selectShop").select2({
                        placeholder: "Выберите списки магазина",
                        allowClear: true
                    });
                }
                else {
                    $("#selectShop").select2({
                        placeholder: "Выберите категорию магазина",
                        allowClear: true
                    });
                }
                $("#selectOzon").select2({
                    placeholder: "Выберите категорию на Ozon",
                    allowClear: true
                });
                buttonAdd.attr('disabled', true);
            } else {
                const shopCatId = selectShop.val(),
                    shopCatName = selectShop.find('option:selected').html().replace(/&nbsp;/gi, ''),
                    ozonCatId = selectOzon.val(),
                    ozonCatName = selectOzon.find('option:selected').html().replace(/&nbsp;/gi, '');

                const newRow = '<li class="category-row">\n' +
                    '                        <span class="remove">X</span>\n' +
                    '                        <ul class="item">\n' +
                    '                            <li class="shop" data-id="' + shopCatId + '" data-name="' + shopCatName.trim() + '">' + shopCatName + '</li>\n' +
                    '                            <li class="ozon" data-id="' + ozonCatId + '" data-name="' + ozonCatName + '">' + ozonCatName + '</li>\n' +
                    '                            <li class="markup"><label>Наценка, %: <input class="markup_price"  value="0" type="number" name="markup_price"></label></li>\n' +
                    '                            <li class="fix"><label>Фиксированная цена, руб: <input class="fix_price"  value="0" type="number" name="fix_price"></label></li>\n' +
                    '                        </ul>\n' +
                    '                    </li>';

                categoryList.append(newRow);
                selectShop.children('option:selected').prop('selected', false);
                selectOzon.children('option:selected').prop('selected', false);
                $("#selectShop option[value='" + shopCatId + "']").remove();
                if($('.check-set-js button').hasClass("check-set")){
                    $("#selectShop").select2({
                        placeholder: "Выберите списки магазина",
                        allowClear: true
                    });
                }
                else {
                    $("#selectShop").select2({
                        placeholder: "Выберите категорию магазина",
                        allowClear: true
                    });
                }
                $("#selectOzon").select2({
                    placeholder: "Выберите категорию на Ozon",
                    allowClear: true
                });
                buttonAdd.attr('disabled', true);

                if (categoryList.children().length > 0) {
                    buttonApply.removeClass('hidden');
                    buttonRemove.removeClass('hidden');
                }
            }
        }
    });

    $(document).on('click','.remove',function(event){
        $(this).parent().fadeOut(500,function(){
            let shop = $(this).find('li.shop');
            let option = "<option value='"+shop.data('id')+"'>"+shop.data('name')+"</option>";
            $('#selectShop').append(option);
            $(this).remove();
            // if (categoryList.children().length > 0) {
            //     buttonApply.removeClass('hidden');
            //     buttonRemove.removeClass('hidden');
            // } else {
            //     buttonApply.addClass('hidden');
            //     buttonRemove.addClass('hidden');
            // }
        });
        event.stopPropagation();
    });

    $(document).on('submit','#apply-success',function(e){
        e.preventDefault();
       let items = categoryList.find('ul.item');
       var data = {};

       items.each(function(i,e){
           let shop = $(e).find('.shop'),
               markup = $(e).find('.markup_price'),
               fix = $(e).find('.fix_price'),
               fix_ozon= fix.val(),
               ozon = $(e).find('.ozon'),
                markup_ozon= markup.val();

           data[shop.data('id')]={};
           data[shop.data('id')].ozon = ozon.data('id');
           data[shop.data('id')].markup_ozon = markup_ozon;
           data[shop.data('id')].fix = fix_ozon;
       });
        App.commit(data);
    });

    $(document).on('submit','#apply-success-set',function(){
        let items = categoryList.find('ul.item');
        var data = {};

        items.each(function(i,e){
            let shop = $(e).find('.shop'),
                markup = $(e).find('.markup_price'),
                ozon = $(e).find('.ozon'),
                markup_ozon= markup.val(),
                name = shop.data('name'),
                fix = $(e).find('.fix_price'),
                fix_ozon= fix.val();

            data[shop.data('id')]={};
            data[shop.data('id')].ozon = ozon.data('id');
            data[shop.data('id')].markup_ozon = markup_ozon;
            data[shop.data('id')].name = name;
            data[shop.data('id')].fix = fix_ozon;
        });
        App.commit_set(data);
    });


    $(document).on('submit','#remove-all-js',function(){
        let url  = '?plugin=ozonius&module=api&action=removeAll';
        App.remove(url);
    });
    $(document).on('submit','#remove-all-js-set',function(){
        let url  = '?plugin=ozonius&module=api&action=setsRemoveAll';
        App.remove(url);
    });


});


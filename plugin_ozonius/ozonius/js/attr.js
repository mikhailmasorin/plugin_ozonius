$(document).ready(function () {
    $('.shop-slide-block').slideUp('fast');
    $('.ozon-slide-block').slideUp('fast');

    $('.slide').on('click', function () {
        $('.shop-slide-block').slideUp('slow');
        $('.shop-slide-block').removeClass('active');
        $('.ozon-slide-block').slideUp('slow');

        if ($(this).next('.shop-slide-block').css('display') !== 'block') {
            $(this).next('.shop-slide-block').addClass("active");
            let ozonId = $(this).data('ozon_id');
            let catId = $(this).data('category_id');

            var obj = {
                ozon_id: ozonId,
                cat_id: catId,
                flag: $(this).hasClass('js-ozon-categories')?'category':'set'
            };

            $('.ozon-slide-block[data-cat_ozon_id=' + ozonId + ']').slideDown('slow');
            $(this).next('.shop-slide-block').slideDown('slow');
            let container = jQuery('.ozon-slide-block[data-cat_ozon_id=' + obj.ozon_id + ']');
            container.html('');
            $.ajax({
                type: "POST",
                url: '?plugin=ozonius&module=api&action=attrAllView',
                data: JSON.stringify(obj),
                //contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (res) {

                    $.each(res, function (index, values) {
                        container.append($(
                            '<h4>Обязательные атрибуты</h4>'
                        ));
                        $.each(values.requiredAttributes, function (index, value) {
                            var htmlSatisfy = '';
                            var htmlSatisfyFormat = '';

                            if(values.satisfy && values.satisfy.indexOf(value.id) != -1 || value.name =="Изображение"){

                                htmlSatisfy = '<p class="satisfy">Соответствует имени</p>';
                            }
                            else {
                                htmlSatisfy = '<p class="not-satisfy">Не соответствует имени</p>';
                            }
                            if(values.satisfy && values.satisfy.indexOf(value.id) != -1){
                                if(value.is_collection == 0 && value.dictionary_id > 0){
                                    if(values.is_select && values.is_select.indexOf(value.id) != -1){
                                        htmlSatisfyFormat = '<p class="satisfy">Соответствует формату</p>';
                                    }
                                    else {
                                        htmlSatisfyFormat = '<p class="not-satisfy">Не соответствует формату</p>';
                                    }
                                }
                                if(value.is_collection == 1 && value.dictionary_id > 0){
                                    if(values.is_multiple && values.is_multiple.indexOf(value.id) != -1){
                                        htmlSatisfyFormat = '<p class="satisfy">Соответствует формату</p>';
                                    }
                                    else {
                                        htmlSatisfyFormat = '<p class="not-satisfy">Не соответствует формату</p>';
                                    }
                                }
                            }


                        var html, exa = '';
                        if (value.is_collection == 1) {
                            html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Выбор нескольких значении из списка".</p></div>';
                        }
                        if (value.is_collection == 0 && value.dictionary_id > 0) {
                            html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Выбор одного значения из списка".</p></div>';
                        }
                        if (value.is_collection == 0 && value.dictionary_id == 0) {
                            if (value.id != 4194) {
                                html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Строка текста".</p></div>';
                            }
                        }
                        if(value.id != 85){
                                exa = '<button data-cat_id = "' + ozonId + '" data-attr_id = "' + value.id + '" class="example-js btn btn-outline-success">Показать примеры значений</button>';
                        }

                        container.append($(
                            '<li class="al-attr d-flex flex-row align-baseline">' +
                            '<span class="al-name col-4">' +
                            value.name +  '(' + value.id + ')' + '<br>' +
                            '<p class="required">Обязательный</p>' +
                            htmlSatisfy +
                            htmlSatisfyFormat +
                            '</span>' +
                            '<span class="al-description col-8">' + value.description + '<br>' +
                            exa +
                            html +
                            '<div class="example-value"></div>' +
                            '</span>' +
                            '</li>'
                        ));
                    });

                        container.append($(
                            '<button data-id = "'+ ozonId +'" class="optional-js btn btn-outline-success">' +
                            'Показать все не обязательные атрибуты'+
                            '</button>'
                        ));
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    console.log('jqXHR:');
                    console.log(jqXHR);
                    console.log('textStatus:');
                    console.log(textStatus);
                    console.log('errorThrown:');
                    console.log(errorThrown);
                }
            });
        }


    });

    $(document).on('click','button.example-js',function(){
        var attrId = $(this).data('attr_id');
        var attrCatId = $(this).data('cat_id');
        var obj = {
            attr_id: attrId,
            cat_id: attrCatId,
        };
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=attrExampleValues',
            data: JSON.stringify(obj),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                let container = jQuery('.example-js[data-cat_id=' + attrCatId + '][data-attr_id=' + attrId + ']').closest('.al-attr').find(".example-value");

                if (res.cat_value == false) {
                    container.append(jQuery('<div>Примеров не найдено</div>'));
                } else {
                    $.each(res.cat_value, function (index, value) {
                        //console.log(value.length > 10);

                        //container.find('div:gt(10)').hide();


                            container.append(jQuery('<div>' + value.value + '</div>'));

                    });
                    container.each(function(){
                        const show = 10;
                        let spans = $(this).children("div");

                        if( spans.length > show){
                            spans.slice(show).hide();

                            const tpl = '<a href="#" class="add">Показать еще [num]</a>';
                            let a = tpl.replace("[num]", spans.length - show);
                            $(a).appendTo(this);
                        }
                    });

                    container.on('click', '.add', function(e){
                        e.preventDefault();
                        $(this).hide()
                            .prevAll('div')
                            .show();
                    });
                }


            },
            beforeSend: function () {
                jQuery('.example-js[data-cat_id=' + attrCatId + '][data-attr_id=' + attrId + ']').attr("disabled", "disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    });

    $(document).on('click','.optional-js',function(){
        var catId = $(this).data('id');

        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=attrOptValues',
            data: JSON.stringify(catId),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                let container = jQuery('.ozon-slide-block[data-cat_ozon_id=' + catId + ']');

                    $('.optional-js').hide();
                container.append($(
                    '<h4>Не обязательные атрибуты</h4>'
                ));
                    $.each(res, function (index, value) {
                        var html, exa = '';
                        if (value.is_collection == 1) {
                            html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Выбор нескольких значении из списка".</p></div>';
                        }
                        if (value.is_collection == 0 && value.dictionary_id > 0) {
                            html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Выбор одного значения из списка".</p></div>';
                        }
                        if (value.is_collection == 0 && value.dictionary_id == 0) {
                            if (value.id != 4194) {
                                html = '<div style="color: #0d86ff"><p style="margin-top: 15px;">Тип: "Текст".</p><p>Формат характеристики: "Строка текста".</p></div>';
                            }
                        }
                        if(value.id != 85){
                                exa = '<button data-cat_id = "'+ catId +'" data-attr_id = "'+ value.id +'" class="example-js btn btn-outline-success">Показать примеры значений</button>';
                        }
                        container.append($(
                            '<li class="al-attr d-flex flex-row align-baseline">' +
                            '<span class="al-name col-4">' +
                            value.name +  '(' + value.id + ')' + '<br>' +
                             '</span>' +
                            '<span class="al-description col-8">' + value.description + '<br>' +
                            exa +
                            html +
                            '<div class="example-value"></div>' +
                            '</span>' +
                            '</li>'
                        ));
                    });
            },
            beforeSend: function () {
                jQuery('.optional-js[data-id=' + catId + ']').attr("disabled", "disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    });


});
var App = {
    orderConfirm: function (data) {
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=orderConfirm',
            data: JSON.stringify(data),
            dataType: "json",
            success: function(res){
                location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    },
    cancel: function (data) {
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=orderCancel',
            data: JSON.stringify(data),
            dataType: "json",
            success: function(res){
                if(res.length != 0){
                location.reload();
                }
            },


            error: function(jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
    },
    print: function (data) {
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=orderPrintPDF',
            data: JSON.stringify(data),
            success: function(res){
                if(res == 'Отправление не готово к маркировке, повторите попытку позже'){
                    alert(res);
                }
                else {
                    printJS({printable: res, type: 'pdf', base64: true})}
            },
            error: function(jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
        return false;
    },
    addTrack: function (data) {
        $.ajax({
            type: "POST",
            url: '?plugin=ozonius&module=api&action=addTrackNumber',
            data: JSON.stringify(data),
            success: function(res){
                if(res[0]['result'] == true){
                    alert("Трек номер успешно добавлен.");
                }
                else {
                    alert("Ошибка при отправлении: " + res[0]['error']);
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {

                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
        return false;
    }

};

$("#order_success_js").submit(function(){
        var postingNumber = $(this).data('posting_number');
        App.orderConfirm(postingNumber);
    return false;
    });
$("#order_cancelled_js").submit(function(){
        var statusNumber = $(this).data('status_ozon');
        var postingNumber = $(this).data('posting_number');
        var anotherText = $('.another_reason').val();
        var obj = {
            status: statusNumber,
            posting: postingNumber,
            another: anotherText
        };

        App.cancel(obj);

    });

    $('.status-cancell').on('change', function () {
        $("#order_cancelled_js").data("status_ozon", this.value);
        if (this.value == '402') {
            $(".another_reason").css("display", 'block');
        } else {
            $(".another_reason").css("display", 'none');
        }
    });
//$('#add-track-ozon').on('click', function () {
$(".add-track-ozon").click(function(){
   $('#order_add_track').show()
});

    $("#order_print_js").submit(function(){
        var postingNumber = $(this).data('posting_number');
        App.print(postingNumber);
        return false;
    });
$("#order_add_track").submit(function(){
    let postingNumber = $(this).data('posting_number');
    let trackNumber = $('.number_track').val();
    let obj = {
        posting: postingNumber,
        track: trackNumber
    };
    App.addTrack(obj);
    return false;
});




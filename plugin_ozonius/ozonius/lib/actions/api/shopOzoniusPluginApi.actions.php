<?php
/**
 * Created by PhpStorm.
 * User: Wesma13
 * Date: 28.08.2019
 * Time: 17:33
 */

class shopOzoniusPluginApiActions extends waActions
{
    protected function preExecute()
    {
        parent::preExecute();
    }

    public function getSettingPlugin($setting_name = '')
    {
        if (!empty($setting_name)) {
            return wa('shop')->getPlugin('ozonius')->getSettings($setting_name);
        } else {
            return wa('shop')->getPlugin('ozonius')->getSettings();
        }
    }

    public function helloAction()
    {
        $id = waRequest::param('id');
        echo "Ложки не существует" . $id;
    }

    protected function _getExistsMatches()
    {
        $categoryModel = new shopCategoryModel();
        $existsMatches = array();
        $existsMatchesQR = $categoryModel->query("SELECT id, shop_category_id, ozon_category_id FROM " . shopOzoniusPlugin::TABLE_CATEGORY)->fetchAll();

        foreach ($existsMatchesQR as $key => $match) {
            $existsMatches[$match['shop_category_id']] = array(
                'id' => $match['id'],
                'ozon_category_id' => $match['ozon_category_id']
            );
        }

        return $existsMatches;
    }

    protected function _getExistsMatchesSets()
    {
        //$categoryModel = new shopCategoryModel();
        $setModel = new shopSetModel();
        $existsMatches = array();
        $existsMatchesQR = $setModel->query("SELECT id, shop_sets_id, ozon_category_id FROM " . shopOzoniusPlugin::TABLE_SETS)->fetchAll();

        foreach ($existsMatchesQR as $key => $match) {
            $existsMatches[$match['shop_sets_id']] = array(
                'id' => $match['id'],
                'ozon_category_id' => $match['ozon_category_id']
            );
        }

        return $existsMatches;
    }

    public function removeAllAction()
    {
        $response = array();
        $categoryModel = new shopCategoryModel();

        try {
            $existingMatches = $this->_getExistsMatches();

            if (!empty($existingMatches)) {
                foreach ($existingMatches as $shopCategoryId => $data) {
                    $productsExists = $categoryModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozonius_category_id = {$data['id']}")->fetch();
                    if ($productsExists['count']) { // если по такой категории уже загружены товары
                        $deleteErrors[$shopCategoryId] = $productsExists['count'];
                    } else { // если товары еще не загружены
                        $result = $categoryModel->query("DELETE FROM " . shopOzoniusPlugin::TABLE_CATEGORY . " WHERE id = {$data['id']}");
                        if ($result) {
                            $response['deleted_categories'][] = $shopCategoryId;
                        }
                    }
                }
            }

            if (isset($response['deleted_categories']) && count($response['deleted_categories']) > 0) {
                $count = count($response['deleted_categories']);
                $response['messages']['deleted'] = "Успешно удалено {$count} категорий!";
            }

            if (isset($deleteErrors) && !empty($deleteErrors)) {
                foreach ($deleteErrors as $shopId => $count) {
                    $name = $categoryModel->getById($shopId)['name'];
                    $response['messages']['delete_errors'][] = "Невозможно удалить категорию {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте категорию";
                }
            }

        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");

            $response['messages']['main'] = "Ошибка при удалении. Перезагрузите страницу и выполните действие еще раз.";
        }

        $this->sendApiResponse($response);
    }



    public function setProductsAddAction()
    {
        $model = new waModel();

        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $items = $plugin->getList();

        foreach ($items as $item) {

            $offer = addslashes($item['offer_id']);
            $productsExists = $model->query("SELECT ozon_offer_id FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozon_offer_id = '{$offer}'")->fetch()['ozon_offer_id'];
            if($productsExists)continue;

            $sql = "SELECT `id`, `product_id` FROM shop_product_skus WHERE `sku` = '{$item['offer_id']}'";
            $result = $model->query($sql)->fetchAll();

            $productsOzonExists = $model->query("SELECT id FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE shop_product_sku_id = '{$result[0]['id']}'")->fetch()['id'];

            if($productsOzonExists)continue;

            if ($result){
                $insertQuery = "INSERT  INTO `shop_ozonius_products` (shop_prod_manually,shop_product_id,shop_product_sku_id,ozon_offer_id,ozon_product_id,create_task_id)
                                    VALUES (i:prod,i:product_id,i:sku,s:offer,i:task,i:task_id)";

                try {
                    $model->query($insertQuery, array(
                        'prod' => 1,
                        'product_id' => $result[0]['product_id'],
                        'sku' => $result[0]['id'],
                        'offer' => $item['offer_id'],
                        'task' => $item['product_id'],
                        'task_id' => $item['product_id']
                    ));
                } catch (Error $error) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log($error->getMessage(), 'shopOzonius.log');
                }

            }
        }

    }

    public function setsRemoveAllAction()
    {
        $response = array();
        $setModel = new shopSetModel();

        try {
            $existingMatches = $this->_getExistsMatchesSets();

            if (!empty($existingMatches)) {
                foreach ($existingMatches as $shopSetId => $data) {
                    $productsExists = $setModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozonius_sets_id = {$data['id']}")->fetch();
                    if ($productsExists['count']) { // если по такой категории уже загружены товары
                        $deleteErrors[$shopSetId] = $productsExists['count'];
                    } else { // если товары еще не загружены
                        $result = $setModel->query("DELETE FROM " . shopOzoniusPlugin::TABLE_SETS . " WHERE id = {$data['id']}");
                        if ($result) {
                            $response['deleted_sets'][] = $shopSetId;
                        }
                    }
                }
            }

            if (isset($response['deleted_sets']) && count($response['deleted_sets']) > 0) {
                $count = count($response['deleted_sets']);
                $response['messages']['deleted'] = "Успешно удалено {$count} списков!";
            }

            if (isset($deleteErrors) && !empty($deleteErrors)) {
                foreach ($deleteErrors as $shopId => $count) {
                    $name = $setModel->getById($shopId)['name'];
                    $response['messages']['delete_errors'][] = "Невозможно удалить список {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте";
                }
            }

        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");

            $response['messages']['main'] = "Ошибка при удалении. Перезагрузите страницу и выполните действие еще раз.";
        }

        $this->sendApiResponse($response);
    }

    public function commitAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin_enabled = $this->getSettingPlugin('status_plugin');
        $categoryModel = new shopCategoryModel();
        foreach ($data['matches'] as $key => $attr) {
            $categoryModel->query("UPDATE `shop_ozonius_category` SET `markup_price` = '{$attr['markup_ozon']}',`fix_price` = '{$attr['fix']}' WHERE `shop_category_id` = {$key}");
        }

        try {
            $existingMatches = $this->_getExistsMatches();

            if ($data['matches']) {

                foreach ($data['matches'] as $shopCategoryId => $ozonCategoryId) {

                    $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
                    $ozonName = $plugin->getCategory($ozonCategoryId['ozon'])[0]['title'];

                    if (array_key_exists($shopCategoryId, $existingMatches)) { // соответствие уже существует
                        if ($existingMatches[$shopCategoryId]['ozon_category_id'] != $ozonCategoryId['ozon']) { // изменилась категория на озон
                            $productsExists = $categoryModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozonius_category_id = {$existingMatches[$shopCategoryId]['id']}")->fetch();
                            if ($productsExists) { // если по такой категории уже загружены товары
                                $updateErrors[$shopCategoryId] = $productsExists['count'];
                                unset($existingMatches[$shopCategoryId]);
                                //$response['messages']['error'] = "Невозможно переназначить категорию  для которой уже загружены товары на Ozon, удалите товары с торговой площадки, затем переназначте категорию"
                            } else { // если товары еще не загружены
                                $result = $categoryModel->query("UPDATE " . shopOzoniusPlugin::TABLE_CATEGORY . " SET ozon_category_id = {$ozonCategoryId['ozon']}, ozon_name = '{$ozonName}' WHERE shop_category_id = {$shopCategoryId}");
                                if ($result) {
                                    $response['categories'][] = $shopCategoryId;
                                    unset($existingMatches[$shopCategoryId]);
                                }
                            }
                        } else { // ничего не изменилось
                            unset($existingMatches[$shopCategoryId]);
                        }
                    } else { // такого соответствия еще нет
                        $result = $categoryModel->query("INSERT INTO " . shopOzoniusPlugin::TABLE_CATEGORY . " (shop_category_id,ozon_category_id,ozon_name,markup_price) VALUES ({$shopCategoryId},{$ozonCategoryId['ozon']},'{$ozonName}','{$ozonCategoryId['markup_ozon']}')");
                        if ($result) {
                            $response['categories'][] = $shopCategoryId;
                        }
                    }
                }
            }

            if (!empty($existingMatches)) {
                foreach ($existingMatches as $shopCategoryId => $data) {
                    $productsExists = $categoryModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozonius_category_id = {$data['id']}")->fetch();
                    if ($productsExists['count']) { // если по такой категории уже загружены товары
                        $deleteErrors[$shopCategoryId] = $productsExists['count'];
                    } else { // если товары еще не загружены
                        $result = $categoryModel->query("DELETE FROM " . shopOzoniusPlugin::TABLE_CATEGORY . " WHERE id = {$data['id']}");
                        if ($result) {
                            $response['deleted_categories'][] = $shopCategoryId;
                        }
                    }
                }
            }

            if (isset($response['categories']) && count($response['categories']) > 0) {
                $count = count($response['categories']);
                $response['messages']['count'] = "Успешно сопоставлено {$count} категорий.";
                $response['messages']['main'] = "Сопоставление завершено!";
            } else {
                $response['messages']['count'] = "Успешно сопоставлено 0 категорий.";
                $response['messages']['main'] = "Сопоставление завершено!";
            }

            if (isset($response['deleted_categories']) && count($response['deleted_categories']) > 0) {
                $count = count($response['deleted_categories']);
                $response['messages']['deleted'] = "Успешно удалено {$count} категорий!";
            }

            if (isset($deleteErrors) && !empty($deleteErrors)) {
                foreach ($deleteErrors as $shopId => $count) {
                    $name = $categoryModel->getById($shopId)['name'];
                    $response['messages']['delete_errors'][] = "Невозможно удалить категорию {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте категорию";
                }
            }

            if (isset($updateErrors) && !empty($updateErrors)) {
                foreach ($updateErrors as $shopId => $count) {
                    $name = $categoryModel->getById($shopId)['name'];
                    $response['messages']['update_errors'][] = "Невозможно переназначить категорию {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте категорию";
                }
            }
        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");

            $response['messages']['main'] = "Ошибка при сопоставлении. Перезагрузите страницу и выполните действие еще раз.";
        }

        $this->sendApiResponse($response);
    }

    public function commitSetsAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin_enabled = $this->getSettingPlugin('status_plugin');
        $setModel = new shopSetModel();

        foreach ($data['matches'] as $key => $attr) {

            $setModel->query("UPDATE `shop_ozonius_sets` SET `markup_price` = '{$attr['markup_ozon']}',`fix_price` = '{$attr['fix']}' WHERE `shop_sets_id` = '{$key}'");
        }

        try {
            $existingMatches = $this->_getExistsMatchesSets();

            if ($data['matches']) {

                foreach ($data['matches'] as $shopSetsId => $ozonCategoryId) {

                    $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
                    $ozonName = $plugin->getCategory($ozonCategoryId['ozon'])[0]['title'];

                    if (array_key_exists($shopSetsId, $existingMatches)) { // соответствие уже существует
                        if ($existingMatches[$shopSetsId]['ozon_category_id'] != $ozonCategoryId['ozon']) { // изменилась категория на озон
                            $productsExists = $setModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_SETS . " WHERE ozonius_category_id = {$existingMatches[$shopSetsId]['id']}")->fetch();
                            if ($productsExists) { // если по такой категории уже загружены товары
                                $updateErrors[$shopSetsId] = $productsExists['count'];
                                unset($existingMatches[$shopSetsId]);
                                //$response['messages']['error'] = "Невозможно переназначить категорию  для которой уже загружены товары на Ozon, удалите товары с торговой площадки, затем переназначте категорию"
                            } else { // если товары еще не загружены
                                $result = $setModel->query("UPDATE " . shopOzoniusPlugin::TABLE_SETS . " SET shop_sets_name = '{$ozonCategoryId['name']}', ozon_category_id = {$ozonCategoryId['ozon']}, ozon_name = '{$ozonName}' WHERE shop_sets_id = '{$shopSetsId}'");
                                if ($result) {
                                    $response['sets'][] = $shopSetsId;
                                    unset($existingMatches[$shopSetsId]);
                                }
                            }
                        } else { // ничего не изменилось
                            unset($existingMatches[$shopSetsId]);
                        }
                    } else { // такого соответствия еще нет

                        $result = $setModel->query("INSERT INTO " . shopOzoniusPlugin::TABLE_SETS . " (`shop_sets_id`,`shop_sets_name`, `ozon_category_id`, `ozon_name`, `markup_price`) VALUES ('{$shopSetsId}', '{$ozonCategoryId['name']}', {$ozonCategoryId['ozon']},'{$ozonName}','{$ozonCategoryId['markup_ozon']}')");

                        if ($result) {
                            $response['sets'][] = $shopSetsId;
                        }
                    }
                }
            }

            if (!empty($existingMatches)) {
                foreach ($existingMatches as $shopSetsId => $data) {
                    $productsExists = $setModel->query("SELECT count(*) as `count` FROM " . shopOzoniusPlugin::TABLE_PRODUCTS . " WHERE ozonius_sets_id = {$data['id']}")->fetch();
                    if ($productsExists['count']) { // если по такой категории уже загружены товары
                        $deleteErrors[$shopSetsId] = $productsExists['count'];
                    } else { // если товары еще не загружены
                        $result = $setModel->query("DELETE FROM " . shopOzoniusPlugin::TABLE_SETS . " WHERE id = {$data['id']}");
                        if ($result) {
                            $response['deleted_sets'][] = $shopSetsId;
                        }
                    }
                }
            }

            if (isset($response['sets']) && count($response['sets']) > 0) {
                $count = count($response['sets']);
                $response['messages']['count'] = "Успешно сопоставлено {$count} списков.";
                $response['messages']['main'] = "Сопоставление завершено!";
            } else {
                $response['messages']['count'] = "Успешно сопоставлено 0 списков.";
                $response['messages']['main'] = "Сопоставление завершено!";
            }

            if (isset($response['deleted_sets']) && count($response['deleted_sets']) > 0) {
                $count = count($response['deleted_sets']);
                $response['messages']['deleted'] = "Успешно удалено {$count} списков!";
            }

            if (isset($deleteErrors) && !empty($deleteErrors)) {
                foreach ($deleteErrors as $shopId => $count) {
                    $name = $setModel->getById($shopId)['name'];
                    $response['messages']['delete_errors'][] = "Невозможно удалить список {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте список";
                }
            }

            if (isset($updateErrors) && !empty($updateErrors)) {
                foreach ($updateErrors as $shopId => $count) {
                    $name = $setModel->getById($shopId)['name'];
                    $response['messages']['update_errors'][] = "Невозможно переназначить список {$name} для которой уже загружено {$count} товаров на Ozon, удалите товары с торговой площадки, затем переназначте список";
                }
            }
        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");

            $response['messages']['main'] = "Ошибка при сопоставлении. Перезагрузите страницу и выполните действие еще раз.";
        }

        $this->sendApiResponse($response);
    }

    public function attrValuesAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        if ($data['attr_id'] != "") {
            $response['cat_value'] = $plugin->getValuesCategorie($data['cat_id'], $data['attr_id']);
        }


        $this->sendApiResponse($response);
    }

    public function attrExampleValuesAction(){
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        if ($data['attr_id'] != "") {
            $response['cat_value'] = $plugin->getValuesExample($data['cat_id'], $data['attr_id']);
        }
        $this->sendApiResponse($response);
    }
    public function attrOptValuesAction()
    {
        $data = $this->_getInputData();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $response = $plugin->getOptionalAttributeList($data);
        $this->sendApiResponse($response);
    }
    public function matchedCategoriesAction()
    {
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $response['categories'] = $plugin->getMatchedCategories();

        $this->sendApiResponse($response);
    }

    public function matchedSetsAction()
    {
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $response['sets'] = $plugin->getMatchedSets();

        $this->sendApiResponse($response);
    }

    public function prepareCategoryAction()
    {
        $data = $this->_getInputData();
        $response = array('errors' => array(),'errorsExists'=> array());

        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        if (isset($data['categoryId'])) {
            $prepareData = $plugin->prepareCategoryProducts($data['categoryId'], $data['marNum'],$data['ozonCatId']);
        } else {
            $prepareData = $plugin->prepareSetProducts($data['setId'], $data['marNum'],$data['ozonCatId']);
        }


        if (!empty($prepareData)) {
            $response['validCount'] = $prepareData['valid'];


            if (!empty($prepareData['invalid'])) {
                foreach ($prepareData['invalid'] as $product) {
                    $data = array(
                        'name' => $product['name'],
                        'url' => $product['url'],
                        'message' => $product['message']
                    );
                    array_push($response['errors'], $data);
                }
            }

        }

        $this->sendApiResponse($response);
    }

    public function sendCategoryAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        if (isset($data['categoryId'])) {
            $file = $plugin->getPluginDataPath() . "categories/category_{$data['categoryId']}.json";
        } else {
            $file = $plugin->getPluginDataPath() . "categories/category_{$data['setId']}.json";
        }


        if (file_exists($file)) {
            $itemsFromFile = json_decode(file_get_contents($file), true);
        }

        if (!isset($itemsFromFile) || empty($itemsFromFile)) {
            $response['messages'] = "Отсутствуют данные для отправки.";
            $this->sendApiResponse($response);
            return false;
        }

        if (count($itemsFromFile['items']) < 100) {
            $count = count($itemsFromFile['items']);
            $result = $plugin->sendCategoryItems($itemsFromFile['items']);

            if (isset($result) && isset($result['task_id'])) {
                if (isset($data['categoryId'])) {
                    $plugin->reflectTaskInfo($result['task_id'], $data['categoryId']);
                    $plugin->reflectItemsInfo($itemsFromFile['items'], $data['categoryId'], $result['task_id']);
                } else {
                    $plugin->reflectTaskSetInfo($result['task_id'], $data['setId']);
                    $plugin->reflectItemsSetInfo($itemsFromFile['items'], $data['setId'], $result['task_id']);
                }

                $response['messages'] = "Данные о {$count} товарах успешно отправлены!";
                $response['count'] = $count;
            } else {
                $response['messages'] = "Во время отправки данных произошла ошибка на стороне API, повторите попытку позже!";
            }
        } else {
            $chunks = array_chunk($itemsFromFile['items'], 99);
            foreach ($chunks as $key => $items) {
                $count = count($items);
                $result = $plugin->sendCategoryItems($items);
                if (isset($result) && isset($result['task_id'])) {
                    if (isset($data['categoryId'])) {
                        $plugin->reflectTaskInfo($result['task_id'], $data['categoryId']);
                        $plugin->reflectItemsInfo($itemsFromFile['items'], $data['categoryId'], $result['task_id']);
                    } else {
                        $plugin->reflectTaskSetInfo($result['task_id'], $data['setId']);
                        $plugin->reflectItemsSetInfo($itemsFromFile['items'], $data['setId'], $result['task_id']);
                    }
                    $response['messages'][$key]['message'] = "Данные о {$count} товарах успешно отправлены!";
                } else {
                    $response['messages'][$key]['message'] = "Во время отправки данных произошла ошибка на стороне API, повторите попытку позже!";
                    $response['messages'][$key]['count'] = $count;
                }
            }
        }

        $this->sendApiResponse($response);
    }
    public function getProductsInfoGetAction()
    {
        //$data = $this->_getInputData();
        $data = waRequest::get();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozoniusCatId = $data['categoryId'];
        $page = $data['page'];

            if ($data['select'] == 'category') {
                $response['products'] = $plugin->getCategoryOzonProducts($ozoniusCatId, $page);
            }
            if ($data['select'] == 'sets') {
                $response['products'] = $plugin->getSetsOzonProducts($ozoniusCatId, $page);
            }
            if ($data['select'] == 'manually') {
                $response['products'] = $plugin->getManuallyOzonProducts($ozoniusCatId, $page);
            }

        $this->sendApiResponse($response);
    }
    public function getProductsCountGetAction(){
        $data = waRequest::get();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $countProd = 0;
        $limit = 30;
            $model = new waModel();
            if($data['select'] == 'category'){
            $sql = "SELECT COUNT(*) FROM shop_ozonius_products WHERE `ozonius_category_id` = '{$data['categoryId']}'";
            $countProd = $model->query($sql)->fetch()[0];

            }
        if($data['select'] == 'sets'){
            $sql = "SELECT COUNT(*) FROM shop_ozonius_products WHERE `ozonius_sets_id` = '{$data['categoryId']}'";
            $countProd = $model->query($sql)->fetch()[0];
        }
        if($data['select'] == 'manually'){
            $sql = "SELECT COUNT(*) FROM shop_ozonius_products WHERE `shop_prod_manually` = '{$data['categoryId']}'";
            $countProd = $model->query($sql)->fetch()[0];

        }
        $limitSetting = $plugin->getCountPaginationFromSettings();

        if($limitSetting && $limitSetting !==0){
            $limit = $limitSetting;
        }
        else{
            $limit = $limit;
        }
        $res = [
            'count'=>$countProd,
            'select' => $data['select'],
            'limit' => $limit,
        ];
        $this->sendApiResponse($res);

        //$ozoniusCatId = $data['categoryId'];
    }
    public function getProductsInfoAction()
    {
        $data = $this->_getInputData();

        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozoniusCatId = $data['categoryId'];

        $response['products'] = $plugin->getCategoryOzonProducts($ozoniusCatId);
        $response['productsSet'] = $plugin->getSetsOzonProducts($ozoniusCatId);
        $response['productsManually'] = $plugin->getManuallyOzonProducts($ozoniusCatId);

        $this->sendApiResponse($response);
    }

    public function getProductsSetsInfoAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozoniusCatId = (int)$data['categoryId'];
        $response['products'] = $plugin->getCategoryOzonProducts($ozoniusCatId);

        $this->sendApiResponse($response);
    }

    public function productActivateAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozonProductIds = $data['data'];

        if (!empty($ozonProductIds)) {
            $response = $plugin->activateProducts($ozonProductIds);
        }

        $this->sendApiResponse($response);
    }

    public function productDeactivateAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozonProductIds = $data['data'];

        if (!empty($ozonProductIds)) {
            $response = $plugin->deactivateProducts($ozonProductIds);
        }

        $this->sendApiResponse($response);
    }

    public function productDeleteAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $skuIds = $data['data'];

        if (!empty($skuIds)) {
            $response = $plugin->deleteProducts($skuIds);
        }

        $this->sendApiResponse($response);
    }

    public function productUpdatePriceAction()
    {
        $data = $this->_getInputData();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozonProductIds = $data['data'];

        $response = $plugin->updatePrices($ozonProductIds);


        $this->sendApiResponse($response);
    }

    public function productUpdateStockAction()
    {
        $data = $this->_getInputData();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $ozonProductIds = $data['data'];
        $response = $plugin->updateStocks($ozonProductIds);

        $this->sendApiResponse($response);
    }


    public function productUpdateStockCronAction()
    {
        $data = $this->_getInputData();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $ozonProductIds = $data['data'];
        $response = $plugin->updateStocks($ozonProductIds);
        $this->sendApiResponse($response);
    }

    public function searchProductsAction(){
        $data = $this->_getInputData();

        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $response = $plugin->searchProduct($data);
        $this->sendApiResponse($response);
    }
    public function productUpdateDescriptionAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $lastId = array_pop($data['data']);
        $response = $plugin->updateProductCards($data['data'],$lastId['markup'][0],$lastId['ozon_cat'][0]);
        $this->sendApiResponse($response);
    }
    public function productUpdateDescriptionManuallyAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $ozonProductId = $data['data'][0];
        $skuProduct = $data['data'][1][0];
        $response = $plugin->updateProductCardsManually($ozonProductId, $skuProduct);

        $this->sendApiResponse($response);
    }

    protected function _getInputData()
    {
        if (!is_null($GLOBALS['HTTP_RAW_POST_DATA'])) {
            $raw = !empty($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : null;
        } else {
            $raw = implode("", file('php://input'));
        }
        if (preg_match("#csrf#", $raw)) {
            return json_decode(explode("&", $raw)[0], true);
        } else {
            return json_decode($raw, true);
        }
    }

    protected function sendApiResponse($data)
    {
        $this->getResponse()->addHeader('Content-type', 'application/json; charset=utf-8');
        $this->getResponse()->sendHeaders();
        $options = 0;
        if (defined('JSON_PRETTY_PRINT')) {
            $options &= JSON_PRETTY_PRINT;
        }

        if (defined('JSON_UNESCAPED_UNICODE')) {
            $options &= JSON_UNESCAPED_UNICODE;
        }

        if (false && version_compare(PHP_VERSION, '5.4', '>=')) {
            print json_encode($data, $options);
        } else {
            print json_encode($data);
        }
    }

    public function orderConfirmAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $ozonPostingId = (string)$data;
        $response = $plugin->orderConfirm($ozonPostingId);
        $this->sendApiResponse($response);
    }

    public function orderCancelAction()
    {
        $data = $this->_getInputData();
        $response = array();

        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));

        $response = $plugin->orderCancel($data);
        $this->sendApiResponse($response);
    }

    public function orderPrintPDFAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        $response = $plugin->orderPrintPDF($data);
        echo $response;
        //$this->sendApiResponse($response);
    }

    public function addTrackNumberAction()
    {
        $data = $this->_getInputData();
        $response = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
        if (!empty($data['track']) && !empty($data['posting'])) {
            $response = $plugin->addTrack($data);
        }


        $this->sendApiResponse($response);
    }

    /** ----- mock ----- **/

    public function mockOrdersAction()
    {
        echo file_get_contents("mock_orders.json");
    }

    public function mockOrderItemsAction()
    {
        $input = $this->_getInputData();
        $orderId = $input['id'];
        echo file_get_contents("mock_order_items_{$orderId}.json");
    }

    /** ----- ---- ----- **/

    /** ----- view attrs all ------ **/

    public  function attrAllViewAction(){
        $dataArr = $this->_getInputData();
        $matchedCategories = array();
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop', 'id' => 'ozonius'));
                $matchedCategories[$dataArr['cat_id']]['requiredAttributes'] = $plugin->getRequiredAttributeList($dataArr['ozon_id']);
                if($dataArr['flag'] == 'category') {
                    $matchedCategoriesShop[$dataArr['cat_id']]['shopAttributes'] = $plugin->getCategoryAttributes($dataArr['cat_id']);
                }
                else{
                    $matchedCategoriesShop[$dataArr['cat_id']]['shopAttributes'] = $plugin->getSetAttributes($dataArr['cat_id']);
                }
                if ($matchedCategories[$dataArr['cat_id']]['requiredAttributes']) {
                    foreach ($matchedCategories[$dataArr['cat_id']]['requiredAttributes'] as $key => $attr) {

                        foreach ($matchedCategoriesShop[$dataArr['cat_id']]['shopAttributes'] as $typeName => $data) {

                            if (array_key_exists($attr['name'], $data)) {

                                $matchedCategories[$dataArr['cat_id']]['satisfy'][] = $attr['id'];

                                    if (empty($attr['is_collection']) && $attr['dictionary_id'] > 0) {
                                        if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 0) {

                                        $matchedCategories[$dataArr['cat_id']]['is_select'][] = $attr['id'];
                                    }
                                }
                                    if ($attr['is_collection'] && $attr['dictionary_id'] > 0) {
                                        if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 1) {
                                        $matchedCategories[$dataArr['cat_id']]['is_multiple'][] = $attr['id'];
                                    }
                                }

                            }
                        }
                    }
                }
        $this->sendApiResponse($matchedCategories);
    }

}
<?php
    /**
     * Created by PhpStorm.
     * User: Wesma13
     * Date: 29.08.2019
     * Time: 18:37
     */

    class shopOzoniusPluginBackendActions extends waViewActions
    {
        public static $parentsOzon = array();
        public static $parentsShop = array();
        public static $shopCategories = array();
        public static $shopSets = array();
        protected static $categoriesMatched = array();
        protected static $setsMatched = array();

        protected $plugin;
        protected $categoryModel;
        protected $featuresModel;

        public function __construct(waSystem $system = null)
        {
            parent::__construct($system);
            $this->plugin = new shopOzoniusPlugin(array('app_id' => 'shop','id' => 'ozonius'));
            $this->categoryModel = new shopCategoryModel();
            $this->featuresModel = new shopFeatureModel();
        }

        public function preExecute()
        {
            $layout = new shopOzoniusBackendLayout();
            $this->setLayout($layout);

            $this->addCss(array(
                'select2.min.css',
                'bootstrap.min.css',
                'style.css',
            ));

            $this->addJs(array(
                //'jquery.js',
                'select2.full.min.js',
                'bootstrap.min.js',
                'main.js',
            ));

            $this->initSidebar();
        }

        public function initSidebar()
        {
            $plugin_root = wa()->getAppPath($this->getPluginRoot(), 'shop');
            $this->view->assign('action', waRequest::get('action', 'default'));

            $sidebar = $this->view->fetch($plugin_root.'/templates/Sidebar.html');
            $this->view->assign('sidebar', $sidebar);
        }

        public function defaultAction()
        {

        }

        public function productsExportAction()
        {
            $this->addJs(array(
                'products.js',
            ));

            $categories = $this->plugin->getMatchedCategories();
            $sets = $this->plugin->getMatchedSets();
            //$productManually = $this->plugin->getMatchedManually();

            foreach ($categories as $shopCategoryId => $data) {
                $categories[$shopCategoryId]['countProducts'] = $this->plugin->getCategoryShopProductsCount($shopCategoryId);
                $categories[$shopCategoryId]['countOzonProducts'] = $this->plugin->getCategoryOzonProductsCount($shopCategoryId);
            }

            $this->view->assign('categories',$categories);

            foreach ($sets as $shopSetId => $data) {
                $sets[$shopSetId]['countProducts'] = $this->plugin->getSetShopProductsCount($shopSetId);
                $sets[$shopSetId]['countOzonProducts'] = $this->plugin->getSetOzonProductsCount($shopSetId);
            }

            $this->view->assign('sets',$sets);
        }

        public function productsAction()
        {
            $this->addJs(array(
                'productsList.js',
            ));

            $categories = $this->plugin->getSendedCategories();
            $this->view->assign('categories',$categories);

            $warehouse = $this->plugin->getWarehouse();
            $this->view->assign('warehouse',$warehouse);

            $sets = $this->plugin->getSendedSets();
            $this->view->assign('sets',$sets);

            $productManually = $this->plugin->getProductManually();

            $this->view->assign('manually',$productManually);
        }

        public function ordersAction()
        {
            $this->view->assign('orders',"Orders");
        }

        public function attributesAction()
        {
            $this->addJs(array(
                'attr.js',
            ));

            $matchedCategories = $this->plugin->getMatchedCategories();
            $tmp = [];
            foreach ($matchedCategories as $k => $v) {
                if (array_key_exists($v['ozonName'], $tmp)) {
                    unset($matchedCategories[$k]);
                } else {
                    $tmp[$v['ozonName']] = true;
                }
            }

//            foreach ($matchedCategories as $shopCategoryId => $data) {
//
//                $matchedCategories[$shopCategoryId]['requiredAttributes'] = $this->plugin->getRequiredAttributeList($data['ozonId']);
//
//                $matchedCategories[$shopCategoryId]['shopAttributes'] = $this->plugin->getCategoryAttributes($shopCategoryId);
//                if ($matchedCategories[$shopCategoryId]['requiredAttributes']) {
//                    foreach ($matchedCategories[$shopCategoryId]['requiredAttributes'] as $key => $attr) {
//
//                        foreach ($matchedCategories[$shopCategoryId]['shopAttributes'] as $typeName => $data) {
//
//                            if (array_key_exists($attr['name'], $data)) {
//
//                                $matchedCategories[$shopCategoryId]['satisfy'][] = $attr['id'];
//
//                                    if (empty($attr['is_collection']) && $attr['dictionary_id'] > 0) {
//                                        if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 0) {
//
//                                        $matchedCategories[$shopCategoryId]['is_select'][] = $attr['id'];
//                                    }
//                                }
//                                    if ($attr['is_collection'] && $attr['dictionary_id'] > 0) {
//                                        if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 1) {
//                                        $matchedCategories[$shopCategoryId]['is_multiple'][] = $attr['id'];
//                                    }
//                                }
//
//                            }
//                        }
//                    }
//                } else $this->view->assign('errorApi',"1");
//
//            }
            $this->view->assign('matchedCategories',$matchedCategories);
//
            $matchedSets = $this->plugin->getMatchedSets();
//            foreach ($matchedSets as $shopSetsId => $data) {
//                $matchedSets[$shopSetsId]['requiredAttributes'] = $this->plugin->getRequiredAttributeList($data['ozonId']);
//                $matchedSets[$shopSetsId]['shopAttributes'] = $this->plugin->getSetAttributes($shopSetsId);
//
//                if ($matchedSets[$shopSetsId]['requiredAttributes']) {
//                    foreach ($matchedSets[$shopSetsId]['requiredAttributes'] as $key => $attr) {
//                        foreach ($matchedSets[$shopSetsId]['shopAttributes'] as $typeName => $data) {
//                            if (array_key_exists($attr['name'], $data)) {
//                                $matchedSets[$shopSetsId]['satisfy'][] = $attr['id'];
//                                if (empty($attr['is_collection']) && $attr['dictionary_id'] > 0) {
//                                    if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 0) {
//
//                                        $matchedSets[$shopSetsId]['is_select'][] = $attr['id'];
//                                    }
//                                }
//                                if ($attr['is_collection'] && $attr['dictionary_id'] > 0) {
//                                    if($data[$attr['name']]['selectable'] == 1 && $data[$attr['name']]['multiple'] == 1) {
//                                        $matchedSets[$shopSetsId]['is_multiple'][] = $attr['id'];
//                                    }
//                                }
//                            }
//                        }
//                    }
//                } else $this->view->assign('errorApi',"1");
//
//            }
            $this->view->assign('matchedSets',$matchedSets);
        }
        public function setsAction()
        {
            $setModel = new shopSetModel();
            static::$shopSets = array();
            static::$setsMatched = array();
            if ($setModel) {
                $sets = $setModel->getAll($setModel->getTableId(), true);
                static::$setsMatched = $this->plugin->getMatchedSets();

                if (!empty(static::$setsMatched)) {
                    foreach (static::$setsMatched as $setId => $setData) {
                        unset($sets[$setId]);
                    }

                    $this->view->assign('setsMatched', static::$setsMatched);
                }
            }

            $plugin = new shopOzoniusPlugin(array('app_id' => 'shop','id' => 'ozonius'));

            if ($plugin) {
                $ozonCategories = $plugin->getCategory();

                // ozon parents
                if ($ozonCategories) {
                    foreach ($ozonCategories as $id => $category) {
                        $this->_checkChildsOzon($category);
                    }

                    if (!empty(static::$parentsOzon)) {
                        $this->view->assign('parents', static::$parentsOzon);
                        static::$parentsOzon = array();
                    }
                }

                // shop parents

                foreach ($sets as $id => $set) {

                    if (!array_key_exists($id, static::$setsMatched)) {
                            static::$shopSets[$id] = $this->_restructureSetArray($id, $set);

                    }
                }
                $this->view->assign('sets', static::$shopSets);
                $this->view->assign('ozonCategories', $ozonCategories);
            }
        }

        public function categoriesAction()
        {
            $categoryModel = new shopCategoryModel();
            static::$shopCategories = array();
            static::$categoriesMatched = array();

            if ($categoryModel) {
                $categories = $categoryModel->getAll($categoryModel->getTableId(), true);

                static::$categoriesMatched = $this->plugin->getMatchedCategories();
                if (!empty(static::$categoriesMatched)) {
                    foreach (static::$categoriesMatched as $categoryId => $categoryData) {
                        unset($categories[$categoryId]);
                    }
                    $this->view->assign('categoriesMatched', static::$categoriesMatched);
                }
            }

            $plugin = new shopOzoniusPlugin(array('app_id' => 'shop','id' => 'ozonius'));

            if ($plugin) {
                $ozonCategories = $plugin->getCategory();

                // ozon parents
                if ($ozonCategories) {
                    foreach ($ozonCategories as $id => $category) {
                        $this->_checkChildsOzon($category);
                    }

                    if (!empty(static::$parentsOzon)) {
                        $this->view->assign('parents', static::$parentsOzon);
                        static::$parentsOzon = array();
                    }
                }

                // shop parents

                foreach ($categories as $id => $category) {
                    $this->_checkChildsShop($id);
                    $depth = $categoryModel->getById($id)['depth'];
                    
                    if (!array_key_exists($id, static::$categoriesMatched)) {
                        if (in_array($id, static::$parentsShop) && $depth == 0) {

                            static::$shopCategories[$id] = $this->_restructureCatArray($id, $category);
                        } else if ($depth == 0 && !in_array($id, static::$parentsShop)) {


                            static::$shopCategories[$id] = $this->_restructureCatArray($id, $category);
                        }
                    }
                }

                if (!empty(static::$parentsShop)) {
                    $this->view->assign('parentsShop', static::$parentsShop);
                    static::$parentsShop = array();
                }

                $this->view->assign('categories', static::$shopCategories);
                $this->view->assign('ozonCategories', $ozonCategories);
            }
        }

        protected function _checkChildsOzon($category)
        {
            if (!empty($category['children'])) {
                static::$parentsOzon[] = $category['category_id'];
                foreach ($category['children'] as $child) {
                    $this->_checkChildsOzon($child);
                }
            } else {
                return;
            }
        }

        protected function _checkChildsShop($categoryId)
        {
            $categoryModel = new shopCategoryModel();
            $childrens = $categoryModel->getSubcategories($categoryId);

            if (!empty($childrens)) {
                static::$parentsShop[] = $categoryId;
                foreach ($childrens as $id => $child) {
                    $this->_checkChildsShop($id);
                }
            } else {
                return;
            }
        }

        protected function _restructureCatArray($id,$category)
        {
            if (!array_key_exists($id, static::$categoriesMatched)) {

                $categoryModel = new shopCategoryModel();

                $categoryData = array(
                    'category_id' => $id,
                    'title'       => $category['name']
                );

                if (!empty($categoryModel->getSubcategories($id))) {
                    $childCategories = $categoryModel->getSubcategories($id);
                    foreach ($childCategories as $childId => $childCat) {
                        $childData = $this->_restructureCatArray($childId, $childCat);
                        $categoryData['children'][$childId] = $childData;
                    }
                    return $categoryData;
                } else {
                    $categoryData['children'] = array();
                    return $categoryData;
                }
            } else {
                return null;
            }
        }
        protected function _restructureSetArray($id,$category)
        {
            if (!array_key_exists($id, static::$setsMatched)) {
                $setData = array(
                    'category_id' => $id,
                    'title'       => $category['name']
                );
                return $setData;
            } else {
                return null;
            }
        }

        private function addCss(array $local_css)
        {
            foreach ($local_css as $_css)
            {
                wa()->getResponse()->addCss('plugins/ozonius/css/'.$_css, 'shop');
            }
        }

        private function addJs(array $local_js)
        {
            foreach ($local_js as $_js)
            {
                wa()->getResponse()->addJs('plugins/ozonius/js/'.$_js, 'shop');
            }
        }


    }
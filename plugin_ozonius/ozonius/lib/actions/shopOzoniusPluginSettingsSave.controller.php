<?php

class shopOzoniusPluginSettingsSaveController extends waJsonController
{
    public function execute()
    {
        try {

            // Сохраняем базовые настройки плагина
            $plugin = waSystem::getInstance()->getPlugin('ozonius');
            $setting = waRequest::post('settings');
            $stocks = waRequest::post('stocks');
            $plugin->saveSettings($setting);
            $this->response['message'] = 'Сохранено';
        } catch (Exception $e) {
            $this->errors['message'] = 'Ошибка при сохранении:<br>'.$e;
        }
    }
}

<?php


class shopOzoniusPluginSettingsAction extends waViewAction
{

    protected $plugin;
    public function __construct(waSystem $system = null)
    {
        parent::__construct($system);
        $this->plugin = new shopOzoniusPlugin(array('app_id' => 'shop','id' => 'ozonius'));
    }
	public function execute()
	{
		$plugin = wa()->getPlugin('ozonius');
        $stocks_model = new shopStockModel();
        $allStocks = $stocks_model->getAll();
        $returnStocks = array();
        $pluginSettings = $plugin->getSettings();
        foreach ($allStocks as $stock){
            $returnStocks[$stock['id']] = $stock['name'];
        }
        $checked_fields_u = !empty($pluginSettings['stocks']) ? $pluginSettings['stocks'] : array();
        $checked_fields_w = !empty($pluginSettings['warehouseData']) ? $pluginSettings['warehouseData'] : array();
        $row_stocks = !empty($pluginSettings['dataRes']) ? $pluginSettings['dataRes'] : array();
        $workflow = new shopWorkflow();
        $actions = $workflow->getConfig();

        $fetch_by_code = false;
        $parent = false;
        $feature_model = new shopFeatureModel();
        $features = $feature_model->select('`id` as `value`, `name` as `title`, `code`, `type`, `selectable`, `multiple`')->where((!$parent ? '`parent_id` IS NULL and ' : '') . '(`type` = \'text\' or `type` = \'varchar\' or `type` = \'double\' or `type` = \'color\' or `type` = \'boolean\' or `type` LIKE \'range%\' or `type` LIKE \'dimension%\' or `type` LIKE \'2d%\' or `type` LIKE \'3d%\')')->order('id ASC')->fetchAll($fetch_by_code ? 'code' : 'value');

        $cronProductStockSendTpl = '0 */3 * * * php %s/cli.php shop ozoniusProductStockSendUpdate';
        $cronProductPriceSendTpl = '0 */5 * * * php %s/cli.php shop ozoniusProductPriceSendUpdate';
        $cronTaskInfoTpl = '1-59/2 * * * * php %s/cli.php shop ozoniusTaskInfoUpdate';
        $cronProductInfoTpl = '0-58/2 * * * * php %s/cli.php shop ozoniusProductInfoUpdate';
        //$cronProductPriceStockTpl = '0-58/2 * * * * php %s/cli.php shop ozoniusProductPriceStockUpdate';
        $cronOrdersTpl = '1-59/2 * * * * php %s/cli.php shop ozoniusOrderUpdate';
        $cronBrandTpl = '0 */3 * * * php %s/cli.php shop ozoniusBrandUpdate';

        $cronProductStockSendCommand = sprintf($cronProductStockSendTpl, wa()->getConfig()->getRootPath());
        $cronProductPriceSendCommand = sprintf($cronProductPriceSendTpl, wa()->getConfig()->getRootPath());
        $cronTaskInfoCommand = sprintf($cronTaskInfoTpl, wa()->getConfig()->getRootPath());
        $cronProductInfoCommand = sprintf($cronProductInfoTpl, wa()->getConfig()->getRootPath());
        //$cronProductPriceStockCommand = sprintf($cronProductPriceStockTpl, wa()->getConfig()->getRootPath());
        $cronOrdersCommand = sprintf($cronOrdersTpl, wa()->getConfig()->getRootPath());
        $cronBrandCommand = sprintf($cronBrandTpl, wa()->getConfig()->getRootPath());

        $warehouse = $this->plugin->getWarehouse();

        $this->view->assign('features',$features);
        $this->view->assign('warehouse',$warehouse);
        $this->view->assign('actions',$actions['states']);
        $this->view->assign('row_stocks',$row_stocks);
        $this->view->assign('cronProductStockSendCommand', $cronProductStockSendCommand);
        $this->view->assign('cronProductPriceSendCommand', $cronProductPriceSendCommand);
        $this->view->assign('cronTaskInfoCommand', $cronTaskInfoCommand);
        $this->view->assign('cronProductInfoCommand', $cronProductInfoCommand);
        //$this->view->assign('cronProductPriceStockCommand', $cronProductPriceStockCommand);
        $this->view->assign('cronOrdersCommand', $cronOrdersCommand);
        $this->view->assign('cronBrandCommand', $cronBrandCommand);
        $this->view->assign('returnStocks', $returnStocks);
        $this->view->assign('settings', $plugin->getSettings());
        $this->view->assign('checked_fields_u', $checked_fields_u);
        $this->view->assign('checked_fields_w', $checked_fields_w);

	}
}
<?php
    /**
     * Created by PhpStorm.
     * User: Ilya Steshkov (Wesma)
     * Date: 27.08.2019
     * Time: 8:53
     */

    return array(
        "ozonius/api/categoriesCommit/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'commit',
        ),
        "ozonius/api/setsCommit/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'commitSets',
        ),
        "ozonius/api/categoriesRemoveAll/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'removeAll',
        ),
        "ozonius/api/setsRemoveAll/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'setsRemoveAll',
        ),
        "ozonius/api/categoriesAttrValues/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'attrValues',
        ),
        "ozonius/api/brandsAttrValues/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'brandsAttrValues',
        ),
        "ozonius/api/brandsSearchAttrValues/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'brandsSearchAttrValues',
        ),
        "ozonius/api/matchedCategories/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'matchedCategories',
        ),
        "ozonius/api/matchedSets/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'matchedSets',
        ),
        "ozonius/api/categoriesPrepareCategory/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'prepareCategory',
        ),
        "ozonius/api/categoriesSendCategory/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'sendCategory',
        ),
        "ozonius/api/getProductsInfo/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'getProductsInfo',
        ),
        // product actions
        "ozonius/api/productActivate/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productActivate',
        ),
        "ozonius/api/productDeactivate/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productDeactivate',
        ),
        "ozonius/api/productDelete/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productDelete',
        ),
        "ozonius/api/productUpdatePrice/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productUpdatePrice',
        ),
        "ozonius/api/productUpdateStock/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productUpdateStock',
        ),
        "ozonius/api/productUpdateDescription/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'productUpdateDescription',
        ),
        /** --- mock --- **/
        "ozonius/api/mockOrders/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'mockOrders',
        ),"ozonius/api/mockOrderItems/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'mockOrderItems',
        ),
        // order actions
        "ozonius/api/orderCancel/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'orderCancel',
        ),"ozonius/api/orderConfirm/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'orderConfirm',
        ),"ozonius/api/orderPrintPDF/" => array(
            'plugin' => 'ozonius',
            'module' => 'api',
            'action' => 'orderPrintPDF',
        ),
        /** ------------ **/
    );

<?php

    return array(
        'php.curl'=>array(
            'name'=>'cURL',
            'description'=>'Обмен данными со сторонними серверами',
            'strict'=>true,
        ),
        'php'=>array(
            'strict'=>true,
            'version'=>'>=5.6',
        ),
        'app.shop'=>array(
            'name'=>'Приложение «Магазин»',
            'description'=>'',
            'strict'=>true,
        )
    );
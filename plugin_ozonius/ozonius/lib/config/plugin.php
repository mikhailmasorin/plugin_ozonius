<?php

return array(
    'name'        => 'Интеграция с торговой площадкой Ozon',
    'description' => 'Позволяет интегрировать интернет-магазин с торговой площадкой Ozon (товары, категории, заказы)',
    'img'         => 'img/ozon-logo.png',
    'vendor'      => '1063942',
    'version'     => '4.7.0',
    'frontend'    => true,
    'custom_settings' => true,
    'handlers' => array(
        'backend_menu'         => 'backendMenu',
        'backend_order'		    => 'backendOrder',
        'order_action.*' => 'orderAction',
    ),
);

<?php
return array(
    'status_plugin'  => array(
        'title'        => _wp('Status plugin'),
        'options' => array(
            0 => _wp('Disabled'),
            1 => _wp('Enabled'),
        ),
        'value' => 1,
        'control_type'=> waHtmlControl::SELECT,
    ),
);
<?php
    /**
     * Created by PhpStorm.
     * User: Wesma13
     * Date: 29.08.2019
     * Time: 18:41
     */

    class shopOzoniusBackendLayout extends shopBackendLayout
    {
        public function display()
        {
            waSystem::popActivePlugin();

            try
            {
                parent::display();
            }
            catch (Exception $e)
            {}

            waSystem::pushActivePlugin('ozonius', 'shop');
        }

        protected function getTemplate()
        {
            $shop_layout = new shopBackendLayout();

            return $shop_layout->getTemplate();
        }
    }
<?php 

class shopOzoniusOrderUpdateCli extends waCliController
{

    public function execute()
    {
        $plugin = new shopOzoniusPlugin(array('app_id' => 'shop','id' => 'ozonius'));
        $plugin_logs = $plugin->getSettingPlugin('log_request');
        $plugin_enabled = $plugin->getSettingPlugin('status_plugin');

        if ($plugin_enabled) {
            $plugin->updateOrderList();
        } else {
            if ( isset($plugin_logs) && $plugin_logs )
            waLog::log("Плагин не активен.", 'shopOzonius.log');
        }
    }

}
<?php

$model = new waModel();

// Проверяем существование поля markup_price
try {
    $model->exec("SELECT markup_price FROM shop_ozonius_category WHERE 0");
} catch (waDbException $e) {
    $model->exec("ALTER TABLE shop_ozonius_category ADD markup_price INT (11) NOT NULL DEFAULT '0'");
}
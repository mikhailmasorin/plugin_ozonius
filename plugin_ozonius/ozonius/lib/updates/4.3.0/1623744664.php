<?php

$model = new waModel();

// Проверяем существование поля shop_prod_manually
try {
    $model->exec("SELECT shop_prod_manually FROM shop_ozonius_products WHERE 0");
} catch (waDbException $e) {
    $model->exec("ALTER TABLE shop_ozonius_products ADD shop_prod_manually INT (10)");
}
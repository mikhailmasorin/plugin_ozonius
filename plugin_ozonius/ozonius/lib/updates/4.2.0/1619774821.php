<?php

$model = new waModel();

// Проверяем существование поля fix_price
try {
    $model->exec("SELECT fix_price FROM shop_ozonius_category WHERE 0");
} catch (waDbException $e) {
    $model->exec("ALTER TABLE shop_ozonius_category ADD fix_price INT (11) NOT NULL DEFAULT '0'");
    $model->exec("ALTER TABLE shop_ozonius_sets ADD fix_price INT (11) NOT NULL DEFAULT '0'");
}
<?php

require_once "vendors/api/Ozon.php";

class shopOzoniusPlugin extends shopPlugin
{
    const
        TABLE_CATEGORY = 'shop_ozonius_category',
        TABLE_SETS = 'shop_ozonius_sets',
        TABLE_PRODUCTS = 'shop_ozonius_products',
        TABLE_ORDERS = 'shop_ozonius_orders',
        TABLE_ORDER_ITEMS = 'shop_ozonius_order_items',
        TABLE_ORDER_LOG = 'shop_ozonius_order_update_log',
        TABLE_TASKS = 'shop_ozonius_tasks';

    const MODEL_NAME_ATTR_ID = 8292,
        MODEL_NAME_ATTR = "Название модели";

    const
        ORDER_FBO_TYPE = 'fbo',
        ORDER_FBS_TYPE = 'fbs';

    private $_requiredParams = [
        'height' => ['mm' => 10000, 'cm' => 1000, 'in' => 393],
        'depth' => ['mm' => 10000, 'cm' => 1000, 'in' => 393],
        'width' => ['mm' => 10000, 'cm' => 1000, 'in' => 393],
        'dimension_unit' => ['allowed' => ['mm', 'cm', 'in']],
        'weight' => ['g' => 1000000, 'kg' => 1000, 'lb' => 2204],
        'weight_unit' => ['allowed' => ['g', 'kg', 'lb']]
    ];

    private $_ozonStatuses = [
        'pending' => 'Ожидает обработки',
        'processing' => 'Обрабатывается',
        'moderating' => 'На модерации',
        'processed' => 'Обработан',
        'failed_moderation' => 'Не прошел модерацию',
        'failed_validation' => 'Не прошел валидацию',
        'failed' => 'Не создан'
    ];

    public function getSettingPlugin($setting_name = '')
    {
        if (!empty($setting_name)) {
            return wa('shop')->getPlugin('ozonius')->getSettings($setting_name);
        } else {
            return wa('shop')->getPlugin('ozonius')->getSettings();
        }
    }


    protected function getApiKeyFromSettings()
    {
        return $this->getSettingPlugin('api_key');
    }

    protected function getClientIdFromSettings()
    {
        return $this->getSettingPlugin('client_id');
    }

    protected function getStocksFromSettings()
    {
        return $this->getSettingPlugin('stocks');
    }
    protected function getWarehouseFromSettings()
    {
        return $this->getSettingPlugin('dataRes');
    }
    public function getCountPaginationFromSettings()
    {
        return $this->getSettingPlugin('elem_pagination');
    }

    private function _getOzonApi()
    {
        $apiKey = $this->getApiKeyFromSettings();
        $clientId = $this->getClientIdFromSettings();
        return Ozon::getInstance($apiKey, $clientId);
    }

    public function getPluginDataPath()
    {
        return wa()->getDataPath('plugins', true, 'shop') . "/ozonius/";
    }

    /** --------- Category api methods --------- **/

    public function getCategory($id = '0')
    {
        $api = $this->_getOzonApi();
        return $api->getCategoryTree($id);
    }

    public function getRequiredAttributeList($categoryId)
    {
        $api = $this->_getOzonApi();
        return $api->getCategoryAttribute($categoryId, Ozon::REQ_ATTR_TYPE_REQUIRED);


    }

    public function getOptionalAttributeList($categoryId)
    {
        $api = $this->_getOzonApi();

        return $api->getCategoryAttribute($categoryId, Ozon::REQ_ATTR_TYPE_OPTIONAL);
    }

    public function brandAttributeList()
    {
        $api = $this->_getOzonApi();

        return $api->getCategoryValueAttributeBrands();
    }

    public function sendCategoryItems($items)
    {
        $api = $this->_getOzonApi();
        return $api->productImport($items);
    }

    /** ---------------------------------------- **/

    public function getStatus()
    {
        $stateMap = array();
        $awaiting_approve =  $this->getSettingPlugin('awaiting_approve');
        $awaiting_packaging =  $this->getSettingPlugin('awaiting_packaging');
        $awaiting_deliver =  $this->getSettingPlugin('awaiting_deliver');
        $delivering =  $this->getSettingPlugin('delivering');
        $driver_pickup =  $this->getSettingPlugin('driver_pickup');
        $delivered =  $this->getSettingPlugin('delivered');
        $cancelled =  $this->getSettingPlugin('cancelled');
        if(!empty($awaiting_approve)){
            $stateMap['awaiting_approve'] = $awaiting_approve;
        }
        if(!empty($awaiting_packaging)){
            $stateMap['awaiting_packaging'] = $awaiting_packaging;
        }
        if(!empty($awaiting_deliver)){
            $stateMap['awaiting_deliver'] = $awaiting_deliver;
        }
        if(!empty($delivering)){
            $stateMap['delivering'] = $delivering;
        }
        if(!empty($driver_pickup)){
            $stateMap['driver_pickup'] = $driver_pickup;
        }
        if(!empty($delivered)){
            $stateMap['delivered'] = $delivered;
        }
        if(!empty($cancelled)){
            $stateMap['cancelled'] = $cancelled;
        }

        return $stateMap;
    }
    public function getWarehouse()
    {
        $api = $this->_getOzonApi();
        return $api->warehouse();
    }

    public function getList()
    {
        $api = $this->_getOzonApi();
        return $api->listInfo();
    }

    /** --------- Category fetching data methods --------- **/

    public function getMatchedCategories()
    {
        $categoryModel = new shopCategoryModel();
        $matched = [];
        $categories = $categoryModel->getAll($categoryModel->getTableId(), true);
        $queryResult = $categoryModel->query("SELECT * FROM `" . self::TABLE_CATEGORY . "`")->fetchAll();
        foreach ($queryResult as $key => $item) {

            $matched[$item['shop_category_id']] = array(
                'name' => $categories[$item['shop_category_id']]['name'],
                'ozonId' => $item['ozon_category_id'],
                'ozonName' => $item['ozon_name'],
                'markupPrice' => $item['markup_price'],
                'fixPrice' => $item['fix_price'],
            );
        }

        return $matched;
    }

    public function getMatchedSets()
    {
        $setModel = new shopSetModel();
        $matched = [];
        $queryResult = $setModel->query("SELECT * FROM `" . self::TABLE_SETS . "`")->fetchAll();
        foreach ($queryResult as $key => $item) {

            $matched[$item['shop_sets_id']] = array(
                'idSet' => $item['shop_sets_id'],
                'name' => $item['shop_sets_name'],
                'ozonId' => $item['ozon_category_id'],
                'ozonName' => $item['ozon_name'],
                'markupPrice' => $item['markup_price'],
                'fixPrice' => $item['fix_price'],
            );
        }

        return $matched;
    }


    public function getCategoryAttributes($categoryId)
    {
        $model = new shopCategoryModel();
        $typeModel = new shopTypeModel();
        $typeFeatModel = new shopTypeFeaturesModel();
        $attributes = [];

        $result = $model->query("select distinct p.type_id from shop_product p 
	            inner join shop_category_products cp on cp.product_id = p.id
	            where cp.category_id = i:id", array('id' => $categoryId));

        foreach ($result as $key => $data) {
            $type = $typeModel->getById($data['type_id']);
            $types = $typeFeatModel->getByType($data['type_id']);
            $types = array_slice($types, 0, 100);
            foreach ($types as $key => $typeData) {

                $attributes[$type['name']][$typeData['name']] = array(
                    'type' => $typeData['type'],
                    'selectable' => $typeData['selectable'],
                    'multiple' => $typeData['multiple'],
                );
            }
        }

        return $attributes;
    }
    public function getCurrencyPrice($price, $currency)
    {
        $model = new waModel();
        $currencyDefault = $model->query("SELECT code FROM shop_currency WHERE sort = i:id", array('id' => 0))->fetch()['code'];
        if ($currencyDefault != 'RUB') {
            if ($currency != 'RUB') {
                $currencyRate = $model->query("SELECT rate FROM shop_currency WHERE code = s:code", array('code' => 'RUB'))->fetch()['rate'];
                $price = $price / $currencyRate;
            }
        }
        else{
            if ($currency != 'RUB') {
                $currencyRate = $model->query("SELECT rate FROM shop_currency WHERE code = s:code", array('code' => $currency))->fetch()['rate'];
                $price = $price * $currencyRate;
            }
        }
        return $price;
    }
    public function getCurrencyPriceValidateNoSku($price, $currency)
    {
        $model = new waModel();
        $currencyDefault = $model->query("SELECT code FROM shop_currency WHERE sort = i:id", array('id' => 0))->fetch()['code'];
        if ($currencyDefault != 'RUB') {
            $currencyRate = $model->query("SELECT rate FROM shop_currency WHERE code = s:code", array('code' => 'RUB'))->fetch()['rate'];
            $price = $price / $currencyRate;
        }
        else{
            if ($currency != 'RUB') {
                $currencyRate = $model->query("SELECT rate FROM shop_currency WHERE code = s:code", array('code' => $currency))->fetch()['rate'];
                $price = $price * $currencyRate;
            }
        }
        return $price;
    }
    public function getSetAttributes($setId)
    {
        $model = new shopCategoryModel();
        $typeModel = new shopTypeModel();
        $typeFeatModel = new shopTypeFeaturesModel();
        $attributes = [];

        $result = $model->query("select distinct p.type_id from shop_product p 
	            inner join shop_set_products sp on sp.product_id = p.id
	            where sp.set_id = s:id", array('id' => $setId));

        foreach ($result as $key => $data) {
            $type = $typeModel->getById($data['type_id']);
            $types = $typeFeatModel->getByType($data['type_id']);
            $types = array_slice($types, 0, 100);
            foreach ($types as $key => $typeData) {
                $attributes[$type['name']][$typeData['name']] = array(
                    'type' => $typeData['type'],
                    'selectable' => $typeData['selectable'],
                    'multiple' => $typeData['multiple'],
                );
            }

        }

        return $attributes;
    }


    public function getCategoryProducts($categoryId)
    {
        $collection = new shopProductsCollection("category/{$categoryId}");
        $products = $collection->getProducts('*', 0, $collection->count());
        return $products;
    }

    public function getUpdateProducts($ozonID)
    {

        $model = new waModel();
        $selectQuery = "SELECT  shop_product_id FROM shop_ozonius_products WHERE ozon_product_id = i:id";
        $productId = $model->query($selectQuery, array('id' => $ozonID))->fetch()['shop_product_id'];
        $collection = new shopProductsCollection("id/{$productId}");
        $products = $collection->getProducts('*', 0, $collection->count());
        return $products;
    }

    public function getSetProducts($setId)
    {
        $collection = new shopProductsCollection("set/{$setId}");

        $products = $collection->getProducts('*', 0, $collection->count());

        return $products;
    }

    public function getOzoniusCategoryId($categoryId)
    {
        $model = new waModel();
        $selectQuery = "SELECT id FROM " . self::TABLE_CATEGORY . " WHERE shop_category_id = i:id";
        return $model->query($selectQuery, array('id' => $categoryId))->fetch()['id'];
    }

    public function getOzoniusSetId($setId)
    {
        $model = new waModel();
        $selectQuery = "SELECT id FROM " . self::TABLE_SETS . " WHERE shop_sets_id = s:id";
        return $model->query($selectQuery, array('id' => $setId))->fetch()['id'];
    }

    public function ozonPriceMarkup($data, $price, $ids)
    {
        if ($data) {
            foreach ($data as $key => $attr) {
                if ($ids == $key) {
                    $resPrice = ($price / 100 * $attr['markupPrice']) + $price + $attr['fixPrice'];
                    $price = str_replace(",", "", number_format((float)$resPrice, 2));
                }
            }
            return $price;
        }
    }




    /**
     * @param $categoryId
     * @return array
     */
    public function getCategoryOzonProducts($ozoniusCatId,$page)
    {
        $model = new waModel();
        $products = array();
        $limit = 30;
        $limitSetting = $this->getCountPaginationFromSettings();
        if($limitSetting && $limitSetting !==0){
            $limit = $limitSetting;
        }
        else{
            $limit = $limit;
        }
        $page = (empty($page)) ? 1 : $page;
        $start = ($page != 1) ? $page * $limit - $limit : 0;

        $selectQuery = "SELECT shop_product_id,shop_product_sku_id,id,ozon_product_id,ozon_stocks,ozon_visibility_details,ozon_visible,ozon_validation_errors,ozon_price,ozon_state,ozon_offer_id 
                            FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_category_id = i:id LIMIT {$start}, {$limit}";

        $data = $model->query($selectQuery, array('id' => $ozoniusCatId))->fetchAll();

        if ($data) {
            foreach ($data as $key => $product) {

                $shopInfo = $model->query("SELECT price, `count` from shop_product_skus
                                                where sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch();

                $getStocks = $this->getStocksFromSettings();
                $get_fbs = $this->getSettingPlugin('get_fbs');
                $getWarehouses = $this->getWarehouseFromSettings();
                if($get_fbs == "On"){
                    if (!empty($getWarehouses)) {
                        $arr = array();
                        foreach ($getWarehouses as $getWarehouse){

                            $getStShop = explode(';', $getWarehouse['shop']);
                            $arr[] = $getStShop;
                        }
                        $getStocksStr =  implode(',', array_map(function ($value) {
                            return $value[1];
                        }, $arr));


                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];

                    }
                }
                else{
                    if (!empty($getStocks)) {
                        $getStocksStr = implode(',', $getStocks);
                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];


                    } else {
                        $shopCount = ($shopInfo['count']) ? $shopInfo['count'] : '';
                    }
                }

                $productId = $model->query("SELECT `product_id` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['product_id'];
                $nameDefault = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];

                $name_skus = $model->query("SELECT `name` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['name'];

                $name = $nameDefault . ", " . $name_skus;
                if (!$name_skus || $name_skus == '') {
                    $name = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                }
                $url = $this->_getBackendProductUrl($product['shop_product_id']);
                $stocks = json_decode($product['ozon_stocks'], 1);

                if (array_key_exists($product['ozon_state'], $this->_ozonStatuses)) {
                    $state = $this->_ozonStatuses[$product['ozon_state']];
                } else {
                    $state = $product['ozon_state'];
                }

                $visibleState = ($product['ozon_visible']) ? 'Показывается' : 'Скрыт';
                $visible = array(
                    'state' => $visibleState,
                    'details' => json_decode($product['ozon_visibility_details'], 1)
                );

                $validationErrors = json_decode($product['ozon_validation_errors'], 1) ?: array();
                if ($validationErrors) {
                    foreach ($validationErrors as $key => $error) {
                        $fixed = preg_replace('/(u)/', '\u', $validationErrors[$key]['error']);
                        $validationErrors[$key]['error'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                        }, $fixed);
                    }
                }
                $markup = $model->query("SELECT t1.ozon_category_id, t1.markup_price, t1.fix_price FROM " . self::TABLE_CATEGORY . " AS t1
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_category_id
WHERE t2.id = {$product['id']}")->fetch();

                $settingAttrPrice = $this->getSettingPlugin('features_price');

                if($settingAttrPrice && $settingAttrPrice != ""){
                    $productFeatureModel = new shopProductFeaturesModel();
                    $featuresSku = $productFeatureModel->getValues($product['shop_product_id'],$data['shop_product_sku_id']);
                    $price = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $price = str_replace(",", "", number_format((float)$shopInfo['price'], 2));
                }

                $products[$product['id']] = array(
                    'name' => $name,
                    'url' => $url,
                    'price' => str_replace(",", "", number_format((float)$product['ozon_price'], 2)),
                    'markup' => $markup['markup_price'],
                    'fix' => $markup['fix_price'],
                    'shopPrice' => $price,
                    'ozonId' => $product['ozon_product_id'],
                    'ozonCat' => $markup['ozon_category_id'],
                    'stocks' => $stocks,
                    'shopCount' => $shopCount,
                    'shopSku' => $product['ozon_offer_id'],
                    'state' => $state,
                    'visible' => $visible,
                    'validationErrors' => $validationErrors
                );
            }
        }
        return $products;
    }

    public function getSetsOzonProducts($ozoniusSetId,$page)
    {
        $model = new waModel();
        $products = array();
        $limit = 30;
        $limitSetting = $this->getCountPaginationFromSettings();
        if($limitSetting && $limitSetting !==0){
            $limit = $limitSetting;
        }
        else{

            $limit = $limit;
        }
        $page = (empty($page)) ? 1 : $page;
        $start = ($page != 1) ? $page * $limit - $limit : 0;
        $selectQuery = "SELECT shop_product_id,shop_product_sku_id,id,ozon_product_id,ozon_stocks,ozon_visibility_details,ozon_visible,ozon_validation_errors,ozon_price,ozon_state,ozon_offer_id 
                            FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_sets_id = i:id LIMIT {$start}, {$limit}";

        $data = $model->query($selectQuery, array('id' => $ozoniusSetId))->fetchAll();

        if ($data) {
            foreach ($data as $key => $product) {

                $shopInfo = $model->query("SELECT price, `count` from shop_product_skus
                                                where sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch();

                $getStocks = $this->getStocksFromSettings();
                $get_fbs = $this->getSettingPlugin('get_fbs');
                $getWarehouses = $this->getWarehouseFromSettings();
                if($get_fbs == "On"){
                    if (!empty($getWarehouses)) {


                        $arr = array();
                        foreach ($getWarehouses as $getWarehouse){

                            $getStShop = explode(';', $getWarehouse['shop']);
                            $arr[] = $getStShop;
                        }
                        $getStocksStr =  implode(',', array_map(function ($value) {
                            return $value[1];
                        }, $arr));

                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];

                    }
                }
                else{
                    if (!empty($getStocks)) {
                        $getStocksStr = implode(',', $getStocks);
                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];


                    } else {
                        $shopCount = ($shopInfo['count']) ? $shopInfo['count'] : '';
                    }
                }


                $productId = $model->query("SELECT `product_id` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['product_id'];
                $nameDefault = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                $name_skus = $model->query("SELECT `name` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['name'];

                $name = $nameDefault . ", " . $name_skus;
                if (!$name_skus || $name_skus == '') {
                    $name = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                }

                $url = $this->_getBackendProductUrl($product['shop_product_id']);
                $stocks = json_decode($product['ozon_stocks'], 1);

                if (array_key_exists($product['ozon_state'], $this->_ozonStatuses)) {
                    $state = $this->_ozonStatuses[$product['ozon_state']];
                } else {
                    $state = $product['ozon_state'];
                }

                $visibleState = ($product['ozon_visible']) ? 'Показывается' : 'Скрыт';
                $visible = array(
                    'state' => $visibleState,
                    'details' => json_decode($product['ozon_visibility_details'], 1)
                );

                $validationErrors = json_decode($product['ozon_validation_errors'], 1) ?: array();
                if ($validationErrors) {
                    foreach ($validationErrors as $key => $error) {
                        $fixed = preg_replace('/(u)/', '\u', $validationErrors[$key]['error']);
                        $validationErrors[$key]['error'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                        }, $fixed);
                    }
                }
                $markup = $model->query("SELECT t1.ozon_category_id, t1.markup_price, t1.fix_price FROM " . self::TABLE_SETS . " AS t1
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_sets_id
WHERE t2.id = {$product['id']}")->fetch();
                $settingAttrPrice = $this->getSettingPlugin('features_price');

                if($settingAttrPrice && $settingAttrPrice != ""){
                    $productFeatureModel = new shopProductFeaturesModel();
                    $featuresSku = $productFeatureModel->getValues($product['shop_product_id'],$data['shop_product_sku_id']);
                    $price = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $price = str_replace(",", "", number_format((float)$shopInfo['price'], 2));
                }
                $products[$product['id']] = array(
                    'name' => $name,
                    'url' => $url,
                    'price' => str_replace(",", "", number_format((float)$product['ozon_price'], 2)),
                    'markup' => $markup['markup_price'],
                    'fix' => $markup['fix_price'],
                    'shopPrice' => $price,
                    'ozonId' => $product['ozon_product_id'],
                    'ozonCat' => $markup['ozon_category_id'],
                    'stocks' => $stocks,
                    'shopCount' => $shopCount,
                    'shopSku' => $product['ozon_offer_id'],
                    'state' => $state,
                    'visible' => $visible,
                    'validationErrors' => $validationErrors
                );
            }
        }
        return $products;
    }

    public function getManuallyOzonProducts($ozoniusManuallyId,$page)
    {
        $model = new waModel();
        $products = array();
        $limit = 30;
        $limitSetting = $this->getCountPaginationFromSettings();
        if($limitSetting && $limitSetting !==0){
            $limit = $limitSetting;
        }
        else{

            $limit = $limit;
        }
        $page = (empty($page)) ? 1 : $page;
        $start = ($page != 1) ? $page * $limit - $limit : 0;
        $selectQuery = "SELECT shop_product_id,id,ozon_product_id,ozon_stocks,ozon_visibility_details,ozon_visible,ozon_validation_errors,ozon_price,ozon_state,ozon_offer_id 
                            FROM " . self::TABLE_PRODUCTS . " WHERE shop_prod_manually = i:id LIMIT {$start}, {$limit}";

        $data = $model->query($selectQuery, array('id' => $ozoniusManuallyId))->fetchAll();

        if ($data) {
            foreach ($data as $key => $product) {

                $shopInfo = $model->query("SELECT price, `count` from shop_product_skus
                                                where sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch();

                $getStocks = $this->getStocksFromSettings();
                $get_fbs = $this->getSettingPlugin('get_fbs');
                $getWarehouses = $this->getWarehouseFromSettings();
                if($get_fbs == "On"){
                    if (!empty($getWarehouses)) {
                        $arr = array();
                        foreach ($getWarehouses as $getWarehouse){

                            $getStShop = explode(';', $getWarehouse['shop']);
                            $arr[] = $getStShop;
                        }
                        $getStocksStr =  implode(',', array_map(function ($value) {
                            return $value[1];
                        }, $arr));

                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];

                    }
                }
                else{
                    if (!empty($getStocks)) {
                        $getStocksStr = implode(',', $getStocks);
                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];


                    } else {
                        $shopCount = ($shopInfo['count']) ? $shopInfo['count'] : '';
                    }
                }


                $productId = $model->query("SELECT `product_id` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['product_id'];
                $nameDefault = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                $name_skus = $model->query("SELECT `name` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['name'];

                $name = $nameDefault . ", " . $name_skus;
                if (!$name_skus || $name_skus == '') {
                    $name = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                }

                $url = $this->_getBackendProductUrl($product['shop_product_id']);
                $stocks = json_decode($product['ozon_stocks'], 1);

                if (array_key_exists($product['ozon_state'], $this->_ozonStatuses)) {
                    $state = $this->_ozonStatuses[$product['ozon_state']];
                } else {
                    $state = $product['ozon_state'];
                }

                $visibleState = ($product['ozon_visible']) ? 'Показывается' : 'Скрыт';
                $visible = array(
                    'state' => $visibleState,
                    'details' => json_decode($product['ozon_visibility_details'], 1)
                );

                $validationErrors = json_decode($product['ozon_validation_errors'], 1) ?: array();
                if ($validationErrors) {
                    foreach ($validationErrors as $key => $error) {
                        $fixed = preg_replace('/(u)/', '\u', $validationErrors[$key]['error']);
                        $validationErrors[$key]['error'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                        }, $fixed);
                    }
                }

                $products[$product['id']] = array(
                    'name' => $name,
                    'url' => $url,
                    'price' => str_replace(",", "", number_format((float)$product['ozon_price'], 2)),
                    'markup' => 0,
                    'fix' => 0,
                    'shopPrice' => str_replace(",", "", number_format((float)$shopInfo['price'], 2)),
                    'ozonId' => $product['ozon_product_id'],
                    'stocks' => $stocks,
                    'shopCount' => $shopCount,
                    'shopSku' => $product['ozon_offer_id'],
                    'state' => $state,
                    'visible' => $visible,
                    'validationErrors' => $validationErrors
                );
            }
        }
        return $products;
    }



    public function getCategoryOzonProductsCount($categoryId)
    {
        $model = new waModel();
        $id = $this->getOzoniusCategoryId($categoryId);
        $countQuery = "SELECT count(*) as cnt FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_category_id = i:id";
        return $model->query($countQuery, array('id' => $id))->fetch()['cnt'];
    }

    public function getCategoryShopProductsCount($categoryId)
    {
        $count = 0;
        $products = $this->getCategoryProducts($categoryId);
        foreach ($products as $id => $product) {
            $count += $this->getProductSkuCount($id);
        }
        return array('count' => count($products), 'withSkus' => $count);
    }

    public function getSetOzonProductsCount($setId)
    {
        $model = new waModel();
        $id = $this->getOzoniusSetId($setId);
        $countQuery = "SELECT count(*) as cnt FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_sets_id = i:id";
        return $model->query($countQuery, array('id' => $id))->fetch()['cnt'];
    }

    public function getSetShopProductsCount($setId)
    {
        $count = 0;
        $products = $this->getSetProducts($setId);

        foreach ($products as $id => $product) {
            $count += $this->getProductSkuCount($id);
        }
        return array('count' => count($products), 'withSkus' => $count);
    }

    public function getSendedCategories()
    {
        $model = new waModel();
        $categories = array();
        $query = "select cat.shop_category_id, cat.ozon_name, prod.ozonius_category_id 
	                    from " . self::TABLE_CATEGORY . " as cat right join 
	                    		" . self::TABLE_PRODUCTS . " as prod on cat.id = prod.ozonius_category_id WHERE prod.ozonius_category_id <> 0
	                    		group by prod.ozonius_category_id ";
        $result = $model->query($query)->fetchAll();
        foreach ($result as $key => $item) {
            $shopName = $model->query("SELECT `name` from shop_category where id = i:id", array('id' => $item['shop_category_id']))->fetch()['name'];
            $ozonName = $item['ozon_name'];
            $categories[] = [
                'shopName' => $shopName,
                'ozonName' => $ozonName,
                'ozoniusCatId' => $item['ozonius_category_id']
            ];
        }

        return $categories;
    }

    public function getSendedSets()
    {
        $model = new waModel();
        $sets = array();
        $query = "select cat.shop_sets_name, cat.shop_sets_id, cat.ozon_name, prod.ozonius_sets_id 
	                    from " . self::TABLE_SETS . " as cat right join 
	                    		" . self::TABLE_PRODUCTS . " as prod on cat.id = prod.ozonius_sets_id WHERE prod.ozonius_sets_id <> 0
	                    		group by prod.ozonius_sets_id";
        $result = $model->query($query)->fetchAll();
        foreach ($result as $key => $item) {
            $shopName = $model->query("SELECT `name` from shop_set where id = s:id", array('id' => $item['shop_sets_id']))->fetch()['name'];
            $ozonName = $item['ozon_name'];
            $sets[] = [
                'shopName' => $shopName,
                'ozonName' => $ozonName,
                'ozoniusSetId' => $item['ozonius_sets_id']
            ];
        }

        return $sets;
    }

    public function getProductManually()
    {
        $model = new waModel();
        $manually = array();
        $query = "select shop_prod_manually from " . self::TABLE_PRODUCTS . " WHERE shop_prod_manually is NOT NULL";
        $result = $model->query($query)->fetchAll();

        foreach ($result as $key => $item) {
            $shopName = 'товары с озона';
            $ozonName = 'товары с озона';
            $manually[] = [
                'shopName' => $shopName,
                'ozonName' => $ozonName,
                'manuallyId' => $item['shop_prod_manually']
            ];
        }

        return $manually;
    }

    /** -------------------------------------------------- **/

    public function prepareCategoryProducts($categoryId, $markup, $ozonCatId)
    {
        set_time_limit(600);
        $products = $this->getCategoryProducts($categoryId);
        $result = array();
        $valid = 0;
        $items = array();
        $categoryItems = array();

        $CategoryValueAttribute = [];

        $ozonRequiredAttributes[] = $this->getRequiredAttributeList($ozonCatId);

        $api = $this->_getOzonApi();

        foreach ($ozonRequiredAttributes[0] as $value){
                if($value['dictionary_id'] != 0) {
                    usleep(1000000);
                    $CategoryValueAttribute[$value['name']] = $api->getCategoryValueAttribute((int)$ozonCatId, (int)$value['id']);
                }
        }

        foreach ($products as $id => $product) {
            $product['category_id'] = (int)$ozonCatId;
            $product['markup'] = $markup;
            $info = $this->checkProduct($id);
            $responseUrl = $this->_getBackendProductUrl($product['id']);


            if ($info['isNotSend'] && $product['status']) {
                // если товар не отправляли
                $params = $this->_getProductParams($product['id']);
                if ($params['ozon.ignored'] == 1) {
                    $result['invalid'][] = array(
                        'message' => 'В дополнительных параметрах включен ozon.ignored=1',
                        'name' => $product['name'],
                        'url' => $responseUrl,
                    );
                    continue;
                }
                $productInfo = (isset($info['notSentSku'])) ? $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttribute, $opt = array(), $optAttr =array(), $info['notSentSku']) :
                    $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttribute, $opt =array(), $optAttr = array());

                if ($productInfo['valid']) {
                    $valid++;
                    if (isset($productInfo['items'][0]) && is_array($productInfo['items'][0])) {
                        foreach ($productInfo['items'] as $key => $item) {
                            array_push($items, $item);
                        }
                    } else {
                        array_push($items, $productInfo['items']);
                    }
                } else {
                    $result['invalid'][] = $productInfo['data'];
                }
            }

            if ($info['invalidExists']){


                $imp = implode(', ',$info['invalidExists']);
                $result['invalid'][] = array(
                   'message' => "<p style='margin: 0;text-align: left'>Артикула " . $imp . " уже существуют в плагине озона</p>",
                    'name' => $product['name'],
                    'url' => $responseUrl,
                );
            }
        }

        if (!empty($items)) {
            $categoryItems['items'] = $items;
            $this->_itemsToJsonFile($categoryItems, $categoryId);
        }

        $result['valid'] = $valid;

        return $result;
    }

    public function prepareUpdateProducts($ozonID,$ozonCatID,$ozonRequiredAttributes,$CategoryValueAttributeReq,$ozonOptionalAttributes,$CategoryValueAttributeOpt,$markup)
    {
        set_time_limit(600);
        $products = $this->getUpdateProducts($ozonID);
        $model = new waModel();
        $items = array();


        foreach ($products as $id => $product) {
            $product['category_id'] = $ozonCatID;
            $product['markup'] = $markup;


            $info = $this->checkProductUpdate($id, $ozonID);

            $productInfo = $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttributeReq, $ozonOptionalAttributes, $CategoryValueAttributeOpt, $info['notSentSku']);

            if (isset($productInfo['items'][0]) && is_array($productInfo['items'][0])) {
                foreach ($productInfo['items'] as $key => $item) {
                    unset($item['category_id']);
                    $item['category_id'] = $ozonCatID;
                    array_push($items, $item);
                }
            } else {
                array_push($items, $productInfo['items']);
            }
        }
        return $items;
    }
    public function prepareUpdateProductsManually($ozonID,$sku,$info)
    {

        set_time_limit(600);
        $products = $this->getUpdateProducts($ozonID);
        $items = array();

        $CategoryValueAttributeOpt = [];
        $CategoryValueAttributeReq = [];

        $ozonRequiredAttributes[] = $this->getRequiredAttributeList($info['category_id']);
        sleep(10);
        $ozonOptionalAttributes[] = $this->getOptionalAttributeList($info['category_id']);

        $api = $this->_getOzonApi();

        foreach ($ozonRequiredAttributes[0] as $value){
                if($value['dictionary_id'] != 0) {
                    usleep(1000000);
                    $CategoryValueAttributeReq[$value['name']] = $api->getCategoryValueAttribute((int)$info['category_id'], (int)$value['id']);
                }
        }
        foreach ($ozonOptionalAttributes[0] as $value){
            if($value['dictionary_id'] != 0) {
                usleep(1000000);
                $CategoryValueAttributeOpt[$value['name']] = $api->getCategoryValueAttribute((int)$info['category_id'], (int)$value['id']);
            }
        }
        foreach ($products as $id => $product) {
            $info = $this->checkProductUpdate($id, $ozonID);
            $product['category_id'] = $info['category_id'];
            $productInfo = $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttributeReq, $ozonOptionalAttributes, $CategoryValueAttributeOpt, $info['notSentSku']);

            if (isset($productInfo['items'][0]) && is_array($productInfo['items'][0])) {
                foreach ($productInfo['items'] as $key => $item) {
                    array_push($items, $item);
                }
            } else {
                array_push($items, $productInfo['items']);
            }
        }
        return $items;
    }

    public function prepareSetProducts($setId, $markup,$ozonCatId)
    {

        set_time_limit(600);
        $products = $this->getSetProducts($setId);
        $result = array();
        $valid = 0;
        $items = array();
        $categoryItems = array();
        $ozonRequiredAttributes[] = $this->getRequiredAttributeList($ozonCatId);
        $CategoryValueAttribute = [];
        $api = $this->_getOzonApi();
        foreach ($ozonRequiredAttributes[0] as $value) {
                if ($value['dictionary_id'] != 0) {
                    usleep(1000000);
                    $CategoryValueAttribute[$value['name']] = $api->getCategoryValueAttribute((int)$ozonCatId, (int)$value['id']);
                }
        }

        foreach ($products as $id => $product) {
            $product['category_id'] = (int)$ozonCatId;
            $product['markup'] = $markup;
            $info = $this->checkProduct($id);
            $responseUrl = $this->_getBackendProductUrl($product['id']);
            if ($info['isNotSend'] && $product['status']) {
                // если товар не отправляли
                $params = $this->_getProductParams($product['id']);
                if ($params['ozon.ignored'] == 1) {
                    $result['invalid'][] = array(
                        'message' => 'В дополнительных параметрах включен ozon.ignored=1',
                        'name' => $product['name'],
                        'url' => $responseUrl,
                    );
                    continue;
                }
                $productInfo = (isset($info['notSentSku'])) ? $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttribute, $opt = array(), $optAttr = array(), $info['notSentSku']) :
                    $this->_validateProduct($product, $ozonRequiredAttributes, $CategoryValueAttribute, $opt = array(), $optAttr = array());


                if ($productInfo['valid']) {
                    $valid++;
                    if (isset($productInfo['items'][0]) && is_array($productInfo['items'][0])) {
                        foreach ($productInfo['items'] as $key => $item) {
                            array_push($items, $item);
                        }
                    } else {
                        array_push($items, $productInfo['items']);
                    }
                } else {
                    $result['invalid'][] = $productInfo['data'];
                }
            }
            if ($info['invalidExists']) {
                $imp = implode(', ', $info['invalidExists']);
                $result['invalid'][] = array(
                    'message' => "<p style='margin: 0;text-align: left'>Артикула " . $imp . " уже существуют в плагине озона</p>",
                    'name' => $product['name'],
                    'url' => $responseUrl,
                );
            }
        }


        if (!empty($items)) {
            $categoryItems['items'] = $items;
            $this->_itemsToJsonFile($categoryItems, $setId);
        }

        $result['valid'] = $valid;

        return $result;
    }

    /** --------- Prepare category helpers --------- **/

    protected function _getBaseUrl()
    {
        return (waRequest::isHttps() ? 'https://' : 'http://') . wa()->getRouting()->getDomain(null, true);
    }

    protected function _getBaseBackUrl()
    {

        return wa()->getRootUrl(true) . wa()->getConfig()->getBackendUrl();
    }

    protected function _getBackendProductUrl($id)
    {
        return $this->_getBaseBackUrl() . "/shop/?action=products#/product/" . $id . "/";
    }

    /**
     * @param $productData
     * @param array $skus
     * @return array [
     *      'valid' => true/false,
     *      'data' => [
     *          ['url' => '..', 'message' => '..'],
     *           ..
     *      ]
     *   ]
     * @throws waDbException
     * @throws waException
     */
    public function getValuesCategorie($cat_id, $attr_id)
    {

        $api = $this->_getOzonApi();
        return $api->getCategoryValueAttribute($cat_id, $attr_id);

    }
    public function getValuesExample($cat_id, $attr_id)
    {
        $api = $this->_getOzonApi();
        return $api->getCategoryExampleValueAttribute($cat_id, $attr_id);

    }


    protected function _validateProduct($productData, $ozonRequiredAttributes, $reqAttr, $ozonOptionalAttributes =array(),$optAttr = array(), $skus = array())
    {


        $imagesModel = new shopProductImagesModel();
        $skusModel = new shopProductSkusModel();
        $taxModel = new shopTaxRegionsModel();
        $model = new waModel();
        $response = array();
        $data = array();
        $errorFields = array();
        $features = $this->_getProductFeatures($productData['id']);
        $settingAttrName = $this->getSettingPlugin('features_name');
        $settingAttrPrice = $this->getSettingPlugin('features_price');
        if($settingAttrName && $settingAttrName !=''){
            $productFeatureModel = new shopProductFeaturesModel();
            $featuresSkuName = $productFeatureModel->getValues($productData['id']);
            $data['name'] = $featuresSkuName[$settingAttrName];
        }
        else {
            if ($productData['name'] && mb_strlen($productData['name']) < 500) {

                $prodData = htmlspecialchars_decode($productData['name']);
                if (!empty($skus)) {
                    $data['name'] = "";

                } else {
                    $data['name'] = $prodData;
                }

            } else {
                $errorFields[] = 'name';
            }
        }
        $settingSetDescriptionAttr = $this->getSettingPlugin('set_ann_attr');
        if ($settingSetDescriptionAttr) {

            if ($features['Аннотация']) {
                $data['description'] = strip_tags($features['Аннотация']);
            } else {
                $errorFields[] = 'У товара нет описания.';
            }
        }
        else {
            if ($productData['description']) {
                $data['description'] = strip_tags($productData['description']);
            } else {
                $errorFields[] = 'У товара нет описания.';
            }
        }
        $settingPricePremium = $this->getSettingPlugin('features_price_premium');
        if($settingPricePremium && $settingPricePremium !=''){


            $productFeatureModel = new shopProductFeaturesModel();
            $featuresSkuPremiumPrice = $productFeatureModel->getValues($productData['id']);
            $data['premium_price'] = $featuresSkuPremiumPrice[$settingPricePremium];
        }


        $data['category_id'] = (int)$productData['category_id'];
        if ($productData['tax_id'] && $productData['tax_id'] !== 0) {
            $tax = number_format($taxModel->getByTax($productData['tax_id'])[0]['tax_value'], 0);
            $vat = $tax * 0.01;
            $data['vat'] = (string)$vat;
        } else {
            $data['vat'] = "0";
        }

            $images = $imagesModel->getImages($productData['id'], '900');
            $main_def_image = "";

            if (!empty($images)) {
                $image = array();
                $isDefault = false;
                foreach ($images as $id => $imgData) {
                    if ($id == $productData['image_id']) {
                        $isDefault = true;
                    }
                    $image[] = $this->_getBaseUrl() . $imgData['url_900'];
                    if (in_array(mb_strtolower($imgData['ext']), array('jpg', 'png', 'jpeg')) && mb_strlen($image['file_name']) < 1000) {
                        if ($isDefault && $productData['image_id']) {
                            $dataImage = array(
                                "product_id" => $productData['id'],
                                "id" => $productData['image_id'],
                                "ext" => $productData['ext'],
                            );
                            $urlImage = shopImage::getUrl($dataImage, "900", true);
                            $main_def_image = !empty($urlImage) ? $urlImage : "";
                        }
                    }
                }
                $data['images'] = $image;

            } else {
                $errorFields[] = "У товара нет изображений.";
            }



        /**
         * Валидация обязательных параметров width,depth,height,dimension_unit,
         * weight, weight_unit
         */
        $settingSetMandatoryAttr = $this->getSettingPlugin('set_mandatory_attr');

        if ($settingSetMandatoryAttr) {

            $requiredParamErrors = array();
            $productFeatureModel = new shopProductFeaturesModel();

            if (!empty($skus)) {
                foreach ($skus as $sku) {
                    $featuresMandatorySku = $productFeatureModel->getValues($productData['id'], $sku);
                    foreach ($featuresMandatorySku as $key => $feature) {
                        switch ($key) {
                            case "dimension_unit":
                                $data['dimension_unit'] = (string)$feature;
                                break;
                            case "height":

                                $data['height'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "weight_unit":

                                $data['weight_unit'] = (string)$feature;
                                break;
                            case "depth":

                                $data['depth'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "weight":

                                $data['weight'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "width":

                                $data['width'] = !is_object($feature) ? $feature : $feature->value;
                                break;

                        }

                    }

                    $attrArr = array("dimension_unit", "height", "weight_unit", "depth", "weight", "width");
                    foreach ($attrArr as $item) {
                        if (!array_key_exists($item, $data)) {
                            $requiredParamErrors[] = 'Отсутствует или пустая обязательная характеристика ' . $item;
                        }


                    }

                }
                $arr = array(
                    'height'=>$data['height'],
                    'depth'=>$data['depth'],
                    'weight'=> $data['weight'],
                    'width'=>$data['width']);
                foreach ($arr as $key=>$it){
                    if(!ctype_digit($it)){
                        $requiredParamErrors[] = 'Значение обязательного параметра '.$key.' указано неверно (должно быть целое число).';
                    }
                    else{
                        $data[$key] = (int)($data[$key]);
                    }
                }
            }
            else{
                $featuresMandatory = $productFeatureModel->getValues($productData['id'], null);
                foreach ($featuresMandatory as $key => $feature) {
                    switch ($key) {
                        case "dimension_unit":
                            $data['dimension_unit'] = (string)$feature;
                            break;
                        case "height":

                            $data['height'] = !is_object($feature) ? $feature : $feature->value;
                            break;
                        case "weight_unit":

                            $data['weight_unit'] = (string)$feature;
                            break;
                        case "depth":

                            $data['depth'] = !is_object($feature) ? $feature : $feature->value;
                            break;
                        case "weight":

                            $data['weight'] = !is_object($feature) ? $feature : $feature->value;
                            break;
                        case "width":

                            $data['width'] = !is_object($feature) ? $feature : $feature->value;
                            break;

                    }

                }

                $attrArr = array("dimension_unit", "height", "weight_unit", "depth", "weight", "width");
                foreach ($attrArr as $item) {
                    if (!array_key_exists($item, $data)) {
                        $requiredParamErrors[] = 'Отсутствует или пустая обязательная характеристика ' . $item;
                    }
                }
                $arr = array(
                    'height'=>$data['height'],
                    'depth'=>$data['depth'],
                    'weight'=> $data['weight'],
                    'width'=>$data['width']);
                foreach ($arr as $key=>$it){
                    if(!ctype_digit($it)){
                        $requiredParamErrors[] = 'Значение обязательного параметра '.$key.' указано неверно (должно быть целое число).';
                    }
                    else{
                        $data[$key] = (int)($data[$key]);
                    }
                }
            }


        } else {
            $params = $this->_getProductParams($productData['id']);
            $requiredParamErrors = array();

            foreach ($this->_requiredParams as $name => $paramData) {
                if (array_key_exists($name, $params)) {
                    if (isset($this->_requiredParams[$name]['allowed'])) {
                        if (in_array($params[$name], $this->_requiredParams[$name]['allowed'])) {
                            $data[$name] = $params[$name];
                        } else {
                            $requiredParamErrors[] = 'Значение обязательного параметра ' . $name . ' указано неверно.';
                        }
                    }

                } else {
                    $requiredParamErrors[] = 'Отсутствует обязательный параметр ' . $name;
                }
            }

            foreach ($params as $name => $value) {
                if (array_key_exists($name, $this->_requiredParams) && !isset($this->_requiredParams[$name]['allowed'])) {
                    if ($name == 'weight') {
                        if (isset($data['weight_unit'])) {
                            if ($value <= $this->_requiredParams[$name][$data['weight_unit']]) {
                                $data[$name] = $value;
                            } else {
                                $requiredParamErrors[] = 'Обязательный параметр ' . $name . ' превышает максимальное допустимое значение.';
                            }
                        }
                    } else {
                        if (isset($data['dimension_unit'])) {
                            if ($value <= $this->_requiredParams[$name][$data['dimension_unit']]) {
                                $data[$name] = $value;
                            } else {
                                $requiredParamErrors[] = 'Обязательный параметр ' . $name . ' превышает максимальное допустимое значение.';
                            }
                        }
                    }
                }
            }

            $arr = array(
                'height'=>$data['height'],
                'depth'=>$data['depth'],
                'weight'=> $data['weight'],
                'width'=>$data['width']);
            foreach ($arr as $key=>$it){
                if(!ctype_digit($it)){
                    $requiredParamErrors[] = 'Значение обязательного параметра '.$key.' указано неверно (должно быть целое число).';
                }
                else{
                    $data[$key] = (int)($data[$key]);
                }
            }
        }
        /**
         * Валидация обязательных атрибутов категории
         */

        $requiredAttributesErrors = array();
        $data['attributes'] = array();
        if ($ozonRequiredAttributes[0]) {
            foreach ($ozonRequiredAttributes[0] as $key => $attr) {
                    if ($attr['name'] == "Изображение")
                        $features['Изображение'] = $main_def_image;
                if (array_key_exists($attr['name'], $features)) {
                    $arrValue[$attr['name']] = $features[$attr['name']];
                    $reqAttrRes = $reqAttr[$attr['name']];
                        $attributeValue = $this->_attrFormatValue_v2($attr, $arrValue, $data['category_id'], $reqAttrRes);
                    if ($attributeValue) {

                        $data['attributes'][$attributeValue['id']] = $attributeValue;
                    } else {
                        $requiredAttributesErrors[] = 'Значение обязательного атрибута ' . $attr['name'] . ' не соответствует ни одному значению предустановленному на Ozon (информация на странице "Атрибуты товаров").';
                    }
                } else {

                    if (!empty($skus)) {
                        foreach ($skus as $sku) {
                            $skuFeatures = $this->_getProductFeatures($productData['id'], $sku);

                            if (!array_key_exists($attr['name'], $skuFeatures)) {
                                $requiredAttributesErrors[] = 'Обязательный атрибут ' . $attr['name'] . ' не указан.';
                            }
                        }
                    } else {
                        $requiredAttributesErrors[] = 'Обязательный атрибут ' . $attr['name'] . ' не указан.';
                    }
                }

            }
        }

            if ($data['description'] != "") {
                $valMasDesc[] = array(
                    'value' => (string)$data['description']
                );
                $arrayRes = array(
                    'id' => (int)4191,
                    'values' => $valMasDesc
                );
                $data['attributes'][] = $arrayRes;
            }

        unset($data['description']);
        if (!isset($data['attributes'])) {
            $errorFields[] = 'Обязательные характеристики для товара не заполнены.';
        }

        if (array_key_exists('Бренд', $features)) {
            if (!is_array($features['Бренд']) && mb_strlen($features['Бренд']) <= 100) {
                $data['vendor'] = (string)$features['Бренд'];
            }
        }

        if (array_key_exists('Код производителя', $features)) {
            if (!is_array($features['Код производителя']) && mb_strlen($features['Код производителя']) <= 100) {
                $data['vendor_code'] = (string)$features['Код производителя'];
            }
        }


        /**
         * Обработка вариантов товара
         */
        $ozonPriceMarkup = $this->getMatchedCategories();
        $ozonPriceMarkupSet = $this->getMatchedSets();
        $productFeatureModel = new shopProductFeaturesModel();

        if (!empty($skus)) {
            $skuVariants = array();
            foreach ($skus as $sku) {
                $skuData = $skusModel->getSku($sku);
                if (empty($skuData['sku'])) {
                    $errorFields[] = 'У товара отсутствует код артикулa.';
                }

                if($settingAttrName && $settingAttrName !=''){
                    unset($data['name']);
                    $featuresSkuName = $productFeatureModel->getValues($productData['id'], $sku);
                    $skuItem['name'] = $featuresSkuName[$settingAttrName];
                }
                $settingPricePremium = $this->getSettingPlugin('features_price_premium');
                if($settingPricePremium && $settingPricePremium !=''){
                    $featuresSkuPremiumPrice = $productFeatureModel->getValues($productData['id'], $sku);
                    $skuItem['premium_price'] = $featuresSkuPremiumPrice[$settingPricePremium];
                }

                $skuName = $skuData['name'];
                $skuItem['name_sku'] = $skuName;



                if($settingAttrPrice && $settingAttrPrice != ""){
                    $featuresSku = $productFeatureModel->getValues($productData['id'], $sku);
                    $skuItem['price'] = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $skuItem['price'] = str_replace(",", "", number_format((float)$skuData['price'], 2));
                }
                $skuItem['offer_id'] = $skuData['sku'];
                $dataSkuPrice = $skuItem['price'];

                if(!$settingAttrPrice && $settingAttrPrice == "") {
                    $skuItem['price'] = $this->getCurrencyPrice($skuItem['price'], $productData['currency']);
                }

                $resPrice = ($skuItem['price'] / 100 * $productData['markup']) + $skuItem['price'];
                $skuItem['price'] = str_replace(",", "", number_format((float)$resPrice, 2));

                if ($skuItem['price'] == 0) {
                    $errorFields[] = 'У товара отсутствует цена.';
                }

                if ($skuData['compare_price'] && $skuData['compare_price'] > $dataSkuPrice) {
                    $skuItem['old_price'] = str_replace(",", "", number_format((float)$skuData['compare_price'], 2));
                    if(!$settingAttrPrice && $settingAttrPrice == "") {
                        $skuItem['old_price'] = $this->getCurrencyPrice($skuItem['old_price'], $productData['currency']);
                    }

                    $resPrice = ($skuItem['old_price'] / 100 * $productData['markup']) + $skuItem['old_price'];
                    $skuItem['old_price'] = str_replace(",", "", number_format((float)$resPrice, 2));

                }

                $skuFeatures = $this->_getProductFeatures($productData['id'], $sku);
                $featuresMandatorySku = $productFeatureModel->getValues($productData['id'], $sku);
                if($settingSetMandatoryAttr) {
                    foreach ($featuresMandatorySku as $key => $feature) {
                        switch ($key) {
                            case "dimension_unit":
                                $skuItem['dimension_unit'] = (string)$feature;
                                break;
                            case "height":

                                $skuItem['height'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "weight_unit":

                                $skuItem['weight_unit'] = (string)$feature;
                                break;
                            case "depth":

                                $skuItem['depth'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "weight":

                                $skuItem['weight'] = !is_object($feature) ? $feature : $feature->value;
                                break;
                            case "width":

                                $skuItem['width'] = !is_object($feature) ? $feature : $feature->value;
                                break;

                        }
                    }

                    $arr = array(
                        'height'=>$skuItem['height'],
                        'depth'=>$skuItem['depth'],
                        'weight'=> $skuItem['weight'],
                        'width'=>$skuItem['width']);
                    foreach ($arr as $key=>$it){
                        if(!ctype_digit($it)){
                            $requiredParamErrors[] = 'Значение обязательного параметра '.$key.' указано неверно (должно быть целое число).';
                        }
                        else{
                            $skuItem[$key] = (int)($skuItem[$key]);
                        }
                    }

                }

                $skuItem['attributes'] = array();
                $skuItem['barcode'] = $skuFeatures['GTIN'];

                if ($ozonRequiredAttributes[0]) {
                    foreach ($ozonRequiredAttributes[0] as $key => $attr) {

                        if (array_key_exists($attr['name'], $skuFeatures)) {
                            $arrValueSku[$attr['name']] = $skuFeatures[$attr['name']];
                            $reqAttrRes = $reqAttr[$attr['name']];


                                $attributeValue = $this->_attrFormatValue_v2($attr, $arrValueSku, $data['category_id'], $reqAttrRes);

                            if ($attributeValue) {
                                //array_push($skuItem['attributes'], $attributeValue);
                                $skuItem['attributes'][$attributeValue['id']] = $attributeValue;
                            }
                        }
                        unset($arrValueSku);
                    }
                }


                if ($ozonOptionalAttributes[0]) {
                    foreach ($ozonOptionalAttributes[0] as $key => $attr) {
                        if (array_key_exists($attr['name'], $skuFeatures)) {
                            $arrValueSku[$attr['name']] = $skuFeatures[$attr['name']];
                            $optAttrRes = $optAttr[$attr['name']];
                                $attributeValue = $this->_attrFormatValue_v2($attr, $arrValueSku, $data['category_id'], $optAttrRes);
                            if ($attributeValue) {
                                //array_push($skuItem['attributes'], $attributeValue);
                                $skuItem['attributes'][$attributeValue['id']] = $attributeValue;
                            }
                        }

                    }
                }
                $skuVariants[] = $skuItem;
                $skuItem = array();
            }
            $data['sku_variants'] = $skuVariants;
        } else {
            if ($productData['price']) {
                if($settingAttrPrice && $settingAttrPrice != ""){
                    $featuresSku = $productFeatureModel->getValues($productData['id']);
                    $data['price'] = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{

                    $data['price'] = str_replace(",", "", number_format((float)$productData['price'], 2));
                }

                $dataPrice = $data['price'];
                if(!$settingAttrPrice && $settingAttrPrice == "") {
                    $currencyDefault = $model->query("SELECT code FROM shop_currency WHERE sort = i:id", array('id' => 0))->fetch()['code'];
                    if ($currencyDefault != 'RUB') {
                        $data['price'] = $this->getCurrencyPriceValidateNoSku($data['price'], $productData['currency']);
                        $data['price'] = str_replace(",", "", number_format((float)$data['price'], 2));
                    }

                }


                $resPrice = ($data['price'] / 100 * $productData['markup']) + $data['price'];
                $data['price'] = str_replace(",", "", number_format((float)$resPrice, 2));

                $oldPrice = str_replace(",", "", number_format((float)$productData['compare_price'], 2));
                if ($oldPrice > $dataPrice) {

                    if(!$settingAttrPrice && $settingAttrPrice == "") {
                        $currencyDefault = $model->query("SELECT code FROM shop_currency WHERE sort = i:id", array('id' => 0))->fetch()['code'];
                        if ($currencyDefault != 'RUB') {
                            $oldPrice = $this->getCurrencyPriceValidateNoSku($oldPrice, $productData['currency']);
                            $oldPrice = str_replace(",", "", number_format((float)$oldPrice, 2));
                        }
                    }
                    $data['old_price'] = $oldPrice;
                    if ($ozonPriceMarkup) {
                        if (isset($productData['markup_valid_cat'])) {
                            if ($productData['markup'] > 0) {

                                $resPrice = ($data['old_price'] / 100 * $productData['markup']) + $data['old_price'];
                                $data['old_price'] = str_replace(",", "", number_format((float)$resPrice, 2));
                            }
                        }
                    }
                    if ($ozonPriceMarkupSet) {
                        if (isset($productData['markup_valid_set'])) {
                            if ($productData['markup'] > 0) {
                                $resPrice = ($data['old_price'] / 100 * $productData['markup']) + $data['old_price'];
                                $data['old_price'] = str_replace(",", "", number_format((float)$resPrice, 2));
                            }
                        }
                    }
                }
            } else {
                $errorFields[] = 'У товара отсутствует цена';
            }

            if ($productData['sku_id']) {
                $sku = $skusModel->getSku($productData['sku_id']);
                $data['offer_id'] = $sku['sku'];
            }

            if (!$data['offer_id']) {
                $errorFields[] = 'У товара отсутствует код артикулa.';
            }

            //$ozonOptionalAttributes = $this->getOptionalAttributeList($data['category_id']);
            if ($ozonOptionalAttributes[0]) {
                foreach ($ozonOptionalAttributes[0] as $key => $attr) {
                    if (array_key_exists($attr['name'], $features)) {
                        //$value = $features[$attr['name']];
                        $arrValueSku[$attr['name']] = $features[$attr['name']];
                        $optAttrRes = $optAttr[$attr['name']];
                            $attributeValue = $this->_attrFormatValue_v2($attr, $arrValueSku, $data['category_id'], $optAttrRes);

                        if ($attributeValue) {
                            array_push($data['attributes'], $attributeValue);
                        }
                    }
                }
            }
            if (array_key_exists('GTIN', $features)) {
                if (!is_array($features['GTIN'])) {
                    $data['barcode'] = (string)$features['GTIN'];
                }
            }
        }

        if (empty($errorFields) && empty($requiredAttributesErrors) && empty($requiredParamErrors)) {
            if ($skus) {

                $response['items'] = array();

                foreach ($data['sku_variants'] as $key => $skuData) {
                    $prodData = htmlspecialchars_decode($productData['name']);
                    $data['name'] = $prodData . "," . $skuData['name_sku'];
                    $dataCopy = $data;

                    unset($dataCopy['sku_variants'], $dataCopy['sku_model_name']);

                    foreach ($skuData['attributes'] as $key => $attribute) {
                        $dataCopy['attributes'][$key] = $attribute;
                    }
                    unset($skuData['attributes']);

                    $newData = array_values($dataCopy['attributes']);

                    $dataCopy['attributes'] = $newData;

                    array_push($response['items'], array_merge($dataCopy, $skuData));
                    //array_push($response['items'], $skuData);


                }

                $response['valid'] = true;
            } else {
                $newData = array_values($data['attributes']);
                $data['attributes'] = $newData;
                $response['items'] = $data;
                $response['valid'] = true;
            }
        } else { // есть ошибки в данных товара
            $responseMessage = '';
            foreach ($errorFields as $error) {
                $responseMessage .= "<p style='margin: 0; text-align: left'>" . $error . "</p>";
            }
            foreach ($requiredAttributesErrors as $error) {
                $responseMessage .= "<p style='margin: 0;text-align: left'>" . $error . "</p>";
            }
            foreach ($requiredParamErrors as $error) {
                $responseMessage .= "<p style='margin: 0;text-align: left'>" . $error . "</p>";
            }
            $responseUrl = $this->_getBackendProductUrl($productData['id']);
            $response['valid'] = false;
            $response['data'] = array('name' => $productData['name'], 'url' => $responseUrl, 'message' => $responseMessage);
        }
        //waLog::log(print_r($response, true), '$response.log');
        return $response;
    }

    protected function _getProductFeatures($productId, $skuId = null)
    {
        $productFeatureModel = new shopProductFeaturesModel();
        $featureModel = new shopFeatureModel();
        $features = $productFeatureModel->getValues($productId, -$skuId);
        $rebased = [];
        foreach ($features as $code => $value) {
            $dataFeature = $featureModel->getByCode($code);
            $name = $dataFeature['name'];
            if ($dataFeature['type'] == 'color' && is_array($value)) {
                $newData = array();
                foreach ($value as $key => $p_val) {
                    $newData[$key] = is_object($p_val) ? $p_val->value : $p_val['value'];
                }
                $rebased[$name] = $newData;

            } else {
                $rebased[$name] = $value;
            }
        }
        return $rebased;
    }
    protected function _getProductParams($productId)
    {
        $productParamsModel = new shopProductParamsModel();
        $params = $productParamsModel->get($productId);
        return $params;
    }

    public function getProductSkuCount($id)
    {
        $model = new waModel();
        $countQuery = "SELECT count(*) as cnt FROM shop_product_skus WHERE product_id = i:id";
        return $model->query($countQuery, array('id' => $id))->fetch()['cnt'];
    }

    public function checkProduct($productId)
    {
        $model = new shopProductModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        $resultArray = [];

        $productSkus = $model->query("SELECT id FROM shop_product_skus WHERE product_id = i:id", array('id' => $productId))->fetchAll();
        if (count($productSkus) > 1) {
            $notSentSku = [];
            $resultArray['isNotSend'] = false;
            foreach ($productSkus as $key => $value) {
                $result = $model->query("SELECT id, ozon_offer_id FROM " . self::TABLE_PRODUCTS . " WHERE shop_product_sku_id = i:id", array('id' => $value['id']))->fetchAll();
                if (count($result) == 0) {
                    $notSentSku[] = $value['id'];
                }
                else{
                    if (isset($plugin_logs) && $plugin_logs) {
                        waLog::log("Product error check card: товар с артикулом " . $result[0]['ozon_offer_id'] . " уже существует в озоне", "shopOzonius.log");
                        $resultArray['invalidExists'][] = $result[0]['ozon_offer_id'];
                    }
                }
            }
            if (!empty($notSentSku)) {
                $resultArray['isNotSend'] = true;
                $resultArray['notSentSku'] = $notSentSku;
            }
        } else {
            $result = $model->query("SELECT id, ozon_offer_id  FROM " . self::TABLE_PRODUCTS . " WHERE shop_product_id = i:id", array('id' => $productId))->fetchAll();
            if ($result){
                $resultArray['isNotSend'] = false;
                if (isset($plugin_logs) && $plugin_logs) {
                    waLog::log("Product error check card: товар с артикулом " . $result[0]['ozon_offer_id'] . " уже существует в озоне", "shopOzonius.log");
                    $resultArray['invalidExists'][] = $result[0]['ozon_offer_id'];
                }
            }
            else{
                $resultArray['isNotSend'] = true;
            }

        }

        return $resultArray;
    }

    public function checkProductUpdate($productId, $ozonId)
    {
        $model = new shopProductModel();
        $resultArray = [];

        $productSkus = $model->query("SELECT id FROM shop_product_skus WHERE product_id = i:id", array('id' => $productId))->fetchAll();
        $productSkusOzon = $model->query("SELECT shop_product_sku_id FROM shop_ozonius_products WHERE ozon_product_id = i:id", array('id' => $ozonId))->fetch()['shop_product_sku_id'];
        if (count($productSkus) > 1) {
            $notSentSku = [];
            $resultArray['isNotSend'] = false;
            foreach ($productSkus as $key => $value) {
                if ($value['id'] == $productSkusOzon) {
                    $notSentSku[] = $value['id'];
                }
            }
            if (!empty($notSentSku)) {
                $resultArray['isNotSend'] = true;
                $resultArray['notSentSku'] = $notSentSku;
            }
        } else {
            $result = $model->query("SELECT id FROM " . self::TABLE_PRODUCTS . " WHERE shop_product_id = i:id", array('id' => $productId))->fetchAll();
            $resultArray['isNotSend'] = $result ? false : true;
        }

        return $resultArray;
    }

    protected function _itemsToJsonFile($items, $id)
    {
        $path = $this->getPluginDataPath() . "categories/";
        waFiles::write($path . 'category_' . $id . '.json', json_encode($items, true));
    }


    protected function _attrFormatValue_v2($attr, $value, $catId, $attrValueOzon)
    {
        $arrayRes = array();
        $settingSetAnnAttr = $this->getSettingPlugin('set_ann_attr');

        if ($attr['dictionary_id'] == 0) {
            if($settingSetAnnAttr){
                $valMas[] = array(
                    'value' => (string)$value[$attr['name']]
                );
                $arrayRes = array(
                    'id' => (int)$attr['id'],
                    'values' => $valMas
                );
            }
            else {
                if ($attr['id'] != 4191) {

                    $valMas[] = array(
                        'value' => (string)$value[$attr['name']]
                    );
                    $arrayRes = array(
                        'id' => (int)$attr['id'],
                        'values' => $valMas
                    );
                }
            }
        } elseif ($attr['is_collection'] == 1) {
            if (is_array($value[$attr['name']])) {

                $CategoryValueAttribute = $attrValueOzon;

                $arrf = array();
                foreach ($CategoryValueAttribute as $itemAttr) {

                    foreach ($value as $key => $items_val) {
                        if ($key == $attr['name']) {
                            foreach ($items_val as $item_val) {



                                if ($itemAttr['value'] == $item_val) {
                                    $arrf[] = array('dictionary_value_id' => $itemAttr['id']);
                                }
                            }
                        }
                    }

                    $arrayRes = array(
                        'id' => (int)$attr['id'],
                        'values' => $arrf
                    );
                }
                $arrayRes['values'] = array_map("unserialize", array_unique(array_map("serialize", $arrayRes['values'])));

            }else{

                $CategoryValueAttribute = $attrValueOzon;
                $arrf = array();

                foreach ($CategoryValueAttribute as $itemAttr) {
                    if ($itemAttr['value'] == $value[$attr['name']]) {
                        $arrf[] = array('dictionary_value_id' => $itemAttr['id']);

                    }
                    $arrayRes = array(
                        'id' => (int)$attr['id'],
                        'values' => $arrf
                    );
                }
            }
        } else {

            $CategoryValueAttribute = $attrValueOzon;

            foreach ($CategoryValueAttribute as $item) {
                if ($item['value'] == $value[$attr['name']]) {

                    $valMas[] = array(
                        'dictionary_value_id' => $item['id']
                    );
                    $arrayRes = array(
                        'id' => (int)$attr['id'],
                        'values' => $valMas
                    );
                }
            }
            if($attr['name'] == 'Бренд' || $attr['name'] == 'Бренд в одежде и обуви') {
                if(empty($arrayRes)){

                        $valMas[] = array(
                            'dictionary_value_id' => (int)126745801 // Не найден бренд
                        );
                        $arrayRes = array(
                            'id' => (int)$attr['id'],
                            'values' => $valMas
                        );
                }
            }

            $arrayRes['values'] = array_map("unserialize", array_unique(array_map("serialize", $arrayRes['values'])));
        }
        return (!empty($arrayRes['id']) && !empty($arrayRes['values'])) ? $arrayRes : false;
    }


    /**
     * @param $items
     * @param $shopCategoryId
     */
    public function reflectItemsInfo($items, $shopCategoryId, $taskId)
    {
        $model = new waModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        if (!empty($items) && $shopCategoryId && $taskId) {
            foreach ($items as $key => $item) {
                $ozoniusCategoryId = $model->query("SELECT id FROM " . self::TABLE_CATEGORY . " WHERE shop_category_id = i:id", array('id' => $shopCategoryId))->fetch()['id'];
                $ozonOfferId = $item['offer_id'];
                $skuInfo = $model->query("SELECT id,product_id FROM shop_product_skus WHERE sku = s:sku", array('sku' => $ozonOfferId))->fetch();

                if (!empty($skuInfo['id']) && !empty($skuInfo['product_id'])) {
                    $findAddedItem = $model->query("SELECT id FROM " . self::TABLE_PRODUCTS . " WHERE shop_product_id = i:id AND shop_product_sku_id = i:sku", array('id' => $skuInfo['product_id'], 'sku' => $skuInfo['id']))->fetch();
                    if ($findAddedItem) continue;
                }

                $insertQuery = "INSERT INTO " . self::TABLE_PRODUCTS . " (ozonius_category_id,shop_product_id,shop_product_sku_id,ozon_offer_id,create_task_id) 
                                    VALUES (i:cat,i:prod,i:sku,s:offer,i:task)";

                try {
                    $model->query($insertQuery, array(
                        'cat' => $ozoniusCategoryId,
                        'prod' => $skuInfo['product_id'],
                        'sku' => $skuInfo['id'],
                        'offer' => $ozonOfferId,
                        'task' => $taskId
                    ));
                } catch (Error $error) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log($error->getMessage(), 'shopOzonius.log');
                }
            }
        }
    }

    public function reflectItemsSetInfo($items, $shopSetId, $taskId)
    {
        $model = new waModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        if (!empty($items) && $shopSetId && $taskId) {
            foreach ($items as $key => $item) {
                $ozoniusCategoryId = $model->query("SELECT id FROM " . self::TABLE_SETS . " WHERE shop_sets_id = s:id", array('id' => $shopSetId))->fetch()['id'];
                $ozonOfferId = $item['offer_id'];
                $skuInfo = $model->query("SELECT id,product_id FROM shop_product_skus WHERE sku = s:sku", array('sku' => $ozonOfferId))->fetch();

                if (!empty($skuInfo['id']) && !empty($skuInfo['product_id'])) {
                    $findAddedItem = $model->query("SELECT id FROM " . self::TABLE_PRODUCTS . " WHERE shop_product_id = i:id AND shop_product_sku_id = i:sku", array('id' => $skuInfo['product_id'], 'sku' => $skuInfo['id']))->fetch();
                    if ($findAddedItem) continue;
                }

                $insertQuery = "INSERT INTO " . self::TABLE_PRODUCTS . " (ozonius_category_id, ozonius_sets_id,shop_product_id,shop_product_sku_id,ozon_offer_id,create_task_id) 
                                    VALUES (i:cat,i:set,i:prod,i:sku,s:offer,i:task)";

                try {
                    $model->query($insertQuery, array(
                        'cat' => "0",
                        'set' => $ozoniusCategoryId,
                        'prod' => $skuInfo['product_id'],
                        'sku' => $skuInfo['id'],
                        'offer' => $ozonOfferId,
                        'task' => $taskId
                    ));
                } catch (Error $error) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log($error->getMessage(), 'shopOzonius.log');
                }
            }
        }
    }

    public function reflectTaskInfo($taskId, $categoryId)
    {
        $model = new waModel();
        $model->query("INSERT INTO " . self::TABLE_TASKS . " (task_id,shop_category_id) VALUES (i:task,i:cat)", array('task' => $taskId, 'cat' => $categoryId));
    }

    public function reflectTaskSetInfo($taskId, $setId)
    {
        $model = new waModel();
        $model->query("INSERT INTO " . self::TABLE_TASKS . " (task_id, shop_sets_id) VALUES (i:task,s:sets)", array('task' => $taskId, 'sets' => $setId));
    }

    /** ---------- CRON TASKS ------------ **/


    public function updateProductStockSend($productIds = array(), $off = 0, $limit = 99)
    {
        $plugin_logs = $this->getSettingPlugin('log_request');
        $api = $this->_getOzonApi();
        $model = new waModel();
        $stocks = array();
        $result = array('success' => 0, 'withErrors' => 0, 'offerIds' => array());
        $getStocks = $this->getStocksFromSettings();
        $getWarehouses = $this->getWarehouseFromSettings();
        $get_fbs = $this->getSettingPlugin('get_fbs');
        $infinity = $this->getSettingPlugin('infinity');
        $disabled_stock = $this->getSettingPlugin('disabled_stock');
        $productIds = $model->query("SELECT ozon_product_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id IS NOT NULL LIMIT 99 OFFSET {$off}")->fetchAll();

        if (empty($productIds)) {
            return null;
        }
        $off += $limit;
        $this->updateProductStockSend($productIds, $off);

        if($get_fbs == "On"){
            if (!empty($getWarehouses) && $productIds) {
                foreach ($productIds as $productId) {
                    $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id, shop_product_sku_id, ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                    $params = $this->_getProductParams($productInfo['shop_product_id']);
                    if ($params['ozon.ignored'] == 1) {
                        if (isset($plugin_logs) && $plugin_logs)
                            waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                        continue;
                    }
                    foreach ($getWarehouses as $warehouse) {
                        $getStShop = explode(';', $warehouse['shop']);
                        $getStOzon = explode(';', $warehouse['ozon']);
                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {

                            $count = $model->query("SELECT SUM(`count`) FROM shop_product_stocks WHERE sku_id = i:id AND stock_id in ({$getStShop[1]})", array  ('id' => $productInfo['shop_product_sku_id']))->fetch()['SUM(`count`)'];
                            if(!isset($count)){
                                $count = $infinity;
                            }
                            $availablePrice = $model->query("SELECT price, available FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch();

                            if($availablePrice['price'] == 0 || $availablePrice['available'] == 0){
                                $count = 0;
                            }

                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }
                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count,
                                'warehouse_id' => (int)$getStOzon[1]
                            );
                            $stocks[] = $data;

                        }
                    }
                }
                $final_array = [];
                foreach($stocks as $arr){
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['product_id'] = $arr['product_id'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['offer_id'] = $arr['offer_id'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock'] = (isset($final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock']))? $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock'] + $arr['stock'] : $arr['stock'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['warehouse_id'] = $arr['warehouse_id'];
                }

                $final_array = array_values($final_array);
                $response = array();
                if(count($final_array) > 99){
                    $chunkStocks = array_chunk($final_array, 100);

                    foreach ($chunkStocks as $chunkStock){
                        $response = $api->productUpdateStocksWarehouse($chunkStock);
                    }
                }
                else{
                    $response = $api->productUpdateStocksWarehouse($final_array);
                }

                if (!$response) {
                    return $result['message'] = "Для данного товара не указаны остатки в магазине.";
                }

                foreach ($response as $item) {
                    if ($item['updated']) {
                        $result['success']++;
                    } else {
                        $result['withErrors']++;
                        $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                    }
                }

            }
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log("Product update stocks: " . json_encode($result, 1), "shopOzonius.log");

            return $result;

        }
        else {
            if ($productIds) {

                if (!empty($getStocks)) {

                    foreach ($productIds as $productId) {
                        $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id, shop_product_sku_id, ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();

                        $params = $this->_getProductParams($productInfo['shop_product_id']);
                        if ($params['ozon.ignored'] == 1) {
                            if (isset($plugin_logs) && $plugin_logs)
                                waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                            continue;
                        }
                        $getStocksStr = implode(',', $getStocks);

                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {
                            $count = $model->query("SELECT SUM(`count`) FROM shop_product_stocks WHERE sku_id = i:id AND stock_id in ({$getStocksStr})", array('id' => $productInfo['shop_product_sku_id']))->fetch()['SUM(`count`)'];

                            if(!isset($count)){
                                $count = $infinity;
                            }
                            $availablePrice = $model->query("SELECT price, available FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch();

                            if($availablePrice['price'] == 0 || $availablePrice['available'] == 0){
                                $count = 0;
                            }
                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }
                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count
                            );
                            $stocks[] = $data;

                        }
                    }
                } else {
                    foreach ($productIds as $productId) {
                        $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id, shop_product_sku_id, ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                        $params = $this->_getProductParams($productInfo['shop_product_id']);
                        if ($params['ozon.ignored'] == 1) {
                            if (isset($plugin_logs) && $plugin_logs)
                                waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                            continue;
                        }
                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {
                            $shopProductSkus = $model->query("SELECT `count`, `price`, `available` FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch()['count'];

                            $count = $shopProductSkus['count'];
                            if($shopProductSkus['price'] == 0 || $shopProductSkus['available'] == 0){
                                $count = 0;
                            }
                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }
                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count
                            );
                            $stocks[] = $data;

                        }

                    }
                }

                $response = $api->productUpdateStocks($stocks);

                if (!$response) {
                    return $result['message'] = "Для данного товара не указаны остатки в магазине.";
                }

                foreach ($response as $item) {
                    if ($item['updated']) {
                        $result['success']++;
                    } else {
                        $result['withErrors']++;
                        $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                    }
                }
            }
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log("Product update stocks: " . json_encode($result, 1), "shopOzonius.log");

            return $result;
        }
    }

    public function updateProductPriceSend($productIds = array(), $off = 0, $limit = 999)
    {
        $api = $this->_getOzonApi();
        $model = new waModel();
        $prices = array();
        $result = array('success' => 0, 'withErrors' => 0, 'offerIds' => array());
        $plugin_logs = $this->getSettingPlugin('log_request');
        $productIds = $model->query("SELECT ozon_product_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id IS NOT NULL LIMIT 999 OFFSET {$off}")->fetchAll();
        if (empty($productIds)) {
            return null;
        }
        $off += $limit;
        $this->updateProductPriceSend($productIds, $off);

        if ($productIds) {
            foreach ($productIds as $productId) {

                $offerId = $model->query("SELECT shop_product_id, shop_product_sku_id, ozon_offer_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetchAll();
                $params = $this->_getProductParams($offerId['shop_product_id']);
                if ($params['ozon.ignored'] == 1) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log("Product update price: товар с артикулом " . $offerId['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                    continue;
                }
                $priceInfo = $model->query("SELECT p.currency, s.price, s.compare_price, t.tax_value, s.product_id  from shop_product_skus s
                                                join shop_product as p on p.id = s.product_id
                                                left outer join shop_tax_regions t on t.tax_id = p.tax_id
                                                where s.sku = s:sku", array('sku' => $offerId[0]['ozon_offer_id']))->fetch();

                if ($priceInfo['tax_value'] && $priceInfo['tax_value'] !== 0) {
                    $tax = number_format($priceInfo['tax_value'], 0);
                    $vat = $tax * 0.01;
                    $vatValue = (string)$vat;
                } else {
                    $vatValue = "0";
                }
                $settingAttrPrice = $this->getSettingPlugin('features_price');
                if($settingAttrPrice && $settingAttrPrice != ""){
                    $productFeatureModel = new shopProductFeaturesModel();
                    $featuresSku = $productFeatureModel->getValues($priceInfo['product_id'],$offerId[0]['shop_product_sku_id']);
                    $price = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $price = str_replace(",", "", number_format((float)$priceInfo['price'], 2));
                }
                $data = array(
                    'product_id' => (int)$productId['ozon_product_id'],
                    'offer_id' => $offerId[0]['ozon_offer_id'],
                    'price' => $price,
                    'vat' => $vatValue
                );

                $marCatQueryPrice = $model->query("SELECT t1.shop_category_id FROM " . self::TABLE_CATEGORY . " AS t1 
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_category_id
WHERE t2.ozon_product_id = {$data['product_id']}")->fetch()['shop_category_id'];
                $marSetQueryPrice = $model->query("SELECT t1.shop_sets_id FROM " . self::TABLE_SETS . " AS t1 
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_sets_id
WHERE t2.ozon_product_id = {$data['product_id']}")->fetch()['shop_sets_id'];

                $dataPrice = $data['price'];

                if(!$settingAttrPrice && $settingAttrPrice == ""){
                    $data['price'] = $this->getCurrencyPrice($data['price'],$priceInfo['currency']);
                }
                $data['price'] = str_replace(",", "", number_format((float)$data['price'], 2));

                if ($priceInfo['compare_price'] > 0 && $priceInfo['compare_price'] > $dataPrice) {
                    $data['old_price'] = $priceInfo['compare_price'];
                    if(!$settingAttrPrice && $settingAttrPrice == ""){
                        $data['old_price'] = $this->getCurrencyPrice($data['old_price'],$priceInfo['currency']);
                    }
                    $data['old_price'] = str_replace(",", "", number_format((float)$data['old_price'], 2));
                }


                $ozonCatPriceMarkup = $this->getMatchedCategories();
                $ozonSetPriceMarkup = $this->getMatchedSets();

                if ($ozonCatPriceMarkup) {
                    $data['price'] = $this->ozonPriceMarkup($ozonCatPriceMarkup, $data['price'], $marCatQueryPrice);
                    if(!empty($data['old_price'])) {
                        $data['old_price'] = $this->ozonPriceMarkup($ozonCatPriceMarkup, $data['old_price'], $marCatQueryPrice);
                    }
                }
                if ($ozonSetPriceMarkup) {
                    $data['price'] = $this->ozonPriceMarkup($ozonSetPriceMarkup, $data['price'], $marSetQueryPrice);
                    if(!empty($data['old_price'])) {
                        $data['old_price'] = $this->ozonPriceMarkup($ozonSetPriceMarkup, $data['old_price'], $marSetQueryPrice);
                    }
                }

                $prices[] = $data;
            }

            $response = $api->productUpdatePrices($prices);

            if (!$response) {
                return $result['message'] = "Для данного товара не указаны цены в магазине.";
            }

            foreach ($response as $item) {
                if ($item['updated']) {
                    $result['success']++;
                } else {
                    $result['withErrors']++;
                    $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                }
            }
        }
        if (isset($plugin_logs) && $plugin_logs)
            waLog::log("Product update prices: " . json_encode($result, 1), "shopOzonius.log");

        return $result;
    }


    public function updateProductInfo($offers =array(), $off = 0, $limit = 999)
    {
        $model = new waModel();
        $offers = $model->query("SELECT ozon_product_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id IS NOT NULL LIMIT 999 OFFSET {$off}")->fetchAll();

        if (empty($offers)) {
            return null;
        }
        $off += $limit;
        $this->updateProductInfo($offers, $off);
        $plugin_logs = $this->getSettingPlugin('log_request');
        $api = $this->_getOzonApi();
        $resArr = array();
        foreach ($offers as $key => $offer) {
            array_push($resArr, $offer['ozon_product_id']);
        }
        $data = $api->productInfoList($resArr);
        foreach ($data['items'] as $item) {

            $skuIsEnabled = '';
            $sources = '';

            if ($item) {
                if ($item['sources']) {
                    $skuIsEnabled = (int)$item['sources'][0]['is_enabled'];
                    $sources = json_encode($item['sources'], 1);
                }
                $stocks = json_encode($item['stocks'], 1);
                $visible = (int)$item['visible'];
                $visDetails = json_encode($item['visibility_details'], 1);

                    $validErrors = json_encode($item['errors'], 1);

                $dt = new DateTime($item['created_at']);
                $createdAt = $dt->format('Y-m-d H:i:s');

                $updateQuery = "UPDATE " . self::TABLE_PRODUCTS . " SET
                                    ozon_created_at = s:created,
                                    ozon_sku = i:sku,
                                    ozon_sources = s:sources,
                                    ozon_sku_is_enabled = s:isen,
                                    ozon_price = s:price,
                                    ozon_stock = i:stock,
                                    ozon_stocks = s:stocks,
                                    ozon_state = s:state,
                                    ozon_visible = s:visible,
                                    ozon_visibility_details = s:visdet,
                                    ozon_validation_errors = s:valerr
                                    WHERE ozon_product_id = s:prod
                                    ";
                try {
                    $model->query($updateQuery, array(
                        'created' => $createdAt,
                        'sku' => $item['sku'],
                        'sources' => $sources,
                        'isen' => $skuIsEnabled,
                        'price' => $item['price'],
                        'stock' => $item['stock'],
                        'stocks' => $stocks,
                        'state' => $item['state'],
                        'visible' => $visible,
                        'visdet' => $visDetails,
                        'valerr' => $validErrors,
                        'prod' => $item['id']
                    ));
                } catch (Exception $exception) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log($exception->getMessage(), "shopOzonius.log");
                }
            }
        }
    }

    public function updateTaskInfo()
    {

        $model = new waModel();
        $tasks = $model->query("SELECT task_id,items FROM " . self::TABLE_TASKS . " WHERE items is null or items <> 'all items imported'")->fetchAll();
        $plugin_logs = $this->getSettingPlugin('log_request');

        if ($tasks) {
            foreach ($tasks as $key => $task) {
                $toUpdate = false;

                if ($task['items']) {

                    $currentItems = json_decode($task['items'], 1);
                    foreach ($currentItems as $item) {
                        if ($item['status'] !== 'imported') {
                            $toUpdate = true;
                            break;
                        }
                    }
                } else {
                    $toUpdate = true;
                }

                if ($toUpdate) {
                    $api = $this->_getOzonApi();
                    $data = $api->taskInfo($task['task_id']);

                    if ($data) {
                        if ($data['items']) {
                            $taskItems = json_encode($data['items'], 1);
                            $model->query("UPDATE " . self::TABLE_TASKS . " SET items = s:items WHERE task_id = i:task", array('items' => $taskItems, 'task' => $task['task_id']));

                            foreach ($data['items'] as $item) {
                                $params = array('status' => $item['status'], 'offer' => $item['offer_id']);
                                $updateQuery = "UPDATE " . self::TABLE_PRODUCTS . " SET ozon_state = s:status";
                                if ($item['product_id'] && $item['product_id'] !== 0) {
                                    $updateQuery .= ", ozon_product_id = i:prod";
                                    $params['prod'] = $item['product_id'];
                                }
                                $updateQuery .= " WHERE ozon_offer_id = s:offer";

                                try {
                                    $model->query($updateQuery, $params);
                                } catch (Exception $exception) {
                                    if (isset($plugin_logs) && $plugin_logs)
                                        waLog::log($exception->getMessage(), "shopOzonius.log");
                                }
                            }
                        }
                    }
                } else {
                    $model->query("UPDATE " . self::TABLE_TASKS . " SET items = 'all items imported'");
                }
            }
        }
    }

    /** ---------------------------------- **/

    /** ---------- PRODUCT ACTIONS ------------ **/

    public function activateProducts($productIds)
    {
        $plugin_logs = $this->getSettingPlugin('log_request');
        $api = $this->_getOzonApi();
        $results = array('success' => 0, 'withError' => 0, 'errorIds' => array());

        foreach ($productIds as $productId) {
            $response = $api->productActivate($productId);
            if ($response == 'success') {
                $results['success']++;
            } else {
                $results['withError']++;
                $results['errorIds'][] = $productId;
            }
        }
        if (isset($plugin_logs) && $plugin_logs)
            waLog::log("Product deactivate: " . json_encode($results, 1), "shopOzonius.log");

        return $results;
    }

    public function deactivateProducts($productIds)
    {
        $plugin_logs = $this->getSettingPlugin('log_request');
        $api = $this->_getOzonApi();
        $results = array('success' => 0, 'withError' => 0, 'errorIds' => array());

        foreach ($productIds as $productId) {
            $response = $api->productDeactivate($productId);
            if ($response == 'success') {
                $results['success']++;
            } else {
                $results['withError']++;
                $results['errorIds'][] = $productId;
            }
        }
        if (isset($plugin_logs) && $plugin_logs)
            waLog::log("Product deactivate: " . json_encode($results, 1), "shopOzonius.log");

        return $results;
    }

    public function deleteProducts($skuIds)
    {


        $model = new waModel();
        foreach ($skuIds as $skuId) {

            $model->query("DELETE FROM " . self::TABLE_PRODUCTS . " WHERE ozon_offer_id = s:id", array('id' => $skuId));
        }

    }

    public function updatePrices($productIds)
    {
        $api = $this->_getOzonApi();
        $model = new waModel();
        $prices = array();
        $result = array('success' => 0, 'withErrors' => 0, 'offerIds' => array());
        $plugin_logs = $this->getSettingPlugin('log_request');
        if ($productIds) {
            foreach ($productIds as $productId) {
                $offerId = $model->query("SELECT shop_product_id,shop_product_sku_id,ozon_offer_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetchAll();
                $params = $this->_getProductParams($offerId['shop_product_id']);
                if ($params['ozon.ignored'] == 1) {
                    if (isset($plugin_logs) && $plugin_logs)
                        waLog::log("Product update price: товар с артикулом " . $offerId['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                    continue;
                }
                $priceInfo = $model->query("SELECT p.currency, s.price, s.compare_price, t.tax_value, s.product_id from shop_product_skus s
                                                join shop_product as p on p.id = s.product_id
                                                left outer join shop_tax_regions t on t.tax_id = p.tax_id
                                                where s.sku = s:sku", array('sku' => $offerId[0]['ozon_offer_id']))->fetch();

                if ($priceInfo['tax_value'] && $priceInfo['tax_value'] !== 0) {
                    $tax = number_format($priceInfo['tax_value'], 0);
                    $vat = $tax * 0.01;
                    $vatValue = (string)$vat;
                } else {
                    $vatValue = "0";
                }

                $settingAttrPrice = $this->getSettingPlugin('features_price');
                if($settingAttrPrice && $settingAttrPrice != ""){
                    $productFeatureModel = new shopProductFeaturesModel();
                    $featuresSku = $productFeatureModel->getValues($priceInfo['product_id'],$offerId[0]['shop_product_sku_id']);
                    $price = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $price = str_replace(",", "", number_format((float)$priceInfo['price'], 2));
                }

                $data = array(
                    'product_id' => (int)$productId,
                    'offer_id' => $offerId[0]['ozon_offer_id'],
                    'price' => $price,
                    'vat' => $vatValue
                );

                $marCatQueryPrice = $model->query("SELECT t1.shop_category_id FROM " . self::TABLE_CATEGORY . " AS t1 
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_category_id
WHERE t2.ozon_product_id = {$data['product_id']}")->fetch()['shop_category_id'];
                $marSetQueryPrice = $model->query("SELECT t1.shop_sets_id FROM " . self::TABLE_SETS . " AS t1 
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_sets_id
WHERE t2.ozon_product_id = {$data['product_id']}")->fetch()['shop_sets_id'];

                $dataPrice = $data['price'];

                if(!$settingAttrPrice && $settingAttrPrice == ""){
                    $data['price'] = $this->getCurrencyPrice($data['price'],$priceInfo['currency']);
                }
                $data['price'] = str_replace(",", "", number_format((float)$data['price'], 2));



                $ozonCatPriceMarkup = $this->getMatchedCategories();
                $ozonSetPriceMarkup = $this->getMatchedSets();
                if ($priceInfo['compare_price'] > 0 && $priceInfo['compare_price'] > $dataPrice) {
                    $data['old_price'] = $priceInfo['compare_price'];

                    if(!$settingAttrPrice && $settingAttrPrice == ""){
                        $data['old_price'] = $this->getCurrencyPrice($data['old_price'],$priceInfo['currency']);
                    }
                    $data['old_price'] = str_replace(",", "", number_format((float)$data['old_price'], 2));

                }
                if ($ozonCatPriceMarkup) {
                    $data['price'] = $this->ozonPriceMarkup($ozonCatPriceMarkup, $data['price'], $marCatQueryPrice);
                    if(!empty($data['old_price'])){
                        $data['old_price'] = $this->ozonPriceMarkup($ozonCatPriceMarkup, $data['old_price'], $marCatQueryPrice);
                    }
                }
                if ($ozonSetPriceMarkup) {
                    $data['price'] = $this->ozonPriceMarkup($ozonSetPriceMarkup, $data['price'], $marSetQueryPrice);
                    if(!empty($data['old_price'])){
                        $data['old_price'] = $this->ozonPriceMarkup($ozonSetPriceMarkup, $data['old_price'], $marSetQueryPrice);
                    }
                }

                $prices[] = $data;
            }

            $response = $api->productUpdatePrices($prices);

            if (!$response) {
                return $result['message'] = "Для данного товара не указаны цены в магазине.";
            }

            foreach ($response as $item) {
                if ($item['updated']) {
                    $result['success']++;
                } else {
                    $result['withErrors']++;
                    $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                }
            }
        }
        if (isset($plugin_logs) && $plugin_logs)
            waLog::log("Product update prices: " . json_encode($result, 1), "shopOzonius.log");

        return $result;
    }

    public function updateStocks($productIds)
    {
        $api = $this->_getOzonApi();
        $model = new waModel();
        $stocks = array();
        $result = array('success' => 0, 'withErrors' => 0, 'offerIds' => array());
        $getStocks = $this->getStocksFromSettings();
        $getWarehouses = $this->getWarehouseFromSettings();
        $plugin_logs = $this->getSettingPlugin('log_request');
        $get_fbs = $this->getSettingPlugin('get_fbs');
        $infinity = $this->getSettingPlugin('infinity');
        $disabled_stock = $this->getSettingPlugin('disabled_stock');

        if($get_fbs == "On"){
            if (!empty($getWarehouses) && $productIds) {
                foreach ($productIds as $productId) {
                    $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id,shop_product_sku_id, ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                    $params = $this->_getProductParams($productInfo['shop_product_id']);
                    if ($params['ozon.ignored'] == 1) {
                        if (isset($plugin_logs) && $plugin_logs)
                            waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                        continue;
                    }
                    foreach ($getWarehouses as $warehouse) {

                        $getStShop = explode(';', $warehouse['shop']);
                        $getStOzon = explode(';', $warehouse['ozon']);
                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {

                            $count = $model->query("SELECT SUM(`count`) FROM shop_product_stocks WHERE sku_id = i:id AND stock_id in ({$getStShop[1]})", array  ('id' => $productInfo['shop_product_sku_id']))->fetch()['SUM(`count`)'];

                            if(!isset($count)){
                                $count = $infinity;
                            }
                            $availablePrice = $model->query("SELECT price, available FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch();

                            if($availablePrice['price'] == 0 || $availablePrice['available'] == 0){
                                $count = 0;
                            }

                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }

                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count,
                                'warehouse_id' => (int)$getStOzon[1]
                            );

                            $stocks[] = $data;

                        }
                    }
                }

                $final_array = [];
                foreach($stocks as $arr){
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['product_id'] = $arr['product_id'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['offer_id'] = $arr['offer_id'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock'] = (isset($final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock']))? $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['stock'] + $arr['stock'] : $arr['stock'];
                    $final_array[$arr['offer_id'].'_'.$arr['warehouse_id']]['warehouse_id'] = $arr['warehouse_id'];
                }

                $final_array = array_values($final_array);
                $response = $api->productUpdateStocksWarehouse($final_array);
                if (!$response) {
                    return $result['message'] = "Для данного товара не указаны остатки в магазине.";
                }

                foreach ($response as $item) {
                    if ($item['updated']) {
                        $result['success']++;
                    } else {
                        $result['withErrors']++;
                        $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                    }
                }

            }
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log("Product update stocks: " . json_encode($result, 1), "shopOzonius.log");

            return $result;

        }
        else {
            if ($productIds) {

                if (!empty($getStocks)) {

                    foreach ($productIds as $productId) {
                        $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id,shop_product_sku_id,ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                        $params = $this->_getProductParams($productInfo['shop_product_id']);
                        if ($params['ozon.ignored'] == 1) {
                            if (isset($plugin_logs) && $plugin_logs)
                                waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                            continue;
                        }
                        $getStocksStr = implode(',', $getStocks);

                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {
                            $count = $model->query("SELECT SUM(`count`) FROM shop_product_stocks WHERE sku_id = i:id AND stock_id in ({$getStocksStr})", array('id' => $productInfo['shop_product_sku_id']))->fetch()['SUM(`count`)'];

                            if(!isset($count)){
                                $count = $infinity;
                            }
                            $availablePrice = $model->query("SELECT price, available FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch();

                            if($availablePrice['price'] == 0 || $availablePrice['available'] == 0){
                                $count = 0;
                            }
                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }
                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count
                            );
                            $stocks[] = $data;

                        }
                    }
                } else {
                    foreach ($productIds as $productId) {
                        $productInfo = $model->query("SELECT shop_product_id,ozon_offer_id,shop_product_sku_id,ozon_price FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                        $params = $this->_getProductParams($productInfo['shop_product_id']);
                        if ($params['ozon.ignored'] == 1) {
                            if (isset($plugin_logs) && $plugin_logs)
                                waLog::log("Product update stocks: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                            continue;
                        }

                        if ($productInfo['ozon_offer_id'] && $productInfo['shop_product_sku_id']) {
                            $shopProductSkus = $model->query("SELECT `count`, `price`, `available` FROM shop_product_skus WHERE id = i:id", array('id' => $productInfo['shop_product_sku_id']))->fetch()['count'];

                            $count = $shopProductSkus['count'];
                            if($shopProductSkus['price'] == 0 || $shopProductSkus['available'] == 0){
                                $count = 0;
                            }
                            if($disabled_stock && $disabled_stock != 0){
                                if($productInfo['ozon_price'] < $disabled_stock){
                                    $count = 0;
                                }
                            }
                            if((int)$count<0){
                                $count = 0;
                            }
                            $data = array(
                                'product_id' => (int)$productId,
                                'offer_id' => (string)$productInfo['ozon_offer_id'],
                                'stock' => (int)$count
                            );
                            $stocks[] = $data;

                        }

                    }
                }

                $response = $api->productUpdateStocks($stocks);

                if (!$response) {
                    return $result['message'] = "Для данного товара не указаны остатки в магазине.";
                }

                foreach ($response as $item) {
                    if ($item['updated']) {
                        $result['success']++;
                    } else {
                        $result['withErrors']++;
                        $result['offerIds'][$item['offer_id']] = array('code' => $item['errors'][0]['code'], 'message' => $item['errors'][0]['message']);
                    }
                }
            }
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log("Product update stocks: " . json_encode($result, 1), "shopOzonius.log");

            return $result;
        }
    }

    public function updateProductCards($productIds,$markup,$ozonCat)
    {
            $plugin_logs = $this->getSettingPlugin('log_request');
            $model = new waModel();
            $CategoryValueAttributeOpt = [];
            $CategoryValueAttributeReq = [];

            $ozonRequiredAttributes[] = $this->getRequiredAttributeList($ozonCat);
            sleep(10);
            $ozonOptionalAttributes[] = $this->getOptionalAttributeList($ozonCat);

            $api = $this->_getOzonApi();

            foreach ($ozonRequiredAttributes[0] as $value) {
                    if ($value['dictionary_id'] != 0) {
                        usleep(1000000);
                        $CategoryValueAttributeReq[$value['name']] = $api->getCategoryValueAttribute((int)$ozonCat, (int)$value['id']);
                    }
            }
            foreach ($ozonOptionalAttributes[0] as $value) {
                if ($value['dictionary_id'] != 0) {
                    usleep(1000000);
                    $CategoryValueAttributeOpt[$value['name']] = $api->getCategoryValueAttribute((int)$ozonCat, (int)$value['id']);
                }
            }
            foreach ($productIds as $productId) {
                if ($productId) {
                $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
                $params = $this->_getProductParams($productInfo['shop_product_id']);
                if ($params['ozon.ignored'] == 1) {
                    if (isset($plugin_logs) && $plugin_logs)
                    waLog::log("Product update card: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                    continue;
                }

                    $prepareData = $this->prepareUpdateProducts($productId, $ozonCat, $ozonRequiredAttributes, $CategoryValueAttributeReq, $ozonOptionalAttributes, $CategoryValueAttributeOpt, $markup);

                    $api->productImport($prepareData);
                }
            }


    }

    public function updateProductCardsManually($productId, $sku)
    {
        $plugin_logs = $this->getSettingPlugin('log_request');
        $model = new waModel();
        $api = $this->_getOzonApi();
        if ($productId) {
            $productInfo = $model->query("SELECT shop_product_id, ozon_offer_id FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId))->fetch();
            $params = $this->_getProductParams($productInfo['shop_product_id']);
            if ($params['ozon.ignored'] == 1) {
                if (isset($plugin_logs) && $plugin_logs)
                waLog::log("Product update card: товар с артикулом " . $productInfo['ozon_offer_id'] ." не может быть передан в озон по причине того что в дополнительных параметрах товара включен  ozon.ignored=1" , "shopOzonius.log");

                return false;
            }
            $infoProduct = $api->productInfo($productId);
            $prepareData = $this->prepareUpdateProductsManually($productId,$sku, $infoProduct);
            unset($prepareData[0]['category_id']);
            $prepareData[0]['category_id'] = $infoProduct['category_id'];


            $api->productImport($prepareData);
        }

    }


    protected function removeProductFromOzonius($productId)
    {
        if ($productId) {
            $model = new waModel();
            $model->query("DELETE FROM " . self::TABLE_PRODUCTS . " WHERE ozon_product_id = i:id", array('id' => $productId));
        }
    }

    /** ---------------------------------- **/

    /** ---------- ORDER ACTIONS ------------ **/

    /**
     * @param $since
     * @param string $to
     * @return mixed
     */
    protected function _getFboOrders($since, $to = '')
    {
        $api = $this->_getOzonApi();
        $gm = gmdate("Y-m-d H:i:s");
        $this->_logUpdate($gm);
        $time = $this->_getTime($since);

        return $api->orderList($time, $gm, self::ORDER_FBO_TYPE);
    }

    /**
     * @param $since
     * @param string $to
     * @return mixed
     */
    protected function _getFbsOrders($since, $to = '')
    {
        $api = $this->_getOzonApi();
        $gm = gmdate("Y-m-d H:i:s");
        $this->_logUpdate($gm);
        $time = $this->_getTime($since);

        return $api->orderList($time, $gm, self::ORDER_FBS_TYPE);
    }
    protected function _getTime($since)
    {

        $plugin_date_order = $this->getSettingPlugin('date_order');

        if($plugin_date_order && !empty($plugin_date_order)){

            $res =  date('Y-m-d\TH:i:s\Z', strtotime($since) - ($plugin_date_order*24*3600));
        }
        else{
            $res =  date('Y-m-d\TH:i:s\Z', strtotime($since));
        }

        return $res;
    }
    /**
     * @param $id
     * @return |null
     */
    public function _getOrderData($id, $deliverySchema)
    {
        $api = $this->_getOzonApi();
        if ($id) {
            return $api->orderInfo($id, $deliverySchema);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function updateOrderList()
    {

        $model = new waModel();
        $orders = array();
        $plugin_logs = $this->getSettingPlugin('log_request');
        $since = $model->query("SELECT dt_update FROM " . self::TABLE_ORDER_LOG . " ORDER BY dt_update DESC LIMIT 1")->fetch()['dt_update'];
        if (!$since) {
            $since = $model->query("SELECT ozon_created_at FROM " . self::TABLE_PRODUCTS . " WHERE ozon_created_at IS NOT NULL ORDER BY ozon_created_at ASC LIMIT 1")->fetch()['ozon_created_at'];
            if (!$since) {
                if (isset($plugin_logs) && $plugin_logs)
                    waLog::log("Orders Update: nothing to update.", "shopOzonius.log");
                return false;
            }
        }
        $orderOzonFboSetting =  $this->getSettingPlugin('order_ozon_fbo');

        $ozonFbsOrders = $this->_getFbsOrders($since);

        if($orderOzonFboSetting) {
            $ozonFboOrders = $this->_getFboOrders($since);
        }
        if (!empty($ozonFbsOrders)) {
            foreach ($ozonFbsOrders as $order) {
                $order['delivery_schema'] = "fbs";
                $this->_orderData($order);
            }
        }
        if (!empty($ozonFboOrders)) {
            foreach ($ozonFboOrders as $order) {
                $order['delivery_schema'] = "fbo";
                $this->_orderData($order);
            }
        }
    }


    protected function _orderData($order){
        $model = new waModel();
        $exists = $model->query("SELECT ozon_order_id, shop_order_id, ozon_order_last_update FROM " . self::TABLE_ORDERS . " WHERE ozon_order_id = i:order", array('order' => $order['order_id']))->fetch();

        if (!$exists) {
            $orderData = $this->_getOrderData($order['posting_number'], $order['delivery_schema']);



            $orderId = $this->_createStoreOrder($orderData, $order['posting_number'], $order['delivery_schema']);
            if(!empty($orderId['id'])) {
                $this->_insertOrderData($order, $orderData, $orderId['id']);
            }
            if (!$orderId) {
                if (isset($plugin_logs) && $plugin_logs)
                    waLog::log("Order with ozon number {$order['order_id']} cannot create in store.", "shopOzonius.log");
            }
        } else {

            $storeOrderId = $exists['shop_order_id'];
            if($storeOrderId) {
                $shopOrderModel = new shopOrderModel();
                $statusOzon = $order['status'];
                $state_id_before = $shopOrderModel->select('state_id')->where('id = '.(int) $storeOrderId)->fetchField();
                $newStatus = self::getStatus()[$statusOzon];
                $shopOrderLogModel = new shopOrderLogModel();
                if($newStatus != $state_id_before) {
                    $shopOrderModel->updateByField( 'id', $storeOrderId, array('state_id' => $newStatus));
                    $shopOrderLogModel->add(array(
                        'order_id' => $storeOrderId,
                        'action_id' => '',
                        'before_state_id' => $state_id_before,
                        'after_state_id' => $newStatus,
                        'text' => 'Изменен статус через API'
                    ));
                }
            }
        }
    }
    protected function _insertOrderData($order, $orderData, $orderId)
    {
        $model = new waModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        $dateС = new DateTime($orderData['created_at']);
        $dateCreate =  $dateС->format('Y-m-d H:i:s');
        $dateP = new DateTime($orderData['in_process_at']);
        $dateUpd =  $dateP->format('Y-m-d H:i:s');
        $insert = "INSERT INTO " . self::TABLE_ORDERS . " (ozon_order_id, shop_order_id, ozon_delivery_schema, ozon_status, posting_number, ozon_order_time, ozon_order_last_update) VALUES (i:order_ozon, i:order_shop, s:ozon_delivery_schema, s:status, s:posting, s:ozon_order_time, s:ozon_order_last_update)";

        $params = array(
            'order_ozon'=> $order['order_id'],
            'order_shop' => $orderId,
            'ozon_delivery_schema'=> $order['delivery_schema'],
            'status' => $orderData['status'],
            'posting' => $order['posting_number'],
            'ozon_order_time'=> $dateCreate,
            'ozon_order_last_update'=> $dateUpd
        );

        try {
            $model->query($insert, $params);
        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");
        }

        foreach ($orderData['products'] as $item) {
            $insert = "INSERT INTO " . self::TABLE_ORDER_ITEMS . " VALUES (i:item,i:order,i:prod,s:offer,i:qty,i:cancel)";
            $insertParams = array(
                'item' => "",
                'order' => $orderData['order_id'],
                'prod' => $item['sku'],
                'offer' => $item['offer_id'],
                'qty' => $item['quantity'],
                'cancel' => ""
            );
            try {
                $model->query($insert, $insertParams);
            } catch (Exception $exception) {
                if (isset($plugin_logs) && $plugin_logs)
                    waLog::log($exception->getMessage(), "shopOzonius.log");
            }
        }
    }

    protected function _createStoreOrder($orderData, $postingNumber, $deliverySchema)
    {
        $waContactModel = new waContactModel();
        $model = new waModel();

        $configShop = wa('shop')->getConfig();
        $email = $this->getSettingPlugin('email');
        $rulesContact = $this->getSettingPlugin('check_rules_contact');
        $domainUrl = wa()->getConfig()->getDomain();
        $fio = explode(" ", $orderData['addressee']['name']);
        if($rulesContact == 'enable' && !empty($orderData['customer'])){
                $contactExists = $waContactModel->getByEmail($orderData['customer']['customer_email']);
                if (!$contactExists) {
                    $contactObject = new waContact();
                    $contactData = array(
                        'email' => $orderData['customer']['customer_email'],
                        'phone' => $orderData['addressee']['phone'],
                        'firstname' => isset($fio[1])?$fio[1]:'',
                        'lastname' => isset($fio[0])?$fio[0]:'',
                    );

                    $contactObject->save($contactData);
                } else {
                    $contactObject = new waContact($contactExists['id']);
                }
        }
        else{
            if($email){
                $contactExists = $waContactModel->getByEmail($email);
                $emp = $email;
            }
            else{
                $contactExists = $waContactModel->getByEmail('ozon@'.$domainUrl);
                $emp = 'ozon@'.$domainUrl;
            }
            if (!$contactExists) {
                $contactObject = new waContact();
                $contactData = array(
                    'name' => 'OZON',
                    'is_user' => true,
                    'email' => $emp,
                );

                $contactObject->save($contactData);
            } else {
                $contactObject = new waContact($contactExists['id']);
            }

        }
        $shipping_date = date('d.m.Y', strtotime($orderData['shipment_date']));
        $order_number = $orderData['order_number'];

        if ($contactObject) {
            $items = array();
            $params = array();
            $currentCurrency = $configShop->getCurrency(false);
            $total = 0;
            foreach ($orderData['products'] as $item) {

                $sku_id = $model->query("SELECT id, product_id FROM shop_product_skus WHERE sku = s:id", array('id' => $item['offer_id']))->fetch();
                if ($sku_id) {
                    $stock = $model->query("SELECT stock_id FROM shop_product_stocks WHERE sku_id = i:id", array('id' => $sku_id['id']))->fetchAll();
                    if ($stock){
                        $getStocks = $this->getStocksFromSettings();

                        $stock_id = null;

                        if ($getStocks){
                            foreach ($getStocks as $getStock){
                                $stockAvailability = $model->query("SELECT count FROM shop_product_stocks WHERE stock_id = i:id AND sku_id = i:idsk", array('id' => $getStock,'idsk' => $sku_id['id']))->fetch()['count'];
                                if($stockAvailability > 0){
                                    $stock_id = $getStock;
                                    break;
                                }
                                else{
                                    continue;
                                }
                            }

                        }

                        $get_fbs = $this->getSettingPlugin('get_fbs');
                        $getWarehouses = $this->getWarehouseFromSettings();
                        if ($get_fbs == "On") {
                            if (!empty($getWarehouses)) {
                                foreach ($getWarehouses as $getWarehouse){
                                    $getStShop = explode(';', $getWarehouse['shop']);
                                    $stockAvailability = $model->query("SELECT count FROM shop_product_stocks WHERE stock_id = i:id AND sku_id = i:idsk", array('id' => $getStShop[1], 'idsk' => $sku_id['id']))->fetch()['count'];
                                    if($stockAvailability > 0){
                                        $stock_id = $getStShop[1];
                                        break;
                                    }
                                    else{
                                        continue;
                                    }
                                }
                            }
                        }

                    } else{
                        $stock_id = null;
                    }

                    $array = [
                        'price' => $item['price'],
                        'quantity' => $item['quantity'],
                        'product_id' => $sku_id['product_id'],
                        'sku_id' => $sku_id['id'],
                        'type' => 'product',
                        'currency' => $currentCurrency,
                        'stock_id' => $stock_id,

                    ];
                    array_push($items, $array);
                    $total += $item['price'] * $item['quantity'];
                }
            }

            $params['sales_channel'] = 'Ozon';
            $params['posting_number'] = $postingNumber;
            $params['delivery_schema'] = $deliverySchema;
            $routingUrl = wa()->getRouting()->getRootUrl();
            $params['storefront'] = wa()->getConfig()->getDomain() . ($routingUrl ? '/' . $routingUrl : '');


            $orderDataNew = array(
                'contact_id'      => $contactObject->getId(),
                'currency' => 'RUB',
                'discount' => 0,
                'total_discount' => 0,
                'items' => $items,
                'params' => $params,
            );
            if($rulesContact == 'enable' && !empty($orderData['customer'])){
                $orderDataNew['customer'] = array(
                    'firstname' => isset($fio[1])?$fio[1]:'',
                    'address.shipping' => array(
                        'country' => 'rus',
                        'city' => $orderData['customer']['address']['city'],
                        'region' => $orderData['customer']['address']['region'],
                        'street' => $orderData['customer']['address']['address_tail'],
                    )
                );
                $orderDataNew['comment'] = "***Заказ OZON***" . PHP_EOL . 'Дата отгрузки: ' . $shipping_date . PHP_EOL . 'Номер заказа: ' . $order_number . PHP_EOL . 'Метод доставки: ' .  $orderData['delivery_method']['name'];
            }
            else{
                if(!empty($orderData['customer'])){
                    $orderDataNew['customer'] = array(
                        'firstname' => isset($fio[1])?$fio[1]:'',
                        'address.shipping' => array(
                            'country' => 'rus',
                            'city' => $orderData['customer']['address']['city'],
                            'region' => $orderData['customer']['address']['region'],
                            'street' => $orderData['customer']['address']['address_tail'],
                        )
                    );
                    $orderDataNew['comment'] = "***Заказ OZON***" . PHP_EOL . 'Дата отгрузки: ' . $shipping_date . PHP_EOL . 'Номер заказа: ' . $order_number . PHP_EOL . 'Метод доставки: ' .  $orderData['delivery_method']['name'] . PHP_EOL . 'Телефон: ' .  $orderData['customer']['phone'] . PHP_EOL . 'email: ' .  $orderData['customer']['customer_email'] . PHP_EOL . 'Комментарии озона: ' .  $orderData['customer']['address']['comment'];
                }
                else{
                    $orderDataNew['customer'] = array(
                        'firstname' => 'Ozon',
                        'address.shipping' => array(
                            'country' => 'rus',
                            'street' => 'Ozon-street',
                        )
                    );
                    $delMethod = '';
                    if(isset($orderData['delivery_method']['name'])){
                        $delMethod = 'Метод доставки: ' . $orderData['delivery_method']['name'];
                    }
                    $orderDataNew['comment'] = "***Заказ OZON***" . PHP_EOL . 'Дата отгрузки: ' . $shipping_date . PHP_EOL . 'Номер заказа: ' . $order_number . PHP_EOL . $delMethod;
                }

            }
            $order = new shopOrder($orderDataNew);
            try {
                $saved_order = $order->save();

            } catch (waException $ex) {
                waLog::dump($order->errors(), 'shopOzoniusOrder.log');
            }

            $orderLog = new shopOrderLogModel();
            $data = array(
                'order_id' => $order['id'],
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<i class="icon16 ozon"></i> Заказ оформлен через OZON '.$deliverySchema.'/list',
            );
            $orderLog->insert($data);
            return $order;


        }
    }

    protected function _updateOrder($order)
    {
        $model = new waModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        try {
            $model->query("UPDATE " . self::TABLE_ORDERS . " SET ozon_status = s:status WHERE ozon_order_id = i:order", array('status' => $order['status'], 'order' => $order['order_id']));
        } catch (Exception $exception) {
            if (isset($plugin_logs) && $plugin_logs)
                waLog::log($exception->getMessage(), "shopOzonius.log");
        }
    }

    protected function _updateOrderData($orderId, $orderData)
    {
        $model = new waModel();
        $plugin_logs = $this->getSettingPlugin('log_request');
        foreach ($orderData['products'] as $item) {
            $update = "UPDATE " . self::TABLE_ORDER_ITEMS . " SET ozon_cancel_reason_id = i:cancel WHERE ozon_order_item_id = i:item";
            try {
                $model->query($update, array('cancel' => '', 'item' => $item['sku']));
            } catch (Exception $exception) {
                if (isset($plugin_logs) && $plugin_logs)
                    waLog::log($exception->getMessage(), "shopOzonius.log");
            }
        }
    }


    protected function _logUpdate($to)
    {
        $model = new waModel();
        $model->query("INSERT INTO " . self::TABLE_ORDER_LOG . " (dt_update) VALUES (s:to)", array('to' => $to));
    }

    /** ----------------------------------- **/

    public function backendMenu()
    {
        $plugin_enabled = $this->getSettingPlugin('status_plugin');

        if ($plugin_enabled) {
            return array(
                'core_li' => '<li class="no-tab"><a href="?plugin=ozonius">Интеграция Ozon</a></li>',
            );
        }
    }

    public function backendOrder($params)
    {

        if (!empty($params['params']['posting_number'])) {

            $orderData = $this->_getOrderData($params['params']['posting_number'], $params['params']['delivery_schema']);
            $orderStatusName = "";
            switch ($orderData['status']) {
                case 'awaiting_packaging':
                    $orderStatusName = "Ожидает упаковки";
                    break;
                case 'not_accepted':
                    $orderStatusName = "Не принят в сортировочном центре";
                    break;
                case 'arbitration':
                    $orderStatusName = " Ожидает решения спора";
                    break;
                case 'awaiting_deliver':
                    $orderStatusName = "Ожидает отгрузки";
                    break;
                case 'driver_pickup':
                    $orderStatusName = "У водителя";
                    break;
                case 'delivered':
                    $orderStatusName = "Доставлено";
                    break;
                case 'cancelled':
                    $orderStatusName = "Отменено";
                    break;
                case 'delivering':
                    $orderStatusName = "Доставляется";
                    break;
            }
            $path = wa('shop')->getAppPath('plugins/ozonius/templates/actions/backend/BackendOrder.html');
            $view = wa()->getView();
            $view->assign('posting', $params['params']['posting_number']);
            $view->assign('deliverySchema', $params['params']['delivery_schema']);
            $view->assign('ozoniusStatusName', $orderStatusName);
            $view->assign('orderData', $orderData);
            $html = $view->fetch($path);
            if (empty($html)) {
                return [];
            }

            return array(
                'info_section' => $html,
            );
        }
    }

    public function orderConfirm($ozonPostingId)
    {

        $api = $this->_getOzonApi();
        $model = new waModel();
        $orderId = $model->query("SELECT shop_order_id FROM " . self::TABLE_ORDERS . " WHERE posting_number = s:order", array('order' => $ozonPostingId))->fetch();

        $orderLog = new shopOrderLogModel();
        $data = array(
            'order_id' => $orderId['shop_order_id'],
            'action_id' => '',
            'datetime' => date('Y-m-d H:i:s'),
            'before_state_id' => '',
            'after_state_id' => '',
            'text' => '<i class="icon16 ozon"></i> Заказ собран OZON (awaiting_deliver) fbs/ship',
        );

        $itemsRes = $model->query("SELECT t2.ozon_product_id as sku, t2.ozon_quantity as quantity FROM " . self::TABLE_ORDERS . " AS t1
INNER JOIN " . self::TABLE_ORDER_ITEMS . " AS t2 ON t1.ozon_order_id = t2.ozon_order_id
WHERE t1.posting_number = s:posting", array('posting' => $ozonPostingId))->fetchAll();
        $it = array();
        $items = array();
        foreach ($itemsRes as $item) {
            $mas = array(
                'quantity' => (string)$item['quantity'],
                'sku' => (int)$item['sku'],
            );
            $it[] = $mas;
        }
        $items['items'] = $it;

        $orderLog->insert($data);

        return $api->orderCollect($ozonPostingId, $items);
    }

    public function orderCancel($dataСancel)
    {

        $api = $this->_getOzonApi();
        $model = new waModel();
        if (empty($dataСancel['status']) || ($dataСancel['status'] == '402' && empty($dataСancel['another']))) {
            return [];
        } else {
            $orderId = $model->query("SELECT shop_order_id FROM " . self::TABLE_ORDERS . " WHERE posting_number = i:order", array('order' => $dataСancel['posting']))->fetch();

            $orderLog = new shopOrderLogModel();
            $data = array(
                'order_id' => $orderId['shop_order_id'],
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<i class="icon16 ozon"></i> Заказ отменен OZON (cancelled) fbs/cancel',
            );

            $orderLog->insert($data);
            return $api->orderCancelApi($dataСancel);
        }

    }

    public function orderPrintPDF($data)
    {
        $api = $this->_getOzonApi();
        return $api->getOrderPrintPDF($data);
    }
    public function addTrack($data)
    {
        $api = $this->_getOzonApi();
        return $api->addTrackSend($data);
    }
    public function searchProduct($data)
    {
        $offers = $products = [];
        $model = new waModel();
        if($data['sel'] == 'category') {
            $offers = $model->query("SELECT * FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_category_id = {$data['select_val']} AND ozon_offer_id LIKE '%" . $data['value'] . "%'")->fetchAll();
        }
        elseif ($data['sel'] == 'sets'){
            $offers = $model->query("SELECT * FROM " . self::TABLE_PRODUCTS . " WHERE ozonius_sets_id = {$data['select_val']} AND ozon_offer_id LIKE '%" . $data['value'] . "%'")->fetchAll();
        }
        elseif ($data['sel'] == 'manually'){
            $offers = $model->query("SELECT * FROM " . self::TABLE_PRODUCTS . " WHERE shop_prod_manually = {$data['select_val']} AND ozon_offer_id LIKE '%" . $data['value'] . "%'")->fetchAll();
        }



        if ($offers) {
            foreach ($offers as $key => $product) {

                $shopInfo = $model->query("SELECT price, `count` from shop_product_skus
                                                where sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch();

                $getStocks = $this->getStocksFromSettings();
                $get_fbs = $this->getSettingPlugin('get_fbs');
                $getWarehouses = $this->getWarehouseFromSettings();
                if($get_fbs == "On"){
                    if (!empty($getWarehouses)) {
                        $arr = array();
                        foreach ($getWarehouses as $getWarehouse){

                            $getStShop = explode(';', $getWarehouse['shop']);
                            $arr[] = $getStShop;
                        }
                        $getStocksStr =  implode(',', array_map(function ($value) {
                            return $value[1];
                        }, $arr));


                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];

                    }
                }
                else{
                    if (!empty($getStocks)) {
                        $getStocksStr = implode(',', $getStocks);
                        $shopCount = $model->query("SELECT SUM(t1.count)
FROM shop_product_stocks as t1
INNER JOIN shop_product_skus t2 ON t1.sku_id = t2.id
 WHERE t2.sku = '{$product['ozon_offer_id']}' AND t1.stock_id in ({$getStocksStr})")->fetch()['SUM(t1.count)'];


                    } else {
                        $shopCount = ($shopInfo['count']) ? $shopInfo['count'] : '';
                    }
                }

                $productId = $model->query("SELECT `product_id` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['product_id'];
                $nameDefault = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];

                $name_skus = $model->query("SELECT `name` FROM shop_product_skus WHERE sku = s:sku", array('sku' => $product['ozon_offer_id']))->fetch()['name'];

                $name = $nameDefault . ", " . $name_skus;
                if (!$name_skus || $name_skus == '') {
                    $name = $model->query("SELECT `name` FROM shop_product WHERE id = i:id", array('id' => $productId))->fetch()['name'];
                }
                $url = $this->_getBackendProductUrl($product['shop_product_id']);
                $stocks = json_decode($product['ozon_stocks'], 1);

                if (array_key_exists($product['ozon_state'], $this->_ozonStatuses)) {
                    $state = $this->_ozonStatuses[$product['ozon_state']];
                } else {
                    $state = $product['ozon_state'];
                }

                $visibleState = ($product['ozon_visible']) ? 'Показывается' : 'Скрыт';
                $visible = array(
                    'state' => $visibleState,
                    'details' => json_decode($product['ozon_visibility_details'], 1)
                );

                $validationErrors = json_decode($product['ozon_validation_errors'], 1) ?: array();
                if ($validationErrors) {
                    foreach ($validationErrors as $key => $error) {
                        $fixed = preg_replace('/(u)/', '\u', $validationErrors[$key]['error']);
                        $validationErrors[$key]['error'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                        }, $fixed);
                    }
                }


                if($data['sel'] == 'category') {
                    $markup = $model->query("SELECT t1.ozon_category_id, t1.markup_price, t1.fix_price FROM " . self::TABLE_CATEGORY . " AS t1
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_category_id
WHERE t2.id = {$product['id']}")->fetch();
                }
                elseif ($data['sel'] == 'sets'){
                    $markup = $model->query("SELECT t1.ozon_category_id, t1.markup_price, t1.fix_price FROM " . self::TABLE_SETS . " AS t1
INNER JOIN " . self::TABLE_PRODUCTS . " AS t2 ON t1.id = t2.ozonius_sets_id
WHERE t2.id = {$product['id']}")->fetch();
                }
                elseif ($data['sel'] == 'manually'){
                    $markup['markup_price'] = $markup['ozon_category_id'] = $markup['fix_price'] = 0;
                }

                $settingAttrPrice = $this->getSettingPlugin('features_price');

                if($settingAttrPrice && $settingAttrPrice != ""){
                    $productFeatureModel = new shopProductFeaturesModel();
                    $featuresSku = $productFeatureModel->getValues($product['shop_product_id'],$data['shop_product_sku_id']);
                    $price = str_replace(",", "", number_format((float)$featuresSku[$settingAttrPrice], 2));
                }
                else{
                    $price = str_replace(",", "", number_format((float)$shopInfo['price'], 2));
                }

                $products[$product['id']] = array(
                    'name' => $name,
                    'url' => $url,
                    'price' => str_replace(",", "", number_format((float)$product['ozon_price'], 2)),
                    'markup' => $markup['markup_price'],
                    'fix' => $markup['fix_price'],
                    'shopPrice' => $price,
                    'ozonId' => $product['ozon_product_id'],
                    'ozonCat' => $markup['ozon_category_id'],
                    'stocks' => $stocks,
                    'shopCount' => $shopCount,
                    'shopSku' => $product['ozon_offer_id'],
                    'state' => $state,
                    'visible' => $visible,
                    'validationErrors' => $validationErrors
                );
            }
        }

        return $products;
    }
}


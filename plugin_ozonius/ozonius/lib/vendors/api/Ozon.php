<?php
/**
 * Created by PhpStorm.
 * User: Ilya Steshkov (Wesma)
 * Date: 23.08.2019
 * Time: 18:48
 */

require_once "TSingleton.php";

class Ozon
{
    use TSingleton;

    const
        API_URL = 'https://api-seller.ozon.ru',
        LANG = 'RU';

    const
        CATEGORY_TREE_URL = '/v1/category/tree',
        CATEGORY_ATTR_URL_v3 = '/v3/category/attribute',
        CATEGORY_ATTR_VALUE_URL_v2 = '/v2/category/attribute/values',
        PRODUCT_IMPORT_URL_v2 = '/v2/product/import',
        PRODUCT_IMPORT_INFO_URL = '/v1/product/import/info',
        PRODUCT_INFO_URL_v2 = '/v2/product/info',
        PRODUCT_INFO_LIST_URL_v2 = '/v2/product/info/list',
        PRODUCT_INFO_STOCKS_WAREHOUSE_URL = '/v1/warehouse/list',
        PRODUCT_LIST_URL = '/v1/product/list',
        PRODUCT_IMPORT_PRICES_URL = '/v1/product/import/prices',
        PRODUCT_IMPORT_STOCKS_URL = '/v1/product/import/stocks',
        PRODUCT_IMPORT_STOCKS_WAREHOUSE_URL = '/v2/products/stocks',
        PRODUCT_ARCHIVE_URL = '/v1/product/archive',
        PRODUCT_UNARCHIVE_URL = '/v1/product/unarchive',
        PRODUCT_DELETE_URL = '/v2/products/delete',
        ORDER_LIST_URL = '/v3/posting/fbs/list',
        ORDER_LIST_FBO_URL = '/v2/posting/fbo/list',
        ORDER_COLLECT_URL = '/v2/posting/fbs/ship',
        ORDER_CANCEL_URL = '/v2/posting/fbs/cancel',
        ORDER_PRINT_URL = '/v2/posting/fbs/package-label',
        ORDER_INFO_URL = '/v3/posting/fbs/get',
        ORDER_INFO_FBO_URL = '/v2/posting/fbo/get',
        ORDER_TRACK_URL = '/v2/fbs/posting/tracking-number/set';

    const
        REQ_ATTR_TYPE_REQUIRED = 'REQUIRED',
        REQ_ATTR_TYPE_OPTIONAL = 'OPTIONAL',
        REQ_ATTR_TYPE_ALL = 'ALL';

    const
        ATTR_TYPE_BOOL = 'bool',
        ATTR_TYPE_TEXT = 'text',
        ATTR_TYPE_OPTION = 'option',
        ATTR_TYPE_CHILD = 'child';

    const
        ORDER_STATUS_AWAITING_APPROVE = "AWAITING_APPROVE",
        ORDER_STATUS_AWAITING_PACKAGING = "AWAITING_PACKAGING",
        ORDER_STATUS_AWAITING_DELIVER = "AWAITING_DELIVER",
        ORDER_STATUS_DELIVERING = "DELIVERING",
        ORDER_STATUS_DELIVERED = "DELIVERED",
        ORDER_STATUS_CANCELLED = "CANCELLED";

    const
        ORDER_TYPE_FBS = "fbs",
        ORDER_TYPE_FBO = "fbo";

    const
        ORDER_DATE_FORMAT = "Y-m-d\TH:i:s";

    public function getPluginDataPath()
    {
        return wa()->getDataPath('plugins', true, 'shop') . "/ozonius/";
    }
    public static $stateMap = [
        'awaiting_approve'=>'complete',

    ];
    private $_apiKey, $_clientId;

    private $allowedMethods = array('GET', 'POST');

    public function __construct($apiKey, $clientId)
    {
        $this->_apiKey = $apiKey ?: '9753260e-2324-fde7-97f1-7848ed7ed097';
        $this->_clientId = (int)$clientId ?: 466;
    }

    private function _apiCall($url, $data = array(), $method = 'POST')
    {
        usleep(200000);
        if (!in_array($method, $this->allowedMethods)) return false;

        // TODO вернуть перед боем
        $url = self::API_URL . $url;
        $curl = curl_init();

        if ($method === 'POST') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            if ($data) {


                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_SLASHES));
                file_put_contents("data.json", json_encode($data, JSON_UNESCAPED_SLASHES));
            }
        } else {
            if ($data) {
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }
        }

        $headers = array();
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Client-Id: ' . $this->_clientId;
        $headers[] = 'Api-Key: ' . $this->_apiKey;
        $headers[] = '‘x-o3-app-name: «Webasyst Shop script Wesma»';
        $headers[] = '‘x-o3-app-version: «4.6.0»,«webasyst@wesma.ru 7 495 118-24-74 доб. 11»';

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $result = curl_exec($curl);

        if (curl_errno($curl)) {
            $this->logError(curl_error($curl));
        }

        curl_close($curl);

        return $result;

    }

    private function _apiCallWarehouse($url){
        usleep(200000);
        $url = self::API_URL . $url;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"client_id\": \"YOUR_CLIENT_ID\"}",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "Client-Id: "  . $this->_clientId,
                "Api-Key: " . $this->_apiKey,
                "‘x-o3-app-name: «Webasyst Shop script Wesma»",
                "‘x-o3-app-version: «4.6.0»,«webasyst@wesma.ru 7 495 118-24-74 доб. 11»"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }
    protected function logError($msg)
    {
        file_put_contents('wa-log/ozonius_error.log', date('Y-m-d H:i:s') . ": " . $msg . "\n\n", FILE_APPEND);
    }

    public function getSettingPlugin($setting_name = '')
    {
        if (!empty($setting_name)) {
            return wa('shop')->getPlugin('ozonius')->getSettings($setting_name);
        } else {
            return wa('shop')->getPlugin('ozonius')->getSettings();
        }
    }

    public function getCategoryTree($categoryId = '0')
    {
        $data = array(
            'category_id' => $categoryId,
            'language' => self::LANG
        );

        $response = json_decode($this->_apiCall(self::CATEGORY_TREE_URL, $data), true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function getCategoryAttribute($categoryId, $attributeType)
    {
        $data = array(
            'attribute_type' => $attributeType,
            'category_id' => array($categoryId),
            'language' => self::LANG
        );
            $response = json_decode($this->_apiCall(self::CATEGORY_ATTR_URL_v3, $data), true);

        if (isset($response['result'])) {
            return $response['result'][0]['attributes'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function getCategoryValueAttribute($categoryId, $attributeType)
    {
        $allArr = array();
        $hasNext = 1;
        $id = 0;
        $count = 0;

        while ((bool)$hasNext) {
        $data = array(
            "category_id" => $categoryId,
            "attribute_id" => $attributeType,
            "language" => "RU",
            "limit" => 5000,
        );

        if($id != 0) {
            $data['last_value_id'] = $id;
        }

         $response = json_decode($this->_apiCall(self::CATEGORY_ATTR_VALUE_URL_v2, $data), true);
        $last = end($response['result']);
        $id = $last['id'];
        $hasNext = $response['has_next'];

        foreach ($response['result'] as $key => $item) {

                        $arr = array(
                        'id' => $item['id'],
                        'value' => $item['value'],
                    );

                        array_push($allArr,$arr);
        }
            $count++;
    }

        if (isset($allArr)) {
            return $allArr;
        }

        $this->logError(json_encode($allArr));
        return false;
    }
    public function getCategoryExampleValueAttribute($categoryId, $attributeType)
    {
            $data = array(
                "category_id" => $categoryId,
                "attribute_id" => $attributeType,
                "language" => "RU",
                "limit" => 5000,
            );
            $response = json_decode($this->_apiCall(self::CATEGORY_ATTR_VALUE_URL_v2, $data), true);


        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }
    public function productImport($items)
    {
        if (empty($items)) {
            $this->logError("productImport (empty items).");
            return false;
        }

        if (count($items) >= 100) {
            $this->logError("productImport (too many items, over 100).");
            return false;
        }

        $data = array(
            'items' => $items
        );
            $response = json_decode($this->_apiCall(self::PRODUCT_IMPORT_URL_v2,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function productInfo($productId)
    {
        if (!$productId) {
            $this->logError("productInfo (empty id).");
            return false;
        }

        $data = array(
            'product_id' => $productId
        );

            $response = json_decode($this->_apiCall(self::PRODUCT_INFO_URL_v2,$data),true);
            if (isset($response['result'])) {
                return $response['result'];
            }
        $this->logError(json_encode($response));
        return false;
    }
    public function productInfoList($productId)
    {
        if (!$productId) {
            $this->logError("productInfo (empty id).");
            return false;
        }

        $data = array(
            'product_id' => $productId
        );
            $response = json_decode($this->_apiCall(self::PRODUCT_INFO_LIST_URL_v2,$data),true);

            if (isset($response['result'])) {
                return $response['result'];
            }
        $this->logError(json_encode($response));
        return false;
    }
    public function taskInfo($taskId)
    {
        if (!$taskId) {
            $this->logError("taskInfo (empty id).");
            return false;
        }

        $data = array(
            'task_id' => $taskId
        );

        $response = json_decode($this->_apiCall(self::PRODUCT_IMPORT_INFO_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }
    public function listInfo()
    {
        $page = 1;
        do {
            $data = array(
                "filter" => array(
                    "visibility" => "ALL"
                ),
                "page" => $page,
                "page_size" => 1000
            );
            $page++;

            $response = json_decode($this->_apiCall(self::PRODUCT_LIST_URL, $data), true);
            if (!empty($response['result'])) {
                foreach ($response['result']['items'] as $data) {
                    $result[] = $data;
                }
            }

            if (!empty($response['result']) and sizeof($response['result']) > $response['result']['total']) break;

        } while (!empty($response['result']));

        if (!empty($result)) {
            return $result;
        }
    }

    public function productActivate($productId)
    {
        if (!$productId) {
            $this->logError("productArhive(empty id).");
            return false;
        }

        $data = array(
            'product_id' => $productId
        );

        $response = json_decode($this->_apiCall(self::PRODUCT_ARCHIVE_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function productDeactivate($productId)
    {
        if (!$productId) {
            $this->logError("productDeactivate (empty id).");
            return false;
        }

        $data = array(
            'product_id' => $productId
        );

        $response = json_decode($this->_apiCall(self::PRODUCT_UNARCHIVE_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function productDelete($offer_id)
    {
        if (!$offer_id) {
            $this->logError("Артикул озона (empty id).");
            return false;
        }
        $data = array(
            'products' => array(
                array(
                    'offer_id'=>(string)$offer_id
                )
            ),

        );
        $response = json_decode($this->_apiCall(self::PRODUCT_DELETE_URL,$data),true);

        if (isset($response)) {
            return $response;
        }
        $this->logError(json_encode($response));
        return false;
    }


    public function productUpdatePrices($prices)
    {
        if (!$prices) {
            $this->logError("productUpdatePrices (empty prices).");
            return false;
        }

        $data = array(
            'prices' => $prices
        );


        $response = json_decode($this->_apiCall(self::PRODUCT_IMPORT_PRICES_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function productUpdateStocks($stocks)
    {
        if (!$stocks) {
            $this->logError("productUpdateStocks (empty stocks).");
            return false;
        }

        $data = array(
            'stocks' => $stocks
        );
        $response = json_decode($this->_apiCall(self::PRODUCT_IMPORT_STOCKS_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function productUpdateStocksWarehouse($stock)
    {
        if (!$stock) {
            $this->logError("productUpdateStocks (empty stocks).");
            return false;
        }
        $data = array(
            'stocks' => $stock
        );
        $response = json_decode($this->_apiCall(self::PRODUCT_IMPORT_STOCKS_WAREHOUSE_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    /**
     * @param string $since format (Y-m-d H:i:s)
     * @param string $to format (Y-m-d H:i:s)
     * @param string $deliverySchema
     * @param array $statuses
     * @param int $page
     * @param int $pageSize
     * @return bool
     * @throws Exception
     */
    public function orderList($since = '', $to = '', $deliverySchema) {

        if (!$since || ! $to) {
            $this->logError("Order_List: Parameters 'since' and 'to' are required.");
            return false;
        } else {
            $to = gmdate("Y-m-d\TH:i:s\Z");
        }
        $response = [];
        $result = [];
        $offset = 0;
        $has_next = 1;
        $limit = 50;
        $count = 0;
        while ((bool)$has_next){
            $data = array(
                "dir" => "asc",
                'filter' => array(
                    'since' => $since,
                    'to' => $to,
                ),
                "offset" => $offset,
                "limit" => $limit,
            );
            $offset += $limit;
            if ($deliverySchema == self::ORDER_TYPE_FBS) {
                $response = json_decode($this->_apiCall(self::ORDER_LIST_URL, $data), true);
                $has_next = $response['result']['has_next'];

                if(!empty($response['result']['postings'])){
                    foreach ($response['result']['postings'] as $data){
                        $result[] = $data;
                    }
                }
                $count++;
            }
            elseif ($deliverySchema == self::ORDER_TYPE_FBO){
                $response = json_decode($this->_apiCall(self::ORDER_LIST_FBO_URL, $data), true);
                $has_next = $response['result']['has_next'];

                if(!empty($response['result'])){
                    foreach ($response['result'] as $data){
                        $result[] = $data;
                    }
                }
                $count++;
            }

        }

        if (!empty($result)) {
            return $result;
        }

        $this->logError(json_encode($response));
        return false;

    }

    public function orderInfo($id, $deliverySchema)
    {
        $response = [];
        if (!$id) {
            $this->logError("orderInfo (empty id).");
            return false;
        }

        $data = array(
            "posting_number" => (string)$id
        );
        if ($deliverySchema == self::ORDER_TYPE_FBS) {
            $response = json_decode($this->_apiCall(self::ORDER_INFO_URL, $data), true);
        }
        elseif ($deliverySchema == self::ORDER_TYPE_FBO){
            $response = json_decode($this->_apiCall(self::ORDER_INFO_FBO_URL, $data), true);
        }
        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function orderCollect($idPosting,$packages)
    {
        if (!$idPosting) {
            $this->logError("orderPosting (empty id).");
            return false;
        }

        $data = array(
            "posting_number" => (string)$idPosting,
            "packages" =>  array($packages)
        );

        $response = json_decode($this->_apiCall(self::ORDER_COLLECT_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }

    public function orderCancelApi($dataСancel)
    {

        $data = array(
            "posting_number" => (string)$dataСancel['posting'],
            "cancel_reason_id" =>  (int)$dataСancel['status'],
            "cancel_reason_message" =>  (string)$dataСancel['another']
        );

        $response = json_decode($this->_apiCall(self::ORDER_CANCEL_URL,$data),true);

        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }
    public function getOrderPrintPDF($posting)
    {
        $data = array(
            'posting_number' => array(
                0 => (string)$posting
            )
        );
        $res = json_decode($this->_apiCall(self::ORDER_PRINT_URL,$data));

        $response = base64_encode($this->_apiCall(self::ORDER_PRINT_URL,$data));
        if($res->error->code == 'POSTINGS_NOT_READY'){
            return 'Отправление не готово к маркировке, повторите попытку позже';
        }
        if (isset($response)) {
            return $response;
        }

        $this->logError(json_encode($response));
        return false;
    }
    public function warehouse(){
        $response = json_decode($this->_apiCallWarehouse(self::PRODUCT_INFO_STOCKS_WAREHOUSE_URL),true);
        if (isset($response['result'])) {
            return $response['result'];
        }

        $this->logError(json_encode($response));
        return false;
    }
    public function addTrackSend($data){
        $dataRes['tracking_numbers'] = array(array(
            "posting_number" => (string)$data['posting'],
            "tracking_number" =>  (string)$data['track'])
        );
        $response = json_decode($this->_apiCall(self::ORDER_TRACK_URL,$dataRes),true);
        if (isset($response['result'])) {
            return $response['result'];
        }
        $this->logError(json_encode($response));
        return false;
    }

}
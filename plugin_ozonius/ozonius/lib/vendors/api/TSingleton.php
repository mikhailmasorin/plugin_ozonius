<?php
    /**
     * Created by PhpStorm.
     * User: Ilya Steshkov (Wesma)
     * Date: 26.08.2019
     * Time: 11:44
     */

    trait TSingleton
    {
        private static $_instance;

        private function __construct(){}
        private function __wakeup(){}
        private function __clone(){}

        public static function getInstance($apiKey,$clientId)
        {
            if (is_null(static::$_instance)) {
                static::$_instance = new static($apiKey,$clientId);
            }
            return static::$_instance;
        }
    }